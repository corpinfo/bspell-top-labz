#using order of operations to delete VPC objects from here: https://forums.aws.amazon.com/thread.jspa?threadID=92407
[CmdletBinding(SupportsShouldProcess = $true, ConfirmImpact = 'High', DefaultParameterSetName = 'All')]
Param(
    $Region = 'us-west-1',

    #Parameter help description
    [Parameter(ParameterSetName='Service')]
    [ValidateSet('IAM','EC2','CloudFormation','ElasticBeanstalk','S3')]
    [string]
    $ServicetoPurge,

    [switch]
    $purgeAll
)
if ( !$Region ) { 
    $Regions= (Get-AWSRegion | 
    Get-AWSRegion | 
        Where-Object { $_.region -match "(us|eu|ca)-(east|west|south|north)"} |
            ForEach-Object {
                try {
                    Get-EC2AvailabilityZone -Region $_.Region | Out-Null
                    $_    
                }
                catch {
                    
                }
            }).Region
} else { $Regions = $Region }

$return = New-Object System.Collections.ArrayList


foreach ( $Region in $Regions ) {
    # Remove any Autoscaling groups first to keep resources from re-appearing
    Get-ASAutoScalingGroup -Region $Region | Remove-ASAutoScalingGroup -ForceDelete:$true -Region $Region

    #remove stacks 
    Get-CFNStack -Region $Region | 
        Where-Object { $_.StackName -notmatch "^rs-.+"} | 
            ForEach-Object {
                $stack = $_
                if ( $PSCmdlet.ShouldProcess( $stack.StackName, 'Delete Stack') ) {
                    try {
                        Remove-CFNStack -StackName $stack.StackName -Region $Region -ErrorAction Stop
                        $stack = Get-CFNStack -StackName $stack.StackName -Region $Region
                        while ( $stack ) {
                            try {
                                $stack = Get-CFNStack -StackName $stack.StackName -Region $Region -ErrorAction Stop
                                if ( $stack.StackStatus.Value -match "DELETE_FAILED" ) {
                                    $stackevents = Get-CFNStackEvent -StackName $stack.StackName -Region $Region | where-object {$_.ResourceStatus -eq 'DELETE_FAILED'}
                                    $failure = $stackevents | Sort-Object Timestamp -Descending | Select-Object -First 1
                                    Write-Error -Message $failure.ResourceStatusReason -TargetObject $failure -ErrorAction Stop
                                } else {
                                    Write-Warning ( '[{0}] Stack: {1} Status: {2}' -f [datetime]::Now.ToString('T'), $stack.StackName, $stack.StackStatus)
                                }
                            }
                            catch {
                                if ( $_.TargetObject.ResourceStatusReason -match "(:|:.)\[(?<res>\w{2,})\]" ) {
                                    $dependency = $stackevents | 
                                    Where-Object {$_.LogicalResourceId -eq $Matches['res']} | 
                                        Sort-Object -Descending Timestamp | 
                                            Select-Object -First 1

                                    #Notify of dependencies. Will add dependency rules as they come along.
                                    switch ($dependency.ResourceType) {
                                        'AWS::IAM::ROLE' { 
                                            $resource = Get-CFNStackResource -LogicalResourceId $dependency.LogicalResourceId -StackName $stack.StackName -Region $Region
                                            $role = Get-IAMRole -RoleName $resource.PhysicalResourceId
                                            $policies = Get-IAMRolePolicyList -RoleName $role.RoleName
                                            try {
                                                foreach ($policy in $policies) {
                                                    Remove-IAMRolePolicy -PolicyName $policy -RoleName $role.RoleName
                                                }
                                                Remove-IAMRole -RoleName $resource.PhysicalResourceId 
                                            }
                                            catch {
                                                Write-Warning $dependency.ResourceStatusReason
                                                Write-Warning $_.Exception.Message
                                            }
                                        }
                                        Default { 
                                            Write-Warning $dependency.ResourceStatusReason
                                        }
                                    }
                                            
                                } else {
                                    $_.TargetObject.ResourceStatusReason
                                }

                                $stack = $null
                                break
                            }
                            sleep -s 5
                        }
                        $deleteSuccess = $true    
                    }
                    catch {
                        if ( $_.Exception.Message -match "Stack.+\[.+\].+TerminationProtection.+enabled" ) {
                            try {
                                Update-CFNTerminationProtection -StackName $stack.StackName -EnableTerminationProtection $false -Force
                                Remove-CFNStack -StackName $stack.StackName -Region $Region -ErrorAction Stop
                                $deleteSuccess = $true
                            } catch {
                                $deleteSuccess = $false
                                Write-Warning $_.exception.Message
                            }
                        } elseif ( $failure.failure.ResourceStatusReason -match "(:|:.)\[(?<res>\w{2,})\]" ) {

                        } else {
                            $deleteSuccess = $false
                            Write-Warning $_.exception.Message
                        }
                    } finally {
                        $return.Add( [PSCustomObject][ordered]@{
                            Asset = $stack.StackName
                            Type = 'CFN Stack'
                            Region = $Region
                            isDeleted = $deleteSuccess
                        } ) | Out-Null
    
                        $stack = $null
                    }
                }
            }

    #delete VPC components 
    $vpcs = Get-EC2Vpc -Region $Region | Where-Object { -not $_.IsDefault }
    foreach ( $vpc in $vpcs ) {
        $splat = @{
            Filter = @{ Name='vpc-id';Values=$vpc.VpcId }
            Region = $Region
        }

        #Remove EC2 Instances tied to VPC
        $instances = ( Get-EC2Instance @splat ).Instances
        foreach ( $instance in $instances ) {
            if ($PSCmdlet.ShouldProcess($instance.InstanceId, "Terminate Instance:")) {
                try {
                    Remove-EC2Instance -InstanceId $instance.InstanceId -Region $Region | Out-Null
                    $deleteSuccess = $true
                }
                Catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $sg.GroupId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $instance.InstanceId
                        Type = 'EC2 Instance'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                } 
            }
        }
        #delete LBs in VPC
        $elbs = Get-ELBLoadBalancer -Region $Region | Where-Object { $_.VPCId -eq $vpc.VpcId }
        foreach ( $lb in $elbs ) {
            if ( $PSCmdlet.ShouldProcess( $lb.CanonicalHostedZoneName, 'Terminate Elastic Load Balancer') ) {
                try {
                    $lb | Remove-ELBLoadBalancer -Confirm:$false -Region $Region
                    $deleteSuccess = $true
                }
                catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $lb.CanonicalHostedZoneName, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $lb.CanonicalHostedZoneName
                        Type = 'ELB'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                }
            }
        }

        #delete NAT GWs
        $natGWs = Get-EC2NatGateway @splat
        foreach ( $nat in $natGWs ) {
            if ($PSCmdlet.ShouldProcess($nat.NatGatewayId, 'Delete NAT Gateway')) {
                try {
                    $nat | Remove-EC2NatGateway -Region $Region
                    $deleteSuccess = $true
                }
                catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $nat.NatGatewayId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $nat.NatGatewayId
                        Type = 'NAT'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                }
            }
        }

        #delete security groups relating to vpc
        $sgs = Get-EC2SecurityGroup @splat | Where-Object { $_.GroupName -ne 'default' }
        foreach ( $sg in $sgs ) {
            if ($PSCmdlet.ShouldProcess($sg.GroupId, 'Remove Security Group')) {
                try {
                    Remove-EC2SecurityGroup -Force -Region $Region -GroupId $sg.GroupId
                    $deleteSuccess = $true
                } 
                Catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $sg.GroupId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $sg.GroupId
                        Type = 'Security Group'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                }
            }
        }

        #delete Internet gateways related to VPC
        $igsplat = $splat.Clone()
        $igsplat.Filter = @{Name='attachment.vpc-id';Values=$vpc.VpcId}
        $internetGateways = Get-EC2InternetGateway @igsplat
        foreach ( $ig in $internetGateways ) {
            if ( $PSCmdlet.ShouldProcess($ig.InternetGatewayId, 'Remove InternetGateway') ) {
                try {
                    if ( $ig.Attachments ) {
                        Dismount-EC2InternetGateway -VpcId $vpc.VpcId -InternetGatewayId $ig.InternetGatewayId -Region $Region
                    }
    
                    Remove-EC2InternetGateway -InternetGatewayId $ig.InternetGatewayId -Force -Region $Region
                    $deleteSuccess = $true
                } 
                catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $ig.InternetGatewayId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $ig.InternetGatewayId
                        Type = 'Internet Gateway'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                }
            }
        }

        #delete route tables
        #to-do:
        #fix 'association.main' filter, which does not seem to properly filter out the main routing table...
        $rtsplat = $splat.Clone()
        $rtsplat.Filter = @(
            @{ Name = 'vpc-id'; Values=$vpc.VpcId }
            #,@{ Name = 'association.main'; Values='false'}
        )
        $routetables = Get-EC2RouteTable @rtsplat | Where-Object { -not $_.Associations.Main }
        foreach ( $rt in $routetables ) {
            if ($PSCmdlet.ShouldProcess($rt.RouteTableId, 'Remove RouteTable')) {
                try {
                    if ( $rt.Associations ) {
                        $rt.Associations | Where-Object { -not $_.Main } | ForEach-Object {
                            Unregister-EC2RouteTable -AssociationId $_.RouteTableAssociationId -Region $Region    
                        }
                    }
                    #$rt | Remove-EC2RouteTable -Region $Region
                    Remove-EC2RouteTable -Region $Region -RouteTableId $rt.RouteTableId -Force -ErrorAction Stop
                    $deleteSuccess = $true
                }
                catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $rt.RouteTableId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $rt.RouteTableId
                        Type = 'RouteTable'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null    
                }
            }
        }

        #Delete all non-default network ACLs
        $NetworkACLs = Get-EC2NetworkAcl @splat | Where-Object { -not $_.IsDefault }
        foreach ($acl in $NetworkACLs) {
            if ($PSCmdlet.ShouldProcess($acl.NetworkAclId, 'Delete Network ACL') ) {
                try {
                    $acl | Remove-EC2NetworkAcl -Region $Region
                    $deleteSuccess = $true
                }
                catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $acl.NetworkAclId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $acl.NetworkAclId
                        Type = 'ACL'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                }
            }
        }

        #delete subnets
        $vpcSubnets = Get-EC2Subnet @splat
        foreach ($subnet in $vpcSubnets) {
            if ($PSCmdlet.ShouldProcess($subnet.SubnetId, 'Remove Subnet') ) {
                try {
                    Remove-EC2Subnet -SubnetId $subnet.SubnetId -Region $Region
                    $deleteSuccess = $true
                }
                catch {
                    Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $subnet.SubnetId, $_.Exception.Message)
                    $deleteSuccess = $false
                }
                finally {
                    $return.Add( [PSCustomObject][ordered]@{
                        Asset = $subnet.SubnetId
                        Type = 'Subnet'
                        Region = $Region
                        isDeleted = $deleteSuccess
                    } ) | Out-Null
                }
            }
        }

        #delete VPC
        if ($PSCmdlet.ShouldProcess($vpc.VpcId, 'Delete VPC')) {
            try {
                Remove-EC2Vpc -Region $Region -VpcId $vpc.VpcId
                $deleteSuccess = $true
            }
            catch {
                Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $vpc.VpcId, $_.Exception.Message)
                $deleteSuccess = $false
            }
            finally {
                $return.Add( [PSCustomObject][ordered]@{
                    Asset = $vpc.VpcId
                    Type = 'VPC'
                    Region = $Region
                    isDeleted = $deleteSuccess
                } ) | Out-Null
            }
        }
    }

    #Delete S3 Buckets
    $s3buckets = Get-S3Bucket | Where-Object { $_.BucketName -notmatch "(^rackspace|logs$)" -and ($_|Get-S3BucketLocation).Value -eq $Region}
    foreach ( $bucket in $s3buckets ) {
        if ($PSCmdlet.ShouldProcess($bucket.BucketName, 'Remove S3 Bucket') ) {
            try {
                Remove-S3BucketPolicy -BucketName $bucket.BucketName
                Remove-S3Bucket -DeleteBucketContent -BucketName $bucket.BucketName
                Write-Warning "Asset Deleted: $($bucket.BucketName)"
                $deleteSuccess = $true
            }
            catch {
                Write-Warning ( 'Asset Delete Failed: {0} - {1}' -f $bucket.BucketName, $_.Exception.Message)
            }
            finally {
                $return.Add( [PSCustomObject][ordered]@{
                    Asset = $bucket.BucketName
                    Type = 'S3Bucket'
                    Region = $Region
                    isDeleted = $deleteSuccess
                } ) | Out-Null    
            }
        }
    }
}
return $return