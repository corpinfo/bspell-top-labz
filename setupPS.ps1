Install-Module 'AWS.Tools.Common','AWS.Tools.Installer'

$awsModules = @(
    'ec2',
    's3',
    'securitytoken',
    'cloudformation',
    'cloudwatch',
    'identitymanagement',
    'rds',
    'route53',
    'ecr',
    'lambda',
    'autoscaling',
    'codedeploy',
    'cloudwatch',
    'elasticloadbalancing',
    'keymanagementservice',
    'sqs',
    'ecs',
    'dynamodbv2',
    'cloudfront',
    'glacier',
    'athena',
    'redshift',
    'ebs',
    'neptune',
    'NetworkFirewall'
)
  
Install-AWSToolsModule -Name $awsModules -Force