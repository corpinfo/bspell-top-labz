Set-jConf -rackspaceCloudAccountNumber 1337809 -AWSAccountNumber 757506681371
<#
$MyDocPath  = [Environment]::GetFolderPath("MyDocuments")
if ( $PSVersionTable.PSVersion -gt [version]'6.0.0' ) {
    $UserModulePath = Join-Path $MyDocPath 'PowerShell\Modules'
} else {
    $UserModulePath = Join-Path $MyDocPath 'WindowsPowerShell\Modules'
}
$source = $pwd.Path

Copy-Item -Path $source -Destination $UserModulePath -Force -Recurse
ipmo JanusPS -Force

<# Set Default Account to work with
Set-jConf -rackspaceCloudAccountNumber 1337809 -AWSAccountNumber 757506681371
Set-jConf -rackspaceCloudAccountNumber '' -AWSAccountNumber ''
Set-jConf -RXTID '1337809' -AWSID '757506681371'
Set-jConf -Browser chrome
Set-jconf -Browser firefox
ls env:\j*
Clear-JanusConfig -Configuration RunningConfig
Clear-JanusConfig -Configuration DefaultConfig
Get-JanusDefaultConfig
#>
#Get-JanusAccount -DefaultRXTID -DefaultAWSID | Start-JanusAWSConsole

#Set-JanusConfig -defaultAccountEnabled $true

#$processes = Get-Process -Name "Appgate SDP*"
#Get-jAccount
#Add-PoshSecret -Name AWSCredential -Property @{session=$credentials} -Expiry $credentials.credential.expiration -PlaintextPassword AWSCredential -Username AWSCredential -ErrorAction Stop
#$secret = Get-PoshSecret -Name AWSCredential -Username AWSCredential -Verbose
Get-jCred -Linux -Flush -Verbose
Get-jCred -CredentialFile
Get-jCred -Flush


Get-jAccount -AWSID $null | ForEach-Object {
    
}

$instance = (Get-ASAutoScalingInstance -Region us-west-1)[0].InstanceId

.\Get-EC2WindowsCred.ps1 -Region us-west-1 -InstanceID $instance -ConnectRDP

(Get-ASAutoScalingInstance -Region us-west-1)[0] | .\Get-EC2WindowsCred.ps1 -Region us-west-1 -ConnectPSSession