Param(
    $Region = 'us-west-2',

    [ValidateSet('AmazonLinux2','Ubuntu','Windows2019')]
    $OS,

    [int]
    $InstanceCount = 1
)
#region Define Variables
$splat = @{
    ProfileName = $null
    Region = $Region
}

$tagSplat = $splat.Clone()
$tagSplat['Tag'] = [amazon.ec2.model.tag]::new('Name','TOP-Lab')
$tagSplat['Resource'] = $null

$availabilityZones = Get-EC2AvailabilityZone @splat
$myAZ = $availabilityZones[0]

#endregion VAR

#region VPC
$vpcSplat = $splat.Clone()
$vpcSplat['CidrBlock'] = "10.0.0.0/16"
$MyVPC = New-EC2Vpc @vpcSplat

#tag it
$tagSplat.Resource = $MyVPC.VpcId
New-EC2Tag @tagSplat

#endregion VPC


#region Subnet
 
## Create a Subnet
$vpcSubnet = $splat.Clone()
$vpcSubnet['VpcId'] = $MyVPC.VpcId
$vpcSubnet['CidrBlock'] = '10.0.1.0/24'
$vpcSubnet['AvailabilityZoneId'] = $myAZ.ZoneId
$MySubnet = New-EC2Subnet @vpcSubnet

#tag it
$tagSplat.Resource = $MySubnet.SubnetId
New-EC2Tag @tagSplat


## Enable Auto-assign Public IP on the Subnet
Edit-EC2SubnetAttribute -SubnetId $MySubnet.SubnetId -MapPublicIpOnLaunch $true @splat
 #endregion subnet

 #region InternetGateway
## Create an Internet Gateway
$igSplat = $splat.Clone()
$MyInternetGateway = New-EC2InternetGateway @igSplat

#tag it
$tagSplat.Resource = $MyInternetGateway.InternetGatewayId
New-EC2Tag @tagSplat

## Attach Internet gateway to your VPC
$igSplat['VpcId'] = $MyVPC.VpcId
$igSplat['InternetGatewayID'] = $MyInternetGateway.InternetGatewayId

Add-EC2InternetGateway @igSplat | Out-Null
#endregion IG

#region Routing Table
## Create a route table
$rtSplat = $splat.Clone()
$rtSplat['VpcId'] = $MyVPC.VpcId
$MyRouteTable = New-EC2RouteTable @rtSplat

#tag it
$tagSplat.Resource = $MyRouteTable.RouteTableId
New-EC2Tag @tagSplat

## Create route to Internet Gateway
$rtSplat.Remove('VpcId')
$rtSplat['RouteTableId'] = $MyRouteTable.RouteTableId
$rtSplat['DestinationCidrBlock'] = '0.0.0.0/0'
$rtSplat['GatewayId'] = $MyInternetGateway.InternetGatewayId
New-EC2Route @rtSplat | Out-Null
 
## Associate the public subnet with route table
$registerTable = $splat.Clone()
$registerTable['SubnetID'] = $MySubnet.SubnetId
$registerTable['RouteTableId'] = $MyRouteTable.RouteTableId

Register-EC2RouteTable @registerTable | Out-Null
#endregion RT

#region Security Group
 
## Create a security group
$sgSplat = $splat.Clone()
$sgSplat['GroupName'] = 'LabSG2'
$sgSplat['Description'] = 'Used for Lab setup stuff'
$sgSplat['VpcId'] = $MyVPC.VpcId

$mysgID = New-EC2SecurityGroup @sgSplat
$mysg = Get-EC2SecurityGroup -GroupId $mysgID @splat

#tag it
$tagSplat.Resource = $mysg.GroupId
New-EC2Tag @tagSplat 

## Create security group ingress rules
$myIp = Invoke-RestMethod 'https://icanhazip.com'
$sgruleSplat = $splat.Clone()
$sgruleSplat['GroupID'] = $mysg.GroupId
$sgruleSplat['IpPermission'] = New-Object Amazon.EC2.Model.IpPermission
$sgruleSplat.IpPermission.IpProtocol = "tcp"
$sgruleSplat.IpPermission.ToPort = 22
$sgruleSplat.IpPermission.FromPort = 22
$sgruleSplat.IpPermission.Ipv4Ranges = [Amazon.Ec2.Model.IpRange]::new()
$sgruleSplat.IpPermission.Ipv4Ranges[0].CidrIp = -join ($myIp.Trim(), '/32')
$sgruleSplat.IpPermission.Ipv4Ranges[0].Description = 'My Local IP Address'

Grant-EC2SecurityGroupIngress @sgruleSplat

#endregion

#region KeyPair

$keySplat = $splat.Clone()
$keySplat['KeyName'] = 'MyVPCLab-KeyPair'
$priKey = "$home\.ssh\id_rsa"
$pubKey = "$home\.ssh\id_rsa.pub"
try {
    $keyPair = Get-EC2KeyPair @keySplat -ErrorAction Stop
}
catch {
    if ( ( Test-Path $priKey ) -and ( Test-Path $pubKey ) ) {
        $keySplat['PublicKey'] = [io.file]::ReadAllText( $pubKey )
        $keyPair = Import-EC2KeyPair @keySplat
        Write-Warning "SSH key Imported:"
    } else {
        #to-do:
        # find a way to match md5 of uploaded key to what is on localmachine
        # and only create new key if checksums do not match
        $keySplat['KeyName']
        $keyPair = New-EC2KeyPair @keySplat
        Write-Warning 'New Key Created:'
        $keyPair.KeyMaterial | Out-File -Encoding ascii -FilePath "$home\.ssh\MyVPCLab-KeyPair" -Force
    }
}
finally {
    Write-Verbose "$($keyPair|Out-String)" -Verbose
}

if ( ( Test-Path $priKey ) -and ( Test-Path $pubKey ) ) {
    if ( -not $keyPair ) {
        $keySplat['PublicKey'] = [io.file]::ReadAllText( $pubKey )
        $keyPair = Import-EC2KeyPair @keySplat
    }
} else {
}

#tag it
$tagSplat.Resource = $keyPair.KeyPairId
New-EC2Tag @tagSplat

#endregion

#region EC2 Instance Setup

## Retrieve Amazon Linux 2 AMI
$amiSplat = $splat.Clone()
$amiSplat['Filter'] = @(
    @{Name = 'state';Values = 'available'},
    @{Name = 'owner-alias';Values = 'amazon'},
    @{Name="description"; Values="Amazon Linux 2 AMI 2.0.*HVM gp2"}
    #@{Name = 'name';Values= 'Windows_Server-2019-English-Full-Base-*'}
)
$ami = Get-EC2Image @amiSplat
$image = $ami | Sort-Object CreationDate -Descending | Select-Object -First 1
 
## Create new EC2 instance
$ec2Splat = $splat.Clone()
$ec2Splat['ImageId'] = $image.ImageId
$ec2Splat['AssociatePublicIp'] = $true
$ec2Splat['InstanceType'] = 't2.micro'
$ec2Splat['KeyName'] = $keyPair.KeyName
#$ec2Splat['PrivateIpAddress'] = '10.0.1.10'
$ec2Splat['SecurityGroupId'] = $mysg.GroupId
$ec2Splat['SubnetId'] = $MySubnet.SubnetId
if ( $OS -match 'Windows' ) {
    $ec2Splat['UserDataFile'] = (Get-Item .\UserData.ps1).FullName
}

1..$InstanceCount | ForEach-Object {
    New-EC2Instance @ec2Splat | ForEach-Object {
        $tagSplat.Resources = $_.Instances[0].Resource
        New-EC2Tag @tagSplat
    }    
}

## Get EC2 Instance Details
$ec2Splat = $splat.Clone()
$ec2Splat['Filter'] = @{Name = 'reservation-id';Values = $MyEc2Instance.ReservationId}
$MyEc2InstanceDetails = Get-EC2Instance @ec2Splat

Write-Output $MyEc2InstanceDetails.Instances

#endregion
