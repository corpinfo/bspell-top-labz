function Build-JanusRestParameters {
    [CmdletBinding()]
    [Alias('Build-jParam')]
    param (
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose ( '[{0}] Beginning Function' -f $function )
    }
    process {
        $script:varSplat = @{
            Name = 'JanusReSTSplat'
            Scope = 'Global'
            ErrorAction = 'Stop'
        }
        
        try {
            Write-Warning "Authenticating SSO. Auth Type: 'nscli-auth'"
            $script:token = Get-RackerToken -AsXAuthToken -ErrorAction Stop
            
            Write-Verbose ( '[{0}] Token Generated.' -f $function )
        }
        catch {
            Write-Warning $_.Exception.Message
        }

        try {
            $script:jsplat = Get-Variable @script:varSplat -ValueOnly -ErrorAction Stop
            $script:jsplat.Headers.'X-Auth-Token' = $script:token
        }
        catch {
            $script:jsplat = @{
                Headers = @{
                    'X-Auth-Token' = $script:token
                    'X-Tenant-Id' = $env:JanusDefaultRackspaceCloudAccount
                }
                Uri = $env:JanusAPIEndpoint
                Method = 'Get'
                ContentType = 'application/json'
                ErrorAction = 'Stop'
                Verbose = $false
            }
        }
        
        $script:varSplat['Value'] = $script:jsplat
        $script:varSplat['Description'] = 'Janus API Config for web requests'
        $script:varSplat['Force'] = $true
        
        try {
            Set-Variable @script:varSplat
        }
        catch {
            Write-Warning $_.Exception.Message
        }
        Write-Verbose ( '[{0}] End Function' -f $function )
    }
}

