
function Get-JanusRestParameters {
    [CmdletBinding()]
    [Alias('Get-jParam')]
    param ()
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose ( '[{0}] Beginning Function' -f $function )
    }
    process {
        #Build the Parameter to ensure consistency
        Build-JanusRestParameters

        #return variable
        try {
            $return = ( Get-Variable -Name JanusReSTSplat -ValueOnly -ErrorAction Stop -Scope Global ).clone()
        }
        catch {
            Write-Warning $_.Exception.Message
        }
        Write-Output $return
        Write-Verbose ( '[{0}] End Function' -f $function )
    }
}
