function Set-JanusRestParameters {
    [CmdletBinding()]
    [Alias('Set-jParam')]
    param (
        [switch]
        $PassThru
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose ( '{0}: Beginning Function' -f $function )
    }
    process {
        Write-Verbose ( '{0}: Powershell Version: {1}' -f $function, $PSVersionTable.PSVersion.Major )
        try {
            $private:token = Get-RackerToken -AsXAuthToken -ErrorAction Stop
            
            Write-Verbose ( '{0}: Token Generated.' -f $function )
        }
        catch {
            Write-Warning $_.Exception.Message
        }
        $private:splat = @{
            Name = 'JanusConfig'
            Value = @{
                Headers = @{
                    'X-Auth-Token' = $private:token
                    'X-Tenant-Id' = $env:JanusDefaultRackspaceCloudAccount
                }
                Uri = $env:JanusAPIEndpoint
                Method = 'Get'
                ContentType = 'application/json'
                ErrorAction = 'Stop'
                Verbose = $false
            }
            Scope = 'Global'
            Description = 'Janus API Config for web requests'
            Force = $true
            Visibility = 'private' #should be private. Set to public for troubleshooting.
        }
        try {
            Set-Variable @private:splat -ErrorAction Stop
            if ( $PassThru ) {
                Get-Variable -Name JanusConfig -ValueOnly -ErrorAction Stop -Scope Global
            }
        }
        catch {
            Write-Warning $_.Exception.Message
        }
        Write-Verbose ( '{0}: End Function' -f $function )
    }
}

