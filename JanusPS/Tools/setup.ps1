#requires -Module PowerShellGet

[CmdletBinding()]
[System.Diagnostics.CodeAnalysis.SuppressMessage('PSAvoidUsingWriteHost', '')]
param()

# PowerShell 5.1 and bellow need the PSGallery to be intialized
$gallery = Get-PSRepository -Name PSGallery -ErrorAction SilentlyContinue
if (-not $gallery) {
    Write-Verbose "Installing PackageProvider NuGet"
    $null = Install-PackageProvider -Name NuGet -Force -ErrorAction SilentlyContinue
}

# Update PowerShellGet if needed
if ((Get-Module PowershellGet -ListAvailable)[0].Version -lt [version]"1.6.0") {
    Write-Verbose "Updating PowershellGet"
    Install-Module PowershellGet -Scope CurrentUser -Force
}

Write-Verbose "Installing Dependencies"

$DocumentsPath  = [Environment]::GetFolderPath("MyDocuments")
if ( $PSVersionTable.PSVersion -gt [version]'6.0.0' ) {
    $UserModulePath = Join-Path $DocumentsPath 'PowerShell\Modules'
} else {
    $UserModulePath = Join-Path $DocumentsPath 'WindowsPowerShell\Modules'
}
try {
    #Install Locally available depencies
    foreach ( $Module in @('Tokx','PoshSecret') ) {
        $requiredModule = Import-Module "$PSScriptRoot\$Module" -Force -PassThru
        $existingModule = Import-Module -Name "$UserModulePath\$Module" -Force -PassThru -ErrorAction SilentlyContinue

        Write-Verbose "Installing Module: $Module"
        if ( $existingModule.Version -ne $requiredModule.version ) {
            Remove-Module $Module -Force -ErrorAction SilentlyContinue
            Remove-Item "$UserModulePath\$Module" -Recurse -Force -ErrorAction SilentlyContinue
            Copy-Item "$PSScriptRoot\$Module" -Destination $UserModulePath -Recurse -Force -ErrorAction SilentlyContinue
            Import-Module -Name $Module -Force
        }
        Write-Verbose "Module Installed: $Module"
    }

    #Install Dependencies from Powershell gallery:
    #todo: update 
    $awsmonalith = Get-Module -Name AWSPowershell.Netcore -ListAvailable
    if ( $awsmonalith ) {
        Write-Warning "Non-Modular AWS Module detected. While this works, it is slow, and will likely require you to install `
        you to set 'Import-Module -Name AWSPowershell.Netcore' at the beginning of your profile to successfully load saved credentials.
        To get around this, remove the module, and install the Modular AWS.Tools Module
        "
    } elseif ( Get-Module -Name AWS.Tools.Common ) {
        Write-Host -ForegroundColor Green 'Module AWS Module Detected. Thanks for being efficient.'
        continue
    } else {
        Install-Module -Name AWS.Tools.Common
    }

} catch {
    Write-Warning $_.Exception.Message
}
Write-Verbose 'Setup Complete'

Install-Module -Name AWS.Tools.Common,AWS.Tools.Installer