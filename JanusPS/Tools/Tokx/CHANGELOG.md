﻿# Tokx Release History

## 3.2.0.0

- Migrate from ADFS to Azure AD
- Deprecation warning on Invoke-Query is only shown to members of GTSWAE

## 3.1.3.0

- Migrate unit tests to Pester 5

## 3.1.2.0

- Do not overwrite `X-Auth-Token` if provided in `Invoke-RestMethodWithAuth`

## 3.1.1.0

- Fix bug where `-ErrorAction` could get passed through to `Invoke-RestMethod` more than once

## 3.1.0.0

- Remove logic to redirect user to `lon.identity-intenal` in `Get-AuthEndpoint`
- Add retry to Get-RackerToken
- Add `-RetryCount` and `-BackoffSeconds` parameters to `Invoke-RestMethodWithAuth`

## 3.0.1.0

- Require PoshSecret 1.1.9.0
- Add deprecation warning to Invoke-Query
- Get-PoshCoreToken supports Core staging environment
  - Requires access to PasswordSafe project 3514
- Get-RackerToken has -AsXAuthToken switch

## 3.0.0.0

- Implement browser-based single-sign-on
- Get-RackerToken uses browser-based single-sign-on by default
- Removed -RelyingParty from Get-SamlToken (RelyingParty is always "tokx")
- Various non-breaking changes to enable future cross-platform support

## 2.4.0.0

- Added new functions to support Intensive credential rotation and customer access using the self-service Intensive API
  - Get-IntCustomerAccess
  - Get-IntPassword
  - Get-IntUsername
  - Grant-IntCustomerAccess
  - Set-IntUsername
  - Test-IntCustomerUsername

## 2.3.0.0

- Require PoshSecret 1.1.8.0, bumping per GTSWAE-5738

## 2.2.0.0

- Remove FireEngine dependency in Get-SsoCredential

## 2.1.0.0

- Adding function Get-SecretServerToken

## 2.0.2.0

- Fix wrong output when RackerToken expired and $RackerToken exists in parent scope

## 2.0.1.0

- Removed deprecation warning on Invoke-Query

## 2.0.0.0

- Deprecated Invoke-Query
- Replaced it with Invoke-RestMethodWithAuth

## 1.6.2.0

- Update jenkinsfile to 1.6.0.0

## 1.6.1.0

- Added -Staging switch to Invoke-Query

## 1.6.0.1

- Fixed bug in Get-SsoCredential when password was invalid

## 1.6.0.0

- Added Get-SsoCredential

## 1.5.3.0

- Fix LibCurl import error

## 1.5.2.0

- Update to support module optimisation

## 1.5.1.0

- Updating Jenkinsfile to publish module to Artifactory

## 1.5.0.0

- Removed function New-RackerToken
- Get-RackerToken now supports fetching token with PSCredential instead of RSA passcode

## 1.4.7.2

- Update regex validation on RSA token to accept special characters

## 1.4.7.1

- Updating dependencies on poshsecret to 1.1.5.2

## 1.4.7.0

- Removed Get-AuthEndpoint -Internal switch as it's deprecated, and moved to the -Endpoint switch

## 1.4.6.1

- Require PoshSecret 1.1.4.0
- Get-SamlToken: Fixed "Error setting CURLOPT_WRITEFUNCTION to LibCurlSharp.Easy+WriteFunction: CURLE_BAD_FUNCTION_ARGUMENT"

## 1.4.6.0

- Moved `.\README.md` to `.\docs\README.md` and replaced `.\docs\index.md`
- Updated README.md (new pages index page) with Installation, Help and Change Log sections

## 1.4.5.0

- Adding updated PSSA exception and jenkinsfile

## 1.4.4.0

- Adding updated PSSA exception and VsCode function

## 1.4.3.0

- PSSA compliance
