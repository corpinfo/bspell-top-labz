class NoHandlerForHttp : InvalidOperationException
{
    NoHandlerForHttp ([string]$Message) : base($Message)
    {
    }

    NoHandlerForHttp ([string]$Message, [Exception]$InnerException) : base($Message, $InnerException)
    {
    }
}
