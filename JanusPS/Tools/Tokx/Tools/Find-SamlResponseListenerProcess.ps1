function Find-SamlResponseListenerProcess
{
    <#
        .SYNOPSIS
        Finds a process listening with HTTP.sys.

        .DESCRIPTION
        Finds a process listening with HTTP.sys, so that it can be force-killed.

        When you use netstat to find an HttpListener, you get PID 4 - "System" - which is unhelpful.
        This command finds the process which has registered a particular endpoint with HTTP.sys.

        .PARAMETER ListenerUri
        The URI on which the process is listening.

        .OUTPUTS
        [ProcessInfo]

        .EXAMPLE
        Find-SamlResponseListenerProcess

        Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
        -------  ------    -----      -----     ------     --  -- -----------
            580      28    56952      74992       1.22  48020   1 powershell

        Finds a process listening with HTTP.sys.

        .EXAMPLE
        Find-SamlResponseListenerProcess -ListenerUri http://127.0.0.1:29004 | Stop-Process

        Finds and stops a process listening with HTTP.sys.

        .NOTES
        HttpListener is a pain; should have gone with Pode...
    #>

    [OutputType([void])]
    [CmdletBinding()]
    param
    (
        [Parameter()]
        [uri]$ListenerUri = $(& (Get-Module Tokx) {$Script:SAML_LISTENER_URI})
    )


    if (-not (Get-Command netsh))  # don't assume Windows
    {
        throw "Unable to find netsh. This command only supports Windows."
    }


    $QueueText = netsh http show servicestate view=requestq verbose=yes | Out-String            # Shows processes that have registered URLs with HTTP.sys
    $Queues    = $QueueText -split '(?<=\n)(?=Request queue name)' | Select-Object -Skip 1

    $Queue     = @($Queues) -match [regex]::Escape($ListenerUri -replace '/$')

    if (-not $Queue)
    {
        throw "No request queue listening on '$ListenerUri' was found in HTTP.sys."
    }

    if ($Queue.Count -gt 1)
    {
        throw "Multiple request queues listening on '$ListenerUri' were found in HTTP.sys."
    }

    $ProcessId = [string]$Queue -replace '(?s).*Process IDs:\s+' -replace '(?s)\s.*URL group.*' -split '\s+' |
        ForEach-Object {[int]($_.Trim())}


    Get-Process -Id $ProcessId -ErrorAction Stop
}
