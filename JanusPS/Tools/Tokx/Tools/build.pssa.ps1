# The ProjectRoot is where you keep all your Repositories: c:\GithubData for most of us.
$ProjectRoot = Split-Path -Path $PSScriptRoot -Parent

# ProjectPath is the path of the current module
$ProjectPath = Split-Path -Path $ProjectRoot -Parent


# Provide the list of pf dependent modules to be imported for running WaePSSA
Import-Module PSScriptAnalyzer -Global -MinimumVersion 1.17.1 -Force

$DependentRepoName = @( 'PoshSecret', 'PoshGithub' , 'WaeStd')

$DependentRepoName | ForEach-Object {
    Import-Module $([io.path]::Combine($ProjectPath, $PSItem, "$PSItem.psd1")) -Force
}

Invoke-WaeVSCodePssa -ProjectRoot $ProjectRoot

###
