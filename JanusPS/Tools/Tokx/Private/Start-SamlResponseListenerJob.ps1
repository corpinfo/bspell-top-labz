function Start-SamlResponseListenerJob
{
    <#
        .SYNOPSIS
        Starts an HTTP listener that handles one request and then shuts down.

        .DESCRIPTION
        Starts an HTTP listener that handles one request and then shuts down.

        .PARAMETER ListenerUri
        The URI on which to listen.

        .PARAMETER RedirectOnSuccessUri
        The URI to redirect the browser to on receiving a valid SAML response.

        .PARAMETER RedirectOnFailUri
        The URI to redirect the browser to when the response is not a valid SAML response.

        .OUTPUTS
        [Management.Automation.Job]

        .EXAMPLE
        Start-SamlResponseListenerJob


        Id     Name            PSJobTypeName   State         HasMoreData     Location             Command
        --     ----            -------------   -----         -----------     --------             -------
        21     SamlResponse... BackgroundJob   Running       True            localhost            ...


        Starts an HTTP listener that handles one request and then shuts down.
    #>

    [OutputType([Management.Automation.Job])]
    [CmdletBinding()]
    param
    (
        [Parameter()]
        [uri]$ListenerUri = $Script:SAML_LISTENER_URI,

        [Parameter()]
        [uri]$RedirectOnSuccessUri = $Script:ADFS_SUCCESS_PAGE_URI,

        [Parameter()]
        [uri]$RedirectOnFailUri = $Script:ADFS_FAILURE_PAGE_URI
    )

    Start-Job -Name SamlResponseListener {

        $ErrorActionPreference = "Stop"


        $Listener = [Net.HttpListener]::new()

        $Listener.Prefixes.Add($using:ListenerUri)
        $Listener.Start()
        Write-Verbose "Listening on $ListenerUri ..."

        try
        {
            [Net.HttpListenerContext]$Context = $Listener.GetContext()
            [Net.HttpListenerRequest]$Request = $Context.Request
            [Net.HttpListenerResponse]$Response = $Context.Response
            Write-Verbose "Received request on $($Request.Url) ..."


            if ($Request.RawUrl -match 'stop|close|end|kill')
            {
                $Buffer = [Text.Encoding]::UTF8.GetBytes("Stopping HttpListener")
                $Response.ContentLength64 = $Buffer.Length
                $Response.OutputStream.Write($Buffer, 0, $Buffer.Length)
                $Response.OutputStream.Close()
                $Response.Close()

                $Message = "The HttpListener was stopped by a request to '$($Request.RawUrl)'."
                throw [OperationCanceledException]::new($Message)
            }


            if ($Request.HttpMethod -eq 'POST')
            {
                Add-Type -AssemblyName System.Web
                $FormContent = [IO.StreamReader]::new($Request.InputStream).ReadToEnd()
                $FormData = [Web.HttpUtility]::ParseQueryString($FormContent)


                $SamlToken = $FormData["SAMLResponse"]
                if ($SamlToken)
                {
                    $Response.Redirect($using:RedirectOnSuccessUri)
                    $Response.Close()

                    Write-Verbose "Received SAML token."
                    return $SamlToken

                }
                else
                {
                    Write-Verbose "POST did not contain SAML response."
                }
            }
            else
            {
                Write-Verbose "Request was of wrong type: $($Request.HttpMethod)."
            }


            $Response.Redirect($using:RedirectOnFailUri)
            $Response.Close()

            throw "Unable to parse a SAML token out of the ADFS response. The data was: $FormContent"

        }

        finally
        {
            Write-Verbose "Stopping HttpListener..."
            $Listener.Stop()
            $Listener.Dispose()
            Write-Verbose "...stopped."
        }
    }
}
