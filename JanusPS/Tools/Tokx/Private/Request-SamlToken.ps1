function Request-SamlToken
{
    <#
        .SYNOPSIS
        Uses a browser-based authentication flow to obtain a Base64-encoded SAML token.

        .DESCRIPTION
        Uses a browser-based authentication flow to obtain a Base64-encoded SAML token.

        .PARAMETER RelyingParty
        The name of the application owning the authentication flow. This corresponds to the ID in
        the ServiceNow App Catalog.

        This parameter is case-sensitive.

        .PARAMETER ListenerUri
        The URI on which to listen.

        .PARAMETER RedirectOnSuccessUri
        The URI to redirect the browser to on receiving a valid SAML response.

        .PARAMETER RedirectOnFailUri
        The URI to redirect the browser to when the response is not a valid SAML response.

        .OUTPUTS
        [string]

        .EXAMPLE
        Request-SamlToken


        O29qaG5kbGlvbkAuI11qUmU9YXM7ajsncWhqYiBhc2JhJ25jJ2FjU0ZtLE4yaCAyMyB1LTNdZGYgbjNqZDBiXGEsamhz
        YnZkYXNkWzA5dWRxbnVxMzJubmRlYXdmZ3EteWhvamhuZGxpb25ALiNdalJlPWFzO2o7J3FoamIgYXNiYSduYydhY1NG
        bSxOMmggMjMgdS0zXWRm...


        Uses a browser-based authentication flow to obtain a Base64-encoded SAML token.

        .NOTES
        Callers should handle this error:

        - Failed to listen on prefix 'http://127.0.0.1:29003/' because it conflicts with an existing
          registration on the machine.

        Remove-SamlResponseListenerJob should handle this. There is also
        Find-SamlResponseListenerProcess in the Tools folder.
    #>

    [OutputType([Management.Automation.Job])]
    [CmdletBinding()]
    param
    (
        [Parameter()]
        [string]$RelyingParty = "tokx",

        [Parameter()]
        [uri]$ListenerUri = $Script:SAML_LISTENER_URI,

        [Parameter()]
        [uri]$RedirectOnSuccessUri = $Script:ADFS_SUCCESS_PAGE_URI,

        [Parameter()]
        [uri]$RedirectOnFailUri = $Script:ADFS_FAILURE_PAGE_URI
    )


    $ErrorActionPreference = "Stop"


    $ListenerSplat = @{
        ListenerUri          = $ListenerUri
        RedirectOnSuccessUri = $RedirectOnSuccessUri
        RedirectOnFailUri    = $RedirectOnFailUri
    }
    $Job = Start-SamlResponseListenerJob @ListenerSplat


    # Open the browser; the user may need to auth there
    try
    {
        $ProgressPreference = 'SilentlyContinue'
        $StsUri = "https://myapps.microsoft.com/signin/5bb2cb8e-3ce2-4093-80ed-5b8ecaa004da?tenantId=570057f4-73ef-41c8-bcbb-08db2fc15c2b"
        Start-Process $StsUri -ErrorAction Stop
    }
    catch
    {
        Remove-SamlResponseListenerJob -ListenerUri $ListenerUri

        if ($_ -match 'No such file or directory')
        {
            throw [NoHandlerForHttp]::new(
                "Failed to find a handler for '$StsUri'. This is usually caused by running on a headless OS with no protocol handler for HTTP.",
                $_.InnerException
            )
        }
        throw
    }



    try
    {
        # Receive and return the SAML token
        $Job | Wait-Job | Receive-Job | Write-Output

    }

    catch
    {
        if ($_ -match "conflicts with an existing registration")
        {
            Remove-SamlResponseListenerJob -ListenerUri $ListenerUri
            Start-Sleep 1

            $RecursionStackFrames = Get-PSCallStack | Where-Object {$_.Name -eq $MyInvocation.MyCommand.Name}
            if ($RecursionStackFrames.Count -le 2)
            {
                Request-SamlToken @PSBoundParameters
            }

        }
        else
        {
            throw
        }
    }

    finally
    {
        Remove-SamlResponseListenerJob -ListenerUri $ListenerUri
    }
}
