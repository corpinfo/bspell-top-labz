function Remove-SamlResponseListenerJob
{
    <#
        .SYNOPSIS
        Removes all SamlResponseListener jobs.

        .DESCRIPTION
        Removes all SamlResponseListener jobs.

        Attempts to stop them gracefully by hitting the 'stop' endpoint - if this fails to stop the
        HttpListener, Remove-Job can take a long time to force-stop them.

        .PARAMETER ListenerUri
        The URI on which the job is listening.

        .OUTPUTS
        [void]

        .EXAMPLE
        Remove-SamlResponseListenerJob

        Removes all SamlResponseListener jobs.
    #>

    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingEmptyCatchBlock", "")]

    [OutputType([void])]
    [CmdletBinding()]
    param
    (
        [Parameter()]
        [uri]$ListenerUri = $Script:SAML_LISTENER_URI
    )

    $Jobs = Get-Job SamlResponseListener -ErrorAction SilentlyContinue
    if (-not $Jobs)
    {
        return
    }

    # URI that stops the HttpListener
    # It is not keen to shut down, and force-stopping jobs can hang the terminal for some time.
    # Much better when it stops gracefully.
    $StopUri = $ListenerUri -replace '/$' -replace '$', '/stop'

    try
    {
        $null = Invoke-WebRequest $StopUri -ErrorAction Stop
        Start-Sleep -Milliseconds 25
    }
    catch {}

    $Jobs | Remove-Job -Force
}
