function Invoke-SecretServerToken
{
    <#
        .SYNOPSIS
        Gets the secret server token from SecretServer

        .DESCRIPTION
        Gets the secret server token from SecretServer
        Either using ServiceAccount and domain as 'Local' with Authenticate method
        Or using a Racker SSO, RSA and domain as 'RACKSPACE' with AuthenticateRADIUS method
        Each method returns the related token

        .PARAMETER Credential
        Provide a PSCredential object containing SSO username and password

        .PARAMETER RsaToken
        The user's RSA token

        .OUTPUTS
        [string]

        .EXAMPLE
        Invoke-SecretServerToken -Credential $Credential

        Returns the token for the ServiceAccount provided

        .EXAMPLE
        Invoke-SecretServerToken -Credential $Credential -RsaToken $RsaToken

        Returns the token for the SSO user provided.
    #>
    [CmdletBinding()]
    [OutputType([string])]
    param
    (
        [Parameter(Position = 0, Mandatory = $true)]
        [System.Management.Automation.Credential()]
        [pscredential]$Credential,

        [Parameter(Position = 1)]
        [System.Security.SecureString]$RsaToken
    )

    Process
    {
        $Proxy = New-WebServiceProxy -uri $Script:TOKX_SECRETSERVER_PROD_ENDPOINT -UseDefaultCredential

        If ($RsaToken)
        {
            $Domain = 'RACKSPACE'
            $AuthenticateResult = $Proxy.AuthenticateRADIUS(
                $($Credential.username),
                [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($($Credential.password))),
                '',
                $Domain,
                [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($RsaToken))
            )
        }
        else
        {
            $Domain = 'Local'
            $AuthenticateResult = $Proxy.Authenticate(
                $($Credential.username),
                [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($($Credential.password))),
                '',
                $Domain
            )
        }

        If ($AuthenticateResult.Errors)
        {
            Write-Error "Error getting new SecretServer token ($($AuthenticateResult.Errors -join ','))"
            return
        }

        return $AuthenticateResult.token
    }
}
