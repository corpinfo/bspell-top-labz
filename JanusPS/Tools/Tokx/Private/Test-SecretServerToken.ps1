function Test-SecretServerToken
{
    <#
        .SYNOPSIS
        Validates a SecretServer token

        .DESCRIPTION
        Validates a SecretServer token. Returns true if token is still valid

        .PARAMETER SecretServerToken
        SecretServer Token

        .OUTPUTS
        [bool]

        .EXAMPLE
        Test-SecretServerToken

        True

        Returns true if token is still valid
    #>
    [CmdletBinding()]
    [OutputType([bool])]
    param
    (
        [Parameter(Position = 0, Mandatory = $true)]
        [string]$SecretServerToken
    )

    Process
    {
        $Proxy = New-WebServiceProxy -uri $Script:TOKX_SECRETSERVER_PROD_ENDPOINT -UseDefaultCredential
        $GetTokenIsValidResponse = $Proxy.GetTokenIsValid($SecretServerToken)

        if($GetTokenIsValidResponse.Errors)
        {
            return $false
        }

        return $true
    }
}
