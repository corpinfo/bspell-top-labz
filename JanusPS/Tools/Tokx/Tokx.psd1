﻿@{
    Description       = 'Module for managing Rackspace, Core and Cloud tokens'
    ModuleToProcess   = 'Tokx.psm1'
    ModuleVersion     = '3.2.0.0'
    GUID              = '42f3f219-66de-4f0a-9f34-5a2aa4166e31'
    Author            = 'Hounsou Dansou'
    CompanyName       = 'Rackspace'
    Copyright         = '2015'
    PowerShellVersion = '5.0.0.0'
    RequiredModules   = @(
        @{
            ModuleName      = 'PoshSecret'
            RequiredVersion = '1.1.9.0'
        }
    )
    NestedModules     = @(
        'Classes/NoHandlerForHttp.psm1'
    )
    FunctionsToExport = @(
        'Get-AuthEndpoint',
        'Get-HiddenStringPrompt',
        'Get-IntCustomerAccess',
        'Get-IntPassword',
        'Get-IntUsername',
        'Get-PoshCoreToken',
        'Get-RackerImpersonationToken',
        'Get-RackerToken',
        'Get-SamlToken',
        'Get-SecretServerToken',
        'Get-SsoCredential',
        'Get-SsoUsername',
        'Grant-IntCustomerAccess',
        'Invoke-Query',
        'Invoke-RestMethodWithAuth',
        'Save-PoshCoreToken',
        'Save-RackerToken',
        'Set-IntUsername',
        'Set-SsoUsername',
        'Test-IntCustomerAccess',
        'Test-PoshCoreToken',
        'Test-RackerToken'
    )
    PrivateData       = @{
        PSData = @{
            Tags      = @(
                'token',
                'api',
                'credential',
                'cloud',
                'rsa',
                'authentication'
            )
            OSVersion = '6.0'
        }
    }
}
