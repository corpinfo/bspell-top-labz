﻿$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    BeforeAll {
        $ValidRsaToken = "12341234"
    }

    Describe CoreToken {

        BeforeEach {
            $ValidCoreToken = '4049eeac980f7bfbcb6df51ef402cb82'
            $ValidRackerToken = 'TXkgcGFzc3dvcmQgaXMgJ2h1bnRlcjIn'
            $ValidStagingPassword = 'hunter2'

            Mock Get-SsoUsername {return "tokx.unit.tests"}     # Don't lock out your account!

            Mock Get-HiddenStringPrompt -ModuleName Tokx {return $ValidRsaToken}

            Mock Get-RackerToken -ParameterFilter {$AsXAuthToken} {return $ValidRackerToken}

            $PWSafeUri = "https://passwordsafe.corp.rackspace.com/projects/3514/credentials/33318"
            Mock Invoke-RestMethodWithAuth {return @{credential = @{password = $ValidStagingPassword}}}

            Mock Invoke-RestMethod {
                if ($Body -match ($ValidRsaToken, $ValidRackerToken, $ValidStagingPassword -join '|'))
                {
                    return @{authtoken = $ValidCoreToken}
                }
                else
                {
                    throw (New-Object System.Net.WebException ("The remote server returned an error: (403) Forbidden."))
                }
            }

            Mock Add-PoshSecret {
                $Script:MockPoshSecret = @{
                    Name     = $Resource;
                    Username = $Username;
                    Password = $Password;
                    Expiry   = $Expiry
                }
            }
        }


        Context NotStored {

            BeforeEach {
                Mock Get-PoshSecret {}
                $Token = Get-PoshCoreToken -AsPlainText
            }

            It 'Tries to retrieve' {
                Should -Invoke Get-PoshSecret -Times 1
            }

            It 'Asks for Rsa' {
                Should -Invoke Get-HiddenStringPrompt -ModuleName Tokx -Times 1
            }

            It 'Hits the API' {
                Should -Invoke Invoke-RestMethod -Times 1
            }

            It 'Stores the token' {
                Should -Invoke Add-PoshSecret -Times 1
                $Script:MockPoshSecret | Should -Not -Be $null
                $Script:MockPoshSecret.Password | Should -Be $ValidCoreToken
            }

            It 'Gives you back a token' {
                $Token | Should -Be $ValidCoreToken
            }
        }

        Context Stored {

            BeforeEach {
                Mock Get-PoshSecret {return $Script:MockPoshSecret}
                $Token = Get-PoshCoreToken -AsPlainText
            }

            It 'Tries to retrieve' {
                Should -Invoke Get-PoshSecret -Times 1
            }

            It "Doesn't ask for Rsa" {
                Should -Invoke Get-HiddenStringPrompt -ModuleName Tokx -Times 0
            }

            It "Doesn't hit the API" {
                Should -Invoke Invoke-RestMethod -Times 0
            }

            It "Doesn't try to store the token" {
                Should -Invoke Add-PoshSecret -Times 0
            }

            It 'Gives you back a token' {
                $Token | Should -Be $ValidCoreToken
            }
        }

        Context "Invalid RSA" {

            BeforeEach {
                Mock Get-HiddenStringPrompt -ModuleName Tokx {return "98769876"}
                Mock Get-PoshSecret {}
                {$null = Get-PoshCoreToken} | Should -Throw
            }

            It 'Tries to retrieve' {
                Should -Invoke Get-PoshSecret -Times 1
            }

            It 'Asks for Rsa' {
                Should -Invoke Get-HiddenStringPrompt -ModuleName Tokx -Times 1
            }

            It 'Hits the API' {
                Should -Invoke Invoke-RestMethod -Times 1
            }

            It 'Returns null' {
                $Token | Should -Be $null
            }

            It "Doesn't store the token" {
                Should -Invoke Add-PoshSecret -Times 0
            }

        }

        Context "Staging" {

            BeforeEach {
                Mock Get-PoshSecret {}
                $Token = Get-PoshCoreToken -AsPlainText -Endpoint Staging
            }

            It 'Tries to retrieve' {
                Should -Invoke Get-PoshSecret -Times 1
            }

            It 'Does not ask for Rsa' {
                Should -Invoke Get-HiddenStringPrompt -ModuleName Tokx -Times 0
            }

            It 'Hits the API' {
                Should -Invoke Invoke-RestMethod -Times 1
            }

            It 'Returns the token' {
                $Token | Should -Not -BeNullOrEmpty
            }

            It "Stores the token" {
                Should -Invoke Add-PoshSecret -Times 1
            }
        }
    }
}
