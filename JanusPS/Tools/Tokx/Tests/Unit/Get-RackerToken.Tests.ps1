$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    Describe "Get-RackerToken" {

        BeforeAll {
            $TestUser = "test.user"
            $TestPassword = "hunter2"
            $TestCred = [pscredential]::new($TestUser, ($TestPassword | ConvertTo-SecureString -AsPlaintext -Force))


            # Let's not lock ourselves out now, eh
            Mock Invoke-RestMethod {throw "Oops, an unmocked rest request!"}
            Mock Invoke-WebRequest {throw "Oops, an unmocked web request!"}
            Mock Get-SsoUsername   {return $TestUser}
            Mock Get-PoshSecret


            $ValidSamlTokenXmlString = "
                <samlp:Response ID=`"deadbeef`" Version=`"2.0`" IssueInstant=`"$([datetime]::Now.ToString('o'))`" Destination=`"https://stepladder.rax.io/static/tokx_adfs_bounce`" Consent=`"urn:oasis:names:tc:SAML:2.0:consent:unspecified`" xmlns:samlp=`"urn:oasis:names:tc:SAML:2.0:protocol`">
                    <Assertion ID=`"dead-beef-2`">
                        <Conditions NotBefore=`"$([datetime]::Now.ToString('o'))`" NotOnOrAfter=`"$([datetime]::Now.AddHours(8).ToString('o'))`"></Conditions>
                    </Assertion>
                </samlp:Response>
            "
            $ValidSamlTokenXml = [xml]$ValidSamlTokenXmlString
            $ValidRackerToken = "foo"
            $ValidRackerTokenResponse = ConvertFrom-Json "{`"access`": {`"serviceCatalog`": `"`",`"user`": {`"RAX-AUTH:defaultRegion`": `"`",`"roles`": [{`"name`": `"Racker`",`"id`": `"9`"},{`"name`": `"dl_RackGlobal`"}],`"name`": `"$TestUser`",`"id`": `"$TestUser`"},`"token`": {
                `"expires`": `"2021-01-23T00:31:29.797Z`",
                `"RAX-AUTH:issued`": `"2021-01-22T12:37:44.797Z`",
                `"RAX-AUTH:authenticatedBy`": [`"RSAKEY`"],
                `"id`": `"$ValidRackerToken`"
            }}}"
            $ValidRsaPincode = "12341234"
        }


        Context "Cache hit" {

            BeforeEach {
                Mock Get-PoshSecret {return @{Password = $ValidRackerToken}}
                Mock Request-SamlToken {throw "Requested fresh token when it should not have"}
                $Result = Get-SamlToken
            }

            It "Returns from cache immediately" {
                Should -Invoke Get-PoshSecret -Times 1 -Exactly
            }
        }

        Context "SAML auth" {

            BeforeEach {
                Mock Get-PoshSecret {}
                Mock Get-SamlToken -ParameterFilter {[bool]$AsXml} {return $ValidSamlTokenXml}
                Mock Add-PoshSecret -ParameterFilter {$Password -match $ValidRackerToken} {}
                Mock Invoke-RestMethod -ParameterFilter {
                    $Uri -match "/RAX-AUTH/federation/saml/auth$" -and
                    $ContentType -eq "application/xml" -and
                    $Method -eq "Post" -and
                    $Body -eq $ValidSamlTokenXml
                } {
                    return $ValidRackerTokenResponse
                }
                $Result = Get-RackerToken
            }

            It "Checks cache" {
                Should -Invoke Get-PoshSecret -Times 1 -Exactly
            }

            It "Gets SAML token" {
                Should -Invoke Get-SamlToken -Times 1 -Exactly
            }

            It "Requests token from Identity service with SAML token" {
                Should -Invoke Invoke-RestMethod -Times 1 -Exactly
            }

            It "Writes to cache" {
                Should -Invoke Add-PoshSecret -Times 1 -Exactly
            }

            It "Returns a token" {
                $Result.XAuthToken.'x-auth-token' | Should -BeExactly $ValidRackerToken
            }
        }


        Context "RSA pincode auth" {

            BeforeEach {
                Mock Get-PoshSecret {}
                Mock Get-HiddenStringPrompt {return $ValidRsaPincode}
                Mock Get-SamlToken {throw "Shouldn't be requesting a SAML token when authing with RSA!"}
                Mock Add-PoshSecret -ParameterFilter {$Password -match $ValidRackerToken} {}
                Mock Invoke-RestMethod -ParameterFilter {
                    $Uri -match "/tokens$" -and
                    $ContentType -eq "application/json" -and
                    $Method -eq "Post" -and
                    $(
                        $BodyObj = $Body | ConvertFrom-Json
                        $BodyObj.auth.rsaCredentials.username -eq $TestUser -and
                        $BodyObj.auth.rsaCredentials.tokenKey -eq $ValidRsaPincode
                    )
                } {
                    return $ValidRackerTokenResponse
                }

                $Result = Get-RackerToken -UseRsa
            }

            It "Checks cache" {
                Should -Invoke Get-PoshSecret -Times 1 -Exactly
            }

            It "Prompts for RSA pincode" {
                Should -Invoke Get-HiddenStringPrompt -Times 1 -Exactly
            }

            It "Requests token from Identity service with RSA pincode" {
                Should -Invoke Invoke-RestMethod -Times 1 -Exactly
            }

            It "Writes to cache" {
                Should -Invoke Add-PoshSecret -Times 1 -Exactly
            }

            It "Returns a token" {
                $Result.XAuthToken.'x-auth-token' | Should -BeExactly $ValidRackerToken
            }
        }


        Context "Credential auth" {

            BeforeEach {
                Mock Get-PoshSecret {}
                Mock Get-HiddenStringPrompt {throw "Shouldn't be requesting an RSA pincode when authing with RSA!"}
                Mock Get-SamlToken {throw "Shouldn't be requesting a SAML token when authing with RSA!"}
                Mock Add-PoshSecret -ParameterFilter {$Password -match $ValidRackerToken} {}
                Mock Invoke-RestMethod -ParameterFilter {
                    $Uri -match "/tokens$" -and
                    $ContentType -eq "application/json" -and
                    $Method -eq "Post" -and
                    $(
                        $BodyObj = $Body | ConvertFrom-Json
                        $BodyObj.auth.passwordCredentials.username -eq $TestUser -and
                        $BodyObj.auth.passwordCredentials.password -eq $TestPassword
                    )
                } {
                    return $ValidRackerTokenResponse
                }

                $Result = Get-RackerToken -Credential $TestCred
            }

            It "Checks cache" {
                Should -Invoke Get-PoshSecret -Times 1 -Exactly
            }

            It "Requests token from Identity service with RSA pincode" {
                Should -Invoke Invoke-RestMethod -Times 1 -Exactly
            }

            It "Writes to cache" {
                Should -Invoke Add-PoshSecret -Times 1 -Exactly
            }

            It "Returns a token" {
                $Result.XAuthToken.'x-auth-token' | Should -BeExactly $ValidRackerToken
            }
        }
    }
}
