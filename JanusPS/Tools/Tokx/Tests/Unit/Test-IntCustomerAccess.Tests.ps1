﻿$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    Describe "Test-IntCustomerAccess" {

        BeforeEach {
            Mock Get-IntCustomerAccess -MockWith {
                return [psobject]@{
                    Account = "1103359"
                    Expires = (Get-Date).AddHours(2)
                }
            }
        }

        It "Returns False when an account is not in the currently granted list" {
            Test-IntCustomerAccess -Account 933316 | Should -Be $false
            Should -Invoke Get-IntCustomerAccess -Times 1 -Scope It -Exactly
        }

        It "Returns True when an account is in the currently granted list, and is not near expiration" {
            Test-IntCustomerAccess -Account 1103359 | Should -Be $true
            Should -Invoke Get-IntCustomerAccess -Times 1 -Scope It -Exactly
        }

        It "Returns False when an account is near expiring" {
            Mock Get-IntCustomerAccess -MockWith {
                return [psobject]@{
                    Account = "1103359"
                    Expires = (Get-Date).AddMinutes(44)
                }
            }
            Test-IntCustomerAccess -Account 1103359 | Should -Be $false
            Should -Invoke Get-IntCustomerAccess -Times 1 -Scope It -Exactly
        }
    }
}
