﻿$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    Describe "Get-IntPassword" {

        BeforeEach {
            Mock Get-IntUsername -MockWith {return "test-racker"}
            Mock Add-PoshSecret -MockWith {}
            Mock Get-PoshSecret -MockWith {
                New-Object System.Management.Automation.PSCredential (
                    "INTENSIVE\mock",
                    "jZEkWDpw3J6Xd2u" | ConvertTo-SecureString -AsPlainText -Force # Dummy password
                )
            }
            Mock Remove-PoshSecret -MockWith {}
            Mock Invoke-RestMethodWithAuth -MockWith {
                return [psobject]@{
                    expires = "2020-07-02T19:37:29.9737587Z"
                    password = "lDXnbj6NGbnMSXpic7VN" # Dummy password
                }
            }
        }

        It "Returns token as PSCredential from cache successfully" {
            $Output = Get-IntPassword
            Should -Invoke Invoke-RestMethodWithAuth -Times 0 -Scope It -Exactly
            $Output | Should -BeOfType [System.Management.Automation.PSCredential]
        }

        It "Returns token as String from cache successfully with -AsPlainText" {
            $Output = Get-IntPassword -AsPlainText
            Should -Invoke Invoke-RestMethodWithAuth -Times 0 -Scope It -Exactly
            $Output | Should -BeOfType [String]
            $Output | Should -Be "jZEkWDpw3J6Xd2u"
        }

        It "Requests a token when there is not one cached, and stores it" {
            Mock Get-PoshSecret -MockWith {}
            $Output = Get-IntPassword
            Should -Invoke Invoke-RestMethodWithAuth -Times 1 -Scope It -Exactly
            Should -Invoke Add-PoshSecret -Times 1 -Scope It -Exactly
        }

        It "Requests a new token when -Flush is specified, and stores it" {
            Mock Get-PoshSecret -MockWith {}
            $Output = Get-IntPassword -Flush
            Should -Invoke Remove-PoshSecret -Times 1 -Scope It -Exactly
            Should -Invoke Invoke-RestMethodWithAuth -Times 1 -Scope It -Exactly
            Should -Invoke Add-PoshSecret -Times 1 -Scope It -Exactly
        }

        It "Writes to the error stream when the Intensive API fails" {
            Mock Get-PoshSecret -MockWith {}
            Mock Invoke-RestMethodWithAuth -MockWith {throw}
            Get-IntPassword -ErrorAction SilentlyContinue -ErrorVariable ApiError
            $ApiError | Should -Not -Be $null
        }
    }
}
