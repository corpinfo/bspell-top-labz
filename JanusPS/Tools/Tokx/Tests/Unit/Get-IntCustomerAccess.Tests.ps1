﻿$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    Describe "Get-IntCustomerAccess" {

        BeforeEach {
            Mock Get-IntUsername -MockWith {return "test-racker"}
            Mock Invoke-RestMethodWithAuth -MockWith {
                return [psobject]@{
                    attributes = @{
                        rsactiveaccess = '{"1103359":"2020-07-02T16:43:27.2851463Z"}'
                    }
                }
            }
        }

        It "Returns account 1103359 in output successfully" {
            $Output = Get-IntCustomerAccess
            Should -Invoke Invoke-RestMethodWithAuth -Times 1 -Scope It -Exactly
            $Output.Account | Should -Be "1103359"
            $Output.Account | Should -BeOfType [string]
            $Output.Expires | Should -BeOfType [datetime]
        }

        It "Returns null when no accounts are granted" {
            Mock Invoke-RestMethodWithAuth -MockWith {
                return [psobject]@{
                    attributes = @{
                        rsactiveaccess = ''
                    }
                }
            }
            Get-IntCustomerAccess | Should -Be $null
            Should -Invoke Invoke-RestMethodWithAuth -Times 1 -Scope It -Exactly
        }

        It "Writes to the error stream when the Intensive API fails" {
            Mock Get-PoshSecret -MockWith {}
            Mock Invoke-RestMethodWithAuth -MockWith {throw}
            Get-IntCustomerAccess -ErrorAction SilentlyContinue -ErrorVariable ApiError
            Should -Invoke Invoke-RestMethodWithAuth -Times 1 -Scope It -Exactly
            $ApiError | Should -Not -Be $null
        }
    }
}
