$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

. "$PSScriptRoot\TestData.ps1"

InModuleScope $Module {
    Describe "My feature" {
        It "Returns token from cache \ Get-PoshSecret" {
            Mock Get-SsoCredential {return New-Object -Typename Psobject -Property @{username = 'validusername'}}
            Mock Get-PoshSecret {return New-Object -Typename Psobject -Property @{password = 'testtoken'}}
            Mock Test-SecretServerToken {return $true}
            Get-SecretServerToken | Should -Be 'testtoken'
            Should -Invoke Test-SecretServerToken -Scope It
        }

        It "No Token Stored \ Gets new token" {
            Mock Get-SsoCredential {return $Global:TOKX_CREDENTIAL}
            Mock Get-PoshSecret {return $null}
            Mock Get-HiddenStringPrompt {return "00000000"}
            Mock Invoke-SecretServerToken {return 'validtoken'}
            Mock Add-PoshSecret {return $null}
            Get-SecretServerToken | Should -Be 'validtoken'
            Should -Invoke Invoke-SecretServerToken -Scope It
            Should -Invoke Add-PoshSecret -Scope It
        }

        It "Get token for ServiceAccount from SecretServer" {
            Mock Invoke-SecretServerToken {return "ValidToken"}
            Get-SecretServerToken -Credential $Global:TOKX_CREDENTIAL | Should -Be 'ValidToken'
            Should -Invoke Invoke-SecretServerToken -Scope It
        }
    }
}
