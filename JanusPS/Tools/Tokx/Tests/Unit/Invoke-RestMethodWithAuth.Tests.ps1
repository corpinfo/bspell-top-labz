﻿$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    BeforeAll {
        Mock Get-RackerToken -MockWith {
            if ($AsXAuthToken) {return 'FOOBAR'}
            return @{
                XAuthToken = @{
                    'x-auth-token' = 'FOOBAR'
                }
            }
        }

        Mock Invoke-RestMethod -MockWith {
            return @{
                Endpoint                   = $Endpoint
                Method                     = $Method
                UseBasicParsing            = $UseBasicParsing
                Uri                        = $Uri
                WebSession                 = $WebSession
                SessionVariable            = $SessionVariable
                Credential                 = $Credential
                UseDefaultCredentials      = $UseDefaultCredentials
                CertificateThumbprint      = $CertificateThumbprint
                Certificate                = $Certificate
                UserAgent                  = $UserAgent
                DisableKeepAlive           = $DisableKeepAlive
                TimeoutSec                 = $TimeoutSec
                Headers                    = $Headers
                MaximumRedirection         = $MaximumRedirection
                Proxy                      = $Proxy
                ProxyCredential            = $ProxyCredential
                ProxyUseDefaultCredentials = $ProxyUseDefaultCredentials
                Body                       = $Body
                ContentType                = $ContentType
                TransferEncoding           = $TransferEncoding
                InFile                     = $InFile
                OutFile                    = $OutFile
                PassThru                   = $PassThru
            }
        }

        $Params = @{
            Method      = 'POST'
            Uri         = [uri]'http://foo.bar'
            Body        = @{foo = 1; bar = 2}
            ContentType = 'whatever'
        }
   }

   Describe "Get-RackerToken" {

        It 'Injects params transparently' {
            $Output = Invoke-RestMethodWithAuth @Params
            Should -Invoke Invoke-RestMethod -Times 1
            foreach ($Kvp in $Params.GetEnumerator())
            {
                $Output[$Kvp.Key] | Should -Be $Kvp.Value
            }
        }

        It 'Injects racker token' {
            $Output = Invoke-RestMethodWithAuth @Params
            Should -Invoke Get-RackerToken -Times 1
            $Output.Headers.'x-auth-token' | Should -Be 'FOOBAR'
        }
   }
}
