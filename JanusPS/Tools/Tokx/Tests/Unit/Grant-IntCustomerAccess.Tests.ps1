﻿$Module = 'Tokx'
Get-Module $Module -All | Remove-Module -Force
Import-Module "$PSScriptRoot\..\..\$Module.psd1" -Force -ErrorAction 'Stop'

InModuleScope $Module {

    Describe "Grant-IntCustomerAccess" {

        BeforeEach {
           Mock Get-IntUsername -MockWith {return "test-racker"}
           Mock Invoke-RestMethodWithAuth -MockWith {$null}
        }

        It "Grants access to an account when access is not currently granted" {
            Mock Test-IntCustomerAccess -MockWith {return $false}
            Grant-IntCustomerAccess -Account 1103359 -ErrorAction SilentlyContinue -ErrorVariable ApiError
            Should -Invoke Invoke-RestMethodWithAuth -Times 1 -Scope It -Exactly
            Should -Invoke Test-IntCustomerAccess -Times 2 -Scope It -Exactly

            # We cannot change the Test-IntCustomerAccess mock mid run
            $ApiError[0] | Should -Be "Could not confirm access has been granted to account 1103359 - response from API: "
        }

        It "Returns True from cache when access is already granted" {
            Mock Test-IntCustomerAccess -MockWith {return $true}
            Grant-IntCustomerAccess -Account 1103359
            Should -Invoke Invoke-RestMethodWithAuth -Times 0 -Scope It -Exactly
        }

        It "Writes to the error stream when the Intensive API fails" {
            Mock Get-PoshSecret -MockWith {}
            Mock Invoke-RestMethodWithAuth -MockWith {throw}
            Get-IntPassword -ErrorAction SilentlyContinue -ErrorVariable ApiError
            $ApiError | Should -Not -Be $null
        }
    }
}
