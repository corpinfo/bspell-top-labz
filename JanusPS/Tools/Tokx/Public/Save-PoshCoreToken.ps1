﻿function Save-PoshCoreToken
{
    <#
        .SYNOPSIS
        Given an authentication token for Core, saves it to the vault

        .DESCRIPTION
        Securely saves authentication tokens using the PoshSecret module. This can be retrieved later with Get-PoshCoreToken.

        If you have a valid token from another source, e.g. the browser, you can use this function to inject the token into the password vault. This saves other code from having to retrieve it from the API and may save the user having to perform additional authentication steps.

        Do not use this function if you have retrieved your token with Get-PoshCoreToken. It is redundant in that case, as Get-PoshCoreToken always saves the token on success.

        .PARAMETER CoreToken
        Token to save, as a string of 32 hexadecimal characters

        .PARAMETER SsoUsername
        The user's SSO username. Defaults to the username of the logged-in user, which is usually the same.

        .PARAMETER TestApiAuth
        Specify to test that the token is valid against the API. If this is omitted, only regex validation is performed. Invokes Test-PoshCoreToken.

        .PARAMETER Expiry
        When the token will expire

        .OUTPUTS
        void

        .EXAMPLE
        Save-PoshCoreToken $Token

        Given that $Token holds a Core token as a string, validates the string is 32 hexadecimal characters and saves to the password vault.

        Throws an exception if validation fails.

        .EXAMPLE
        Save-PoshCoreToken

        .NOTES
        Written by Freddie Sackur, IAWW-1374
    #>
    [CmdletBinding(DefaultParameterSetName='Default')]
    [OutputType([void])]
    param
    (
        [Parameter(Mandatory=$true, Position = 0)]
        [string]$CoreToken,

        [Parameter(Position = 1)]
        [string]$SsoUsername = $((Get-SsoUsername).Tolower()),

        [Parameter()]
        [datetime]$Expiry = (Get-Date).AddSeconds($TOKX_CORETOKEN_LIFETIME_SECONDS),

        [Parameter()]
        [switch]$TestApiAuth
    )

    if ($CoreToken -inotmatch '^[0-9a-f]{32}$')
    {
        throw (New-Object System.ArgumentException ("Token format is invalid"))
    }

    if ($TestApiAuth -and -not (Test-PoshCoreToken -CoreToken $CoreToken))
    {
        throw (New-Object System.Security.Authentication.InvalidCredentialException ("Token is invalid"))
    }

    $AddPoshSecretSplat = @{
        Name = "CoreToken";
        Username = $SsoUsername;
        Password = $CoreToken
        Expiry = $Expiry
    }

    Add-PoshSecret @AddPoshSecretSplat
}
