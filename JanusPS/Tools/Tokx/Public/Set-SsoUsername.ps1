﻿function Set-SsoUsername
{
    <#
        .SYNOPSIS
        Sets the SsoUsername environment variable.

        .DESCRIPTION
        Code exists that assumes your SSO username is the same as your current logon username. This function sets the actual SsoUsername.
        If your SSO username is the same as your current logon username, this function does nothing. If they are different, it sets the SsoUsername environment variable.
        Use in conjunction with Get-SsoUsername.

        .PARAMETER SsoUsername
        The users SSO

        .EXAMPLE
        Set-SsoUsername -SsoUsername mich8638
        Sets the SsoUsername environment variable to "mich8638"

        .OUTPUTS
        Void

        .NOTES
        Author: Freddie Sackur
        Date: 2016-09-09T14:03:17
        Jira: IAWW-627

    #>

    param
    (
        [Parameter(Mandatory=$true, Position=0)]
        [ValidateNotNullOrEmpty()]
        [string]$SsoUsername
    )

    if ($SsoUsername -clike $(($env:USERNAME).Tolower()))
    {
        Remove-ItemProperty -Path HKCU:\Environment -Name SsoUsername -Force -ErrorAction SilentlyContinue
        $env:SsoUsername = $null
    }
    else
    {
        New-ItemProperty -Path HKCU:\Environment -Name SsoUsername -PropertyType String -Value $($SsoUsername.ToLower()) -Force -ErrorAction SilentlyContinue
        $env:SsoUsername = $SsoUsername.ToLower()
    }
}
