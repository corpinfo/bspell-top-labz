﻿function Get-AuthEndpoint
{
<#
    .SYNOPSIS
    Gets the authentication endpoint

    .DESCRIPTION
    Gets the relevant authentication endpoint depending on requested
    Will automatically add LON prefix for UK by looking up system locale

    .PARAMETER Endpoint
    Which cloud authentication endpoint to get the URL for. The default is "Internal"

    .PARAMETER Cloud
    Deprecated - do not use

    .PARAMETER Staging
    Deprecated - do not use

    .PARAMETER Internal
    Deprecated - do not use

    .OUTPUTS
    [string]

    .EXAMPLE
    Get the cloud endpoint
    Get-AuthEndpoint -Endpoint "Cloud"

    .EXAMPLE
    Get the Staging endpoint
    Get-AuthEndpoint -Endpoint "Staging"

    .EXAMPLE
    Get the Internal endpoint
    Get-AuthEndpoint -Endpoint "Internal"

    .NOTES
    Written by Hounsou Dansou
#>
    [CmdletBinding(DefaultParametersetName='Default')]
    [OutputType([string])]
    Param
    (
        [Parameter(ParameterSetName='Default', ValueFromPipelineByPropertyName=$true, Position=0)]
        [ValidateSet("Cloud", "Staging", "Internal")]
        [string]$Endpoint = $TOKX_DEFAULT_ENDPOINT,

        [Parameter(ParameterSetName='Cloud')]       # Deprecated; it's easier to chain functions together using a string and ValidateSet validation
        [switch]$Cloud,
        [Parameter(ParameterSetName='Staging')]     # Deprecated; it's easier to chain functions together using a string and ValidateSet validation
        [switch]$Staging,
        [Parameter(ParameterSetName='Internal')]    # Deprecated; it's easier to chain functions together using a string and ValidateSet validation
        [switch]$Internal
    )

    if ($PsCmdlet.ParameterSetName -ne "Default")
    {
        $Endpoint = $PsCmdlet.ParameterSetName
        Write-Warning "The Cloud, Staging and Internal switches are deprecated. Please use the Endpoint parameter instead."
    }

    $IdentityEnvironment = @{
        'Cloud' = 'identity'
        'Staging' = 'staging.identity-internal'
        'Internal' = 'identity-internal'
    }

    $SubDomain = $IdentityEnvironment.$Endpoint
    return "https://$SubDomain.api.rackspacecloud.com/v2.0"
}
