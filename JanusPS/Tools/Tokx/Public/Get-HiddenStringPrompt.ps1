﻿function Get-HiddenStringPrompt
{
<#
    .SYNOPSIS
    Prompt for users password and obscure input and enter ***, like a password field

    .DESCRIPTION
    Prompt for users password and obscure and enter ***, like a password field
    Can change input text
    note: input is not encrypted

    .PARAMETER Prompt
    What to show in the prompt to the user - rsa token or Password

    .OUTPUTS
    [string]

    .EXAMPLE
    Will show "Please enter your password"
    Get-HiddenStringPrompt -Prompt "password"

    .EXAMPLE
    Will show "Please enter your rsa token"
    Get-HiddenStringPrompt -Prompt "rsa token"

    .NOTES
    Written by Martin Howlett
#>
    Param
    (
        [Parameter()]
        [ValidateSet("rsa token","Password")]
        [string]$Prompt = "Password"
    )


    $SecureString = Read-Host -Prompt "Please enter your $Prompt" -AsSecureString
    $SecureString | ConvertTo-Plaintext
}
