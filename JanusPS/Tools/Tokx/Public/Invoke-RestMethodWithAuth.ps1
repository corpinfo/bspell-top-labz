﻿function Invoke-RestMethodWithAuth
{
    <#
        .SYNOPSIS
        Wrapper around Invoke-RestMethod to inject a Racker token.

        .DESCRIPTION
        Use this as a drop-in replacement for Invoke-RestMethod when the target URI accepts a racker
        token for authentication. This command handles the authentication for you.

        The only difference in use is that the Endpoint parameter has been added. You only need to
        use this parameter when the target URI requires an authentication token from the Staging or
        Internal instances.

        Note that the parameter help for this command is a cut-down version of the help from
        Invoke-RestMethod. For full descriptions of each parameter, use
        `Get-Help Invoke-RestMethod -Full`.

        .PARAMETER Endpoint
        Which cloud authentication endpoint to get the URL for. The default is "Internal", which is
        correct for the majority of Rackspace internal production services.

        .PARAMETER Body
        Specifies the body of the request. The body is the content of the request that follows the
        headers. You can also pipe a body value to Invoke-RestMethod.

        .PARAMETER Certificate
        Specifies the client certificate that is used for a secure web request. Enter a variable
        that contains a certificate or a command or expression that gets the certificate.

        .PARAMETER CertificateThumbprint
        Specifies the digital public key certificate (X509) of a user account that has permission to
        send the request. Enter the certificate thumbprint of the certificate.

        .PARAMETER ContentType
        Specifies the content type of the web request.

        .PARAMETER Credential
        Specifies a user account that has permission to send the request. The default is the current
        user.

        .PARAMETER DisableKeepAlive
        Indicates that the cmdlet sets the KeepAlive value in the HTTP header to False. By default,
        KeepAlive is True. KeepAlive establishes a persistent connection to the server to facilitate
        subsequent requests.

        .PARAMETER Headers
        Specifies the headers of the web request. Enter a hash table or dictionary.

        .PARAMETER InFile
        Specifies the file for which this cmdlet gets the content of the web request.

        .PARAMETER MaximumRedirection
        Specifies how many times Windows PowerShell redirects a connection to an alternate Uniform
        Resource Identifier (URI) before the connection fails. The default value is 5. A value of 0
        (zero) prevents all redirection.

        .PARAMETER Method
        Specifies the method used for the web request.

        .PARAMETER OutFile
        Specifies the output file that this cmdlet saves the response body. Enter a path and file
        name. If you omit the path, the default is the current location.

        By default, Invoke-RestMethod returns the results to the pipeline. To send the results to a
        file and to the pipeline, use the Passthru parameter.

        .PARAMETER PassThru
        Indicates that the cmdlet returns the results, in addition to writing them to a file. This
        parameter is valid only when the OutFile parameter is also used in the command.

        .PARAMETER Proxy
        Specifies that the cmdlet uses a proxy server for the request, rather than connecting
        directly to the Internet resource. Enter the URI of a network proxy server.

        .PARAMETER ProxyCredential
        Specifies a user account that has permission to use the proxy server that is specified by
        the Proxy parameter. The default is the current user.

        .PARAMETER ProxyUseDefaultCredentials
        Indicates that the cmdlet uses the credentials of the current user to access the proxy
        server that is specified by the Proxy parameter.

        .PARAMETER SessionVariable
        Specifies a variable for which this cmdlet creates a web request session and saves it in the
        value. Enter a variable name without the dollar sign ($) symbol.

        .PARAMETER TimeoutSec
        Specifies how long the request can be pending before it times out. Enter a value in seconds.
        The default value, 0, specifies an indefinite time-out.

        .PARAMETER TransferEncoding
        Specifies a value for the transfer-encoding HTTP response header.

        .PARAMETER Uri
        Specifies the Uniform Resource Identifier (URI) of the Internet resource to which the web
        request is sent. This parameter supports HTTP, HTTPS, FTP, and FILE values.

        .PARAMETER UseBasicParsing
        Indicates that the cmdlet uses basic parsing.

        .PARAMETER UseDefaultCredentials
        Indicates that the cmdlet uses the credentials of the current user to send the web request.

        .PARAMETER UserAgent
        Specifies a user agent string for the web request.

        .PARAMETER WebSession
        Specifies a web request session. Enter the variable name, including the dollar sign ($).

        .PARAMETER RetryCount
        Count of times to retry a request on failure. Defaults to $MAX_HTTP_RETRIES

        .PARAMETER BackoffSeconds
        Amount of time to wait between requests on failure. Fefaults to $BACKOFF_INCREMENT_SECONDS

        Increments by this on each iteration of the retry, so 10, 20, 30, is the default behaviour.

        .OUTPUTS
        [System.Xml.XmlDocument], [Microsoft.PowerShell.Commands.HtmlWebResponseObject], [string]
        The output of the cmdlet depends upon the format of the content that is retrieved.

        [PSObject]
        If the request returns JSON strings, Invoke-RestMethodWithAuth returns a PSObject that
        represents the strings.

        .EXAMPLE
        $Uri = 'https://api.intensive.int/ad/v1/domains/intensive/users/mich8638.cust/customeraccess'
        Invoke-RestMethodWithAuth -Uri $Uri

        1459178
        -------
        2019-12-27T19:49:26.8588058Z

        Fetches data from the AD API.

        .EXAMPLE
        $Uri = "https://api.staging.dcx.rackspace.com/devices/487362/vm_net_data"
        Invoke-RestMethodWithAuth -Uri $Uri -Endpoint Staging

        vmotion_ip    : 10.252.202.237
        vlan_name     : vlan4031:10.252.200.0/22
        primary_dns   : 10.252.9.131
        gateway       : 10.252.200.1
        netmask       : 255.255.252.0
        management_ip : 10.252.202.236
        secondary_dns : 10.252.9.132

        Fetches data from the DCX staging API. Because this service is in the staging environment,
        we specify the Staging endpoint to inject a racker token from the Staging environment.

        .EXAMPLE
        Invoke-RestMethodWithAuth -Method Get https://stepladder.rax.io/api/devices/v1/powered_on/560559 -RetryCount 5 -BackoffSeconds 3

        error result msg
        ----- ------ ---
        False   True 560559 is powered on

        Calls the Stepladder Power API, but will retry up to 5 times and increments the backoff by 3 seconds on each failed request.

        .NOTES
        This command is a faithful implementation of all Invoke-RestMethod parameters. Here's how:

        $Command = Get-Command Invoke-RestMethod
        $CommandMetadata = [System.Management.Automation.CommandMetadata]::new($Command)
        [System.Management.Automation.ProxyCommand]::Create($CommandMetadata)
    #>

    [CmdletBinding(HelpUri = 'https://pages.github.rackspace.com/windows-automation/Tokx/functions/Invoke-RestMethodWithAuth')]
    param
    (
        [Parameter()]
        [ValidateSet("Cloud", "Staging", "Internal")]
        [string]$Endpoint = $TOKX_DEFAULT_ENDPOINT,

        [Parameter()]
        [Microsoft.PowerShell.Commands.WebRequestMethod]$Method,

        [Parameter()]
        [switch]$UseBasicParsing,

        [Parameter(Mandatory = $true, Position = 0)]
        [ValidateNotNullOrEmpty()]
        [uri]$Uri,

        [Parameter()]
        [Microsoft.PowerShell.Commands.WebRequestSession]$WebSession,

        [Parameter()]
        [Alias('SV')]
        [string]$SessionVariable,

        [Parameter()]
        [pscredential]
        [System.Management.Automation.CredentialAttribute()]$Credential,

        [Parameter()]
        [switch]$UseDefaultCredentials,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$CertificateThumbprint,

        [Parameter()]
        [ValidateNotNull()]
        [X509Certificate]$Certificate,

        [Parameter()]
        [string]$UserAgent,

        [Parameter()]
        [switch]$DisableKeepAlive,

        [Parameter()]
        [ValidateRange(0, 2147483647)]
        [int]$TimeoutSec,

        [Parameter()]
        [System.Collections.IDictionary]$Headers,

        [Parameter()]
        [ValidateRange(0, 2147483647)]
        [int]$MaximumRedirection,

        [Parameter()]
        [uri]$Proxy,

        [Parameter()]
        [pscredential]
        [System.Management.Automation.CredentialAttribute()]
        $ProxyCredential,

        [Parameter()]
        [switch]$ProxyUseDefaultCredentials,

        [Parameter(ValueFromPipeline = $true)]
        [System.Object]$Body,

        [Parameter()]
        [string]$ContentType,

        [Parameter()]
        [ValidateSet('chunked','compress','deflate','gzip','identity')]
        [string]$TransferEncoding,

        [Parameter()]
        [string]$InFile,

        [Parameter()]
        [string]$OutFile,

        [Parameter()]
        [switch]$PassThru,

        [Parameter()]
        [int]$RetryCount = $MAX_HTTP_RETRIES,

        [Parameter()]
        [int]$BackoffSeconds = $BACKOFF_INCREMENT_SECONDS
    )

    begin
    {
        $null = $PSBoundParameters.Remove('Endpoint')
    }

    process
    {
        if (-not $PSBoundParameters.ContainsKey('Headers'))
        {
            $PSBoundParameters.Headers = @{}
        }

        # Some services (Wex, Jenkins) may pass in their own tokens as they are using a service account
        # that is independent of the currently logged in user
        if (-not $PSBoundParameters.Headers.'X-Auth-Token')
        {
            $XAuthToken = Get-RackerToken -Endpoint $Endpoint -AsXAuthToken
            $PSBoundParameters.Headers.'x-auth-token' = $XAuthToken
        }

        # Not supported in Invoke-RestMethod, so pop them here
        $PSBoundParameters.Remove('RetryCount') | Out-Null
        $PSBoundParameters.Remove('BackoffSeconds') | Out-Null

        # Pop this out, as we handle this in the retry loop
        $PSBoundParameters.Remove('ErrorAction') | Out-Null

        $TimesRetried, $Sleep = 0, 0
        while ($TimesRetried -le $RetryCount)
        {
            try
            {
                Invoke-RestMethod @PSBoundParameters -ErrorAction Stop
                break
            }
            catch
            {
                $StatusCode = $_.Exception.Response.StatusCode.value__
                if ($NON_RETRYABLE_HTTP_CODES -contains $StatusCode)
                {
                    throw
                }
                elseif ($TimesRetried -ne $RetryCount)
                {
                    $TimesRetried++
                    $Sleep = $Sleep + $BackoffSeconds
                    Write-Verbose (
                        "Retrying API call in $($Sleep)s, due to retryable exception occurred $_. " +
                        "Status Code: $StatusCode. Attempt: $TimesRetried/$RetryCount"
                    )
                    Start-Sleep $Sleep
                }
                else
                {
                    throw # Boil exception back up to user if we're on our last retry
                }
            }
        }
    }
}
