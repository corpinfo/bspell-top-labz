﻿function Get-IntUsername
{
    <#
        .SYNOPSIS
        Gets the user's intensive or intensive .cust username.

        .DESCRIPTION
        Gets the user's intensive or intensive .cust username.

        .PARAMETER Cust
        Specifies to get the username for the .cust account.

        .OUTPUTS
        [string]

        The user's intensive or intensive .cust username.

        .EXAMPLE
        Get-IntUsername -Cust

        ryan0603.cust

        Gets the user's intensive .cust username.
    #>
    [CmdletBinding()]
    [OutputType([string])]
    param
    (
        [Parameter()]
        [switch]$Cust
    )

    if ($Cust)
    {
        $EnvVariableName = "IntensiveCustUsername"
    }
    else
    {
        $EnvVariableName = "IntensiveUsername"
    }
    $IntUsername = [Environment]::GetEnvironmentVariable($EnvVariableName, 'User')

    if (-not $IntUsername)
    {
        Set-IntUsername
        $IntUsername = Get-IntUsername -Cust:$Cust
    }

    return $IntUsername
}
