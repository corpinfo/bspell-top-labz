function Test-IntCustomerAccess
{
    <#
        .SYNOPSIS
        Tests whether or not the intensive user has been granted access to the given customer account.

        .DESCRIPTION
        Checks that the user's intensive .cust account shows access granted to a specified account.

        .PARAMETER Account
        The customer account for which to check access.

        .OUTPUTS
        [bool]

        Whether or not the user has been granted access to the given customer access.

        .EXAMPLE
        Test-IntCustomerAccess 1103359

        True

        Tests whether or not the intensive user has been granted access to the customer account '1103359'.
    #>
    [CmdletBinding()]
    [OutputType([bool])]
    param
    (
        [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
        [string]$Account
    )

    process
    {
        $Access = Get-IntCustomerAccess |
            Where-Object {$_.Account -eq $Account} |
            Where-Object {$_.Expires -gt (Get-Date).AddMinutes(45)}

        return [bool]($Access)
    }
}
