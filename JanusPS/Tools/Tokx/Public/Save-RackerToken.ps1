﻿function Save-RackerToken
{
    <#
        .SYNOPSIS
        Given an authentication token for the Rackspace identity API, saves it to the vault

        .DESCRIPTION
        Securely saves authentication tokens using the PoshSecret module. This can be retrieved later with Get-RackerToken.

        If you have a valid token from another source, e.g. the browser, you can use this function to inject the token into the password vault. This saves other code from having to retrieve it from the API and may save the user having to perform additional authentication steps.

        Do not use this function if you have retrieved your token with Get-RackerToken. It is redundant in that case, as Get-RackerToken always saves the token on success.

        .PARAMETER RackerToken
        Token to save, as a psobject

        .PARAMETER JsonRackerToken
        Token to save, in JSON format

        .PARAMETER Endpoint
        Specify which identity authentication endpoint to return (see Get-AuthEndpoint)

        .PARAMETER TestApiAuth
        Specify to test that the token is valid against the API. If this is omitted, only the object structure and expiry timestamp are validated.

        .OUTPUTS
        Void

        .EXAMPLE
        Save-RackerToken $Token

        Given that $Token holds a cloud token as a psobject, validates the object structure and expiry timestamp and saves to the password vault.

        Throws an exception if validation fails.

        .NOTES
        Freddie Sackur, IAWW-1374
    #>
    [CmdletBinding(DefaultParameterSetName='Default')]
    [OutputType([void])]
    param
    (
        [Parameter(ParameterSetName='Default', Position=0)]
        [psobject]$RackerToken,

        [Parameter(ParameterSetName='Json')]
        [string]$JsonRackerToken,

        [Parameter(Position = 1)]
        [ValidateSet("Cloud", "Staging", "Internal")]
        [string]$Endpoint = $TOKX_DEFAULT_ENDPOINT,

        [Parameter()]
        [switch]$TestApiAuth
    )

    $PoshSecretSplat = @{
        Name = "RackerToken"
        Username = $Endpoint;
    }

    #Validate object structure and/or validity against API
    if (-not (Test-RackerToken @PSBoundParameters))
    {
        throw (New-Object System.ArgumentException ("Token failed validation"))
    }

    #Make sure we have Json to store and an expiry date
    if ($PSCmdlet.ParameterSetName -eq 'Default')
    {
        $ExpiryUtcString = $RackerToken.Access.access.token.expires -replace 'Z$'
        $JsonRackerToken = ConvertTo-Json $RackerToken -Depth 10 -Compress
    }
    else
    {
        if ($JsonRackerToken -match '"expires":"(?<Utc>\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d\d\d)?)Z"')
        {
            $ExpiryUtcString = $Matches.Utc
        }
        else
        {
            throw (New-Object System.ArgumentException ("Token did not contain expiry timestamp"))
        }
    }

    $ExpiryUtc = New-Object datetime ([int[]]($ExpiryUtcString -split '-|:|T|\.') + [System.DateTimeKind]::Utc)
    $Expiry = [System.TimeZoneInfo]::ConvertTimeFromUtc($ExpiryUtc, [System.TimeZoneInfo]::Local)

    Add-PoshSecret @PoshSecretSplat -Password $JsonRackerToken -Property @{URL = (Get-AuthEndpoint -Endpoint $Endpoint)} -Expiry $Expiry
}
