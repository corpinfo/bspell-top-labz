﻿function Set-IntUsername
{
    <#
        .SYNOPSIS
        Sets the user's intensive and intensive .cust username. This is dynamically retrieved from the Intensive API.

        .DESCRIPTION
        Sets the user's intensive and intensive .cust username. This is dynamically retrieved from the Intensive API.

        .OUTPUTS
        [void]

        This command does not return any output.

        .EXAMPLE
        Set-IntUsername

        Sets the user's Intensive and Intensive.cust usernames correctly, based on output retrieved from the Intensive API.
    #>
    [CmdletBinding()]
    [OutputType([void])]
    param
    ()

    try
    {
        $Splat = @{
            Method      = "Get"
            Uri         = "https://api.identity.rackspace.corp/v1.0/ad/user/$(Get-SsoUsername)"
            ErrorAction = "Stop"
        }
        $EmployeeId = (Invoke-RestMethodWithAuth @Splat).data.employeeID
    }
    catch
    {
        Write-Error "Unable to find $(Get-SsoUsername) in Rackspace Identity. Please check Get-SsoUsername is correct."
        return
    }

    Write-Verbose "Employee ID: ($EmployeeId)"
    Write-Verbose "Getting Intensive accounts..."

    try
    {
        $Splat = @{
            Method      = "Get"
            Uri         = "$SS_API/ad/v1/domains/intensive/users/?filter=(employeeid%3D$EmployeeId)"
            ErrorAction = "Stop"
        }
        $EmployeeIntensiveAccounts = (Invoke-RestMethodWithAuth @Splat)
    }
    catch
    {
        Write-Error "Unexpected error from self-service Intensive API: $($_.Exception.Message)"
        return
    }

    Write-Verbose "Detected $($EmployeeIntensiveAccounts.Count) Intensive accounts!"

    $StandardAccount = $EmployeeIntensiveAccounts.resources | Where-Object {$_ -notlike "*.cust*"}
    if (-not $StandardAccount)
    {
        Write-Warning "Unable to detect a standard Intensive account from the Intensive API."
    }
    else
    {
        $StandardUsername = "INTENSIVE\" + ([uri]$StandardAccount).Segments[-1]
        Write-Verbose "Detected standard Intensive account: $StandardUsername"
        [Environment]::SetEnvironmentVariable("IntensiveUsername", $StandardUsername, 'User')
        Set-Item ENV:\IntensiveUsername -Value $StandardUsername
    }

    $CustAccount = $EmployeeIntensiveAccounts.resources | Where-Object {$_ -like "*.cust*"}
    if (-not $CustAccount)
    {
        Write-Warning "Unable to detect an Intensive .cust account from the Intensive API."
    }
    else
    {
        $CustUsername = "INTENSIVE\" + ([uri]$CustAccount).Segments[-1]
        Write-Verbose "Detected Intensive .cust account: $CustUsername"
        [Environment]::SetEnvironmentVariable("IntensiveCustUsername", $CustUsername, 'User')
        Set-Item ENV:\IntensiveCustUsername -Value $CustUsername
    }
}
