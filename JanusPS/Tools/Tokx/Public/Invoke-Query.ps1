﻿function Invoke-Query
{
    <#
    .SYNOPSIS
    Deprecated - please use Invoke-RestMethodWithAuth instead!

    Wrapper around Invoke-RestMethod to handle and add Racker token for all Rackspace APIs

    .DESCRIPTION
    Deprecated - please use Invoke-RestMethodWithAuth instead!

    Wrapper around Invoke-RestMethod to handle and add Racker token for all Rackspace APIs
    Can be used for all Rackspace internal systems that need a racker token
    Examples: Encore, Waldo, Lefty, ARIC etc

    Validates a Racker token is present then sends supplied query to endpoint
    Has friendly error handling
    Can handle GET/POST/PUT
    Will automatically set headers of JSON if post is used

    .PARAMETER URL
    The URL of the endpoint you want to hit

    .PARAMETER Method
    Can only be "Get","Post" or "Put"

    .PARAMETER JSON
    If you are sending a POST, the JSON you want to send in the payload

    .PARAMETER UserAgent
    Any custom useragent you wish to add

    .PARAMETER Suppress404
    Suppress 404 errors, if you know the URL you are hitting may not exist

    .PARAMETER Staging
    Uses staging racker token in request headers

    .OUTPUTS
    [object]

    .EXAMPLE
    Simple GET request

    Function will test a Racker token is present and correct, if not it will prompt user before storing it as a global variable then connect to URL

    $URL = "https://encore.rackspace.com/api/ticketing/ticket/lefty/160128-ord-0000703"
    $TicketContent = Invoke-Query -URL $URL -Method Get

    .EXAMPLE
    Simple GET request using staging token

    Function will test a staging Racker token is present and correct, if not it will prompt user before storing it as a global variable then connect to URL

    $URL = "https://api.staging.dcx.rackspace.com/devices/487362/vm_net_data"
    $VMNetData = Invoke-Query -Staging -URL $URL -Method Get

    .EXAMPLE
    Simple JSON POST request

    Function will test a Racker token is present and correct, if not it will prompt user before storing it as a global variable then posting to URL with supplied JSON

    $Payload = @{
                comment = @{
                    "text" = "Test comment";
                    "is_public" = $false

                }
            }
    $PayloadJson = ConvertTo-Json $Payload
    $URL = "https://api.ticketing.encore.rackspace.com/v1/accounts/10031180/tickets/160128-ord-0000701"
    $TicketUpdate = Invoke-Query -URL $URL -Method Put -JSON $PayloadJson

    .EXAMPLE
    GET request supressing 404 error.

    useful if you know the URL you are hitting may not exist (example looking for the datacentre is in in the cloud server API)

    $URL = "https://encore.rackspace.com/api/cloud/users/BobSmith/servers/HKG/$ServerUuid"
    $Params = @{
        URL = $URL
        Method = "Get"
    }
    $server = Invoke-Query -URL $URL -Method Get Suppress404

    this will return blank if URL is not found and doesnt error

    .NOTES
    Written by Hounsou Dansou / Martin Howlett
#>
    param
    (
        [Parameter(Mandatory = $true)]
        [string]$URL,

        [ValidateSet("Get", "Post", "Put")]
        [Parameter(Mandatory = $true)]
        [string]$Method,

        [Parameter()]
        [string]$JSON,

        [Parameter()]
        [string]$UserAgent,

        [Parameter()]
        [switch]$Suppress404,

        [Parameter()]
        [switch]$Staging
    )

    Begin
    {
        $output = $null

        Write-Verbose "[$(Get-Date)] List of Parameters :: $($PSBoundParameters.GetEnumerator() | Out-String)"
        Write-Verbose "[$(Get-Date)] Begin :: $($MyInvocation.MyCommand)"

        if ($env:USERNAME -in ('dimi9999', 'mark4808', 'mart6141', 'ryan0603', 'mich8638', 'conn3008', 'nakk8266', 'vish0313'))
        {
            Write-Warning "Tokx\Invoke-Query is deprecated. Please update your code to use Invoke-RestMethodWithAuth."
            Get-PSCallStack | Out-String | Write-Warning
        }

        #set our header
        if ($Staging)
        {
            $XAuthToken = (Get-RackerToken -Endpoint Staging).XAuthToken
        }
        else
        {
            $XAuthToken = (Get-RackerToken).XAuthToken
        }

        $header = @{'x-auth-token' = $XAuthToken.'x-auth-token' }
    }

    Process
    {
        try
        {
            #build our splat for Invoke-RestMethod
            $Params = @{
                Headers = $header
                Uri     = $URL
                Method  = $Method
            }

            #Add our body if JSON supplied
            If ($JSON)
            {
                $Params.Add("Body", $JSON)
                $Params.Add("ContentType", "application/json")
            }

            #add user agent if user specified one
            If ($UserAgent) { $Params.Add("UserAgent", $UserAgent) }

            $output = Invoke-RestMethod @Params
        }
        catch
        {
            switch -Wildcard ($_)
            {
                "*401*"
                {
                    Throw "ERROR: Invoke-Query :: Unauthorized"
                }
                "*404*"
                {
                    if ($Suppress404)
                    {
                        Write-Verbose  "ERROR SUPPRESSED: Invoke-Query :: URL $URL not found"
                    }
                    else
                    {
                        Throw "ERROR: Invoke-Query :: URL $URL not found"
                    }
                }
                "*Invalid Token*"
                {
                    Throw "ERROR: Invoke-Query :: Invalid Racker token"
                }
                "*500*"
                {
                    Throw "ERROR: Invoke-Query :: (500) Internal Server Error"
                }
                default
                {
                    Throw
                }
            }
        }

        #check for blank output
        if ((-not $output) -AND (-not $Suppress404))
        {

            Throw "ERROR: Invoke-Query :: Blank output"
        }
    }

    End
    {
        Write-Verbose "[$(Get-Date)] End   :: $($MyInvocation.MyCommand)"
        return $output
    }
}
