function Get-SecretServerToken
{
    <#
        .SYNOPSIS
        Gets an authentication token from SecretServer API

        .DESCRIPTION

        For Service Accounts using the Credential parameter, a token will be requested from Secret Server.

        For Racker authentication. If the required token is cached and is not expired and the flush switch is not specified,
        then token is returned from cache. Otherwise, it queries the API for token and saves to cache.
        If not cached or require user will be promoted for SSO username and password and RSA token.

        .PARAMETER Flush
        Discard cached token, if present, and retrieve a new token from the API. Not usually
        required, as the token is tested for expiry before being returned anyway.

        .PARAMETER Credential
        PSCredential object containing user name and password of a ServiceAccount for Secret server.
        This is designed for mass remediation/auditing of Secret Server, were permissions can be granted for a specific Service Account.
        Only use if you explicitly require a service account otherwise do not include this parameter and the SSO and RSA token will be used instead.
        ServiceAccount are not stored in password vault.

        .OUTPUTS
        [string]

        .EXAMPLE
        Get-SecretServerToken

        AgLkX0fL8WAvw36JEFcuHQ_5gxx2AyqlJasyUG4ipP0Lf06GD10GQdPoonsGe-1sMZuhhPYXviXXNoYiqaD1dZ69v7Ta
        y7DVpgKy--hJpCAgSAjJm7gkBNIgxttxtWdL9egKeH79Ep-wqpgfIUHwW0Lj46Whnu9I2Z3S8_29KlrCCH7s94-SIT5f
        faKtC1fIp4ASoDIoQE6_HiKBPfslhiDQf_mxaO-Ucoo6PjGIoRJJJOfUwd5J0VCNSD5o_ZCDX_kxj5QAJBvOc7eJ8dAP
        QnSZ19tr4soxd1O2MBBYuWpBBeVR8CZTKjiV6671weAN9ncqik1rTFxhkLqkoZ9o4i_RiFv1imBJRHrL6gTjQejJhcSX
        ...

        Output token as string from cache if present and not expired, otherwise gets a new token.

        .EXAMPLE
        Get-SecretServerToken -Flush

        AgLkX0fL8WAvw36JEFcuHQ_5gxx2AyqlJasyUG4ipP0Lf06GD10GQdPoonsGe-1sMZuhhPYXviXXNoYiqaD1dZ69v7Ta
        y7DVpgKy--hJpCAgSAjJm7gkBNIgxttxtWdL9egKeH79Ep-wqpgfIUHwW0Lj46Whnu9I2Z3S8_29KlrCCH7s94-SIT5f
        faKtC1fIp4ASoDIoQE6_HiKBPfslhiDQf_mxaO-Ucoo6PjGIoRJJJOfUwd5J0VCNSD5o_ZCDX_kxj5QAJBvOc7eJ8dAP
        QnSZ19tr4soxd1O2MBBYuWpBBeVR8CZTKjiV6671weAN9ncqik1rTFxhkLqkoZ9o4i_RiFv1imBJRHrL6gTjQejJhcSX
        ...

        Gets a new token.

        .EXAMPLE
        $Credential = Get-Credential
        Get-SecretServerToken -Credential $Credential

        GHJus98sdRRs8sdfsdffonnUjhbfYFctcrtcr875fvHChgcksdfsyUG4ipP0Lf06GD10GQdPoonsGe-1sMZuhhPYXviX
        y7DVpgKy--hJpCAgSAjJm7gkBNIgxttxtWdL9egKeH79Ep-wqpgfIUHwW0Lj46Whnu9I2Z3S8_29KlrCCH7s94-SIT5f
        faKtC1fIp4ASoDIoQE6_HiKBPfslhiDQf_mxaO-Ucoo6PjGIoRJJJOfUwd5J0VCNSD5o_ZCDX_kxj5QAJBvOc7eJ8dAP
        QnSZ19tr4soxd1O2MBBYuWpBBeVR8CZTKjiV6671weAN9ncqik1rTFxhkLqkoZ9o4i_RiFv1imBJRHrL6gTjQejJhcSX
        ...

        Gets a token for a service account provided as a credential object.
    #>
    [CmdletBinding(DefaultParameterSetName = 'NoParams')]
    [OutputType([string])]
    param
    (
        [Parameter(Position = 0, ParameterSetName = 'Flush')]
        [switch]$Flush,

        [Parameter(Position = 0, ParameterSetName = 'Credential')]
        [System.Management.Automation.PSCredential]$Credential
    )

    Process
    {
        If ($Credential)
        {
            $token = Invoke-SecretServerToken -Credential $Credential
            return $token
        }

        $Cred = Get-SsoCredential

        $PoshSecretSplat = @{
            Name     = "SecretServerToken"
            Username = $Cred.Username
        }

        # Retrieve from cache and check expiry
        if (-not $Flush)
        {
            # returns null if vault entry is not present
            $SecretServerToken = (Get-PoshSecret @PoshSecretSplat -AsPlaintext).Password

            If ($SecretServerToken)
            {
                $ValidToken = Test-SecretServerToken -SecretServerToken $SecretServerToken

                if($ValidToken)
                {
                    Write-Verbose "SecretServer Token in cache is valid"
                    return $SecretServerToken
                }
                Write-Verbose "SecretServer Token has Expired, getting new token"
            }
        }

        $RsaToken = Get-HiddenStringPrompt -Prompt 'rsa token'
        $RsaToken = $RsaToken | ConvertTo-SecureString -AsPlainText -Force

        $TokenSplat = @{
            Credential = $Cred
            RsaToken   = $RsaToken
        }
        $token = Invoke-SecretServerToken @TokenSplat

        If ($token)
        {
            $null = Add-PoshSecret @PoshSecretSplat -Password $token
            return $token
        }
    }
}
