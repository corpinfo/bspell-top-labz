﻿function Get-RackerImpersonationToken
{
    <#
        .SYNOPSIS
        Get a Racker Impersonation Token used in the monitoring APIs, amongst others

        .DESCRIPTION
        Get a Racker Impersonation Token used in the monitoring APIs, amongst others
        Supply the account number and recieve an Impersonation Token

        .PARAMETER Account
        The account to impersonate

        .OUTPUTS
        [psobject]

        .EXAMPLE
        Get a token for account 1103359
        Get-RackerImpersonationToken -Account 1103359

        .NOTES
        Written by Hounsou Dansou / Martin Howlett
    #>

    [CmdletBinding()]
    [OutputType([psobject])]
    param
    (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Account
    )

    begin
    {
        Write-Verbose "[$(Get-Date)] List of Parameters :: $($PSBoundParameters.GetEnumerator() | Out-String)"
        Write-Verbose "[$(Get-Date)] Begin :: $($MyInvocation.MyCommand)"
    }

    process
    {
        try
        {
            #add hybrid if we dont already have it
            if ($Account -notlike "hybrid*") {$Account = "hybrid$Account"}

            $AuthUrl = Get-AuthEndpoint -Endpoint Internal
            $Uri     = "$AuthUrl/RAX-AUTH/impersonation-tokens"

            $Payload = @{
                'RAX-AUTH:impersonation' = @{
                    'user' = @{'username' = $Account}
                    'expire-in-seconds' = 10800
                }
            }

            $Params = @{
                Uri         = $Uri
                Method      = 'Post'
                Body        = $Payload | ConvertTo-Json -Compress
                ContentType = 'application/json'
            }

            $AccountToken = Invoke-RestMethodWithAuth @Params
            write-output $AccountToken
        }
        catch
        {
            Write-Verbose "[$(Get-Date)] Info  :: $($MyInvocation.MyCommand)"
            Write-Verbose "[$(Get-Date)] Error :: $_.Exception.Message "
            throw
        }
    }

    end
    {
        Write-Verbose "[$(Get-Date)] End   :: $MyInvocation.MyCommand"
    }
}
