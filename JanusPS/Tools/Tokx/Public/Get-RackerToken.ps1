﻿function Get-RackerToken
{
    <#
        .SYNOPSIS
        Gets an authentication token from the Rackpsace identity API.

        .DESCRIPTION
        Securely caches authentication tokens in the local secret vault.

        If the required token:

        - is cached
        - is not expired, or within minutes of expiry
        - and the flush switch is not specified

        then this command returns the token from cache.

        Otherwise, this command will authenticate against an authentication provider.

        By default, this uses a browser-based authentication; single sign-on against ADFS. If that
        fails, it will fall back to prompting the user for an RSA pincode and authenticating against
        the Rackspace Identity Service.

        .PARAMETER SsoUsername
        The user's SSO.

        .PARAMETER RsaToken
        The user's RSA token.

        .PARAMETER UseRsa
        Specifies to use RSA pincode authentication, if needed. This prevents browser-based
        authentication, which may be useful on headless OSes.

        To force always using RSA pincode authentication, add this line to your profile:

        $Global:TOKX_USE_RSA = $true

        This also works as an environment variable.

        This parameter has no effect if also specifying 'SsoUsername' or 'RsaToken', or when a valid
        token can be returned from the cache.

        .PARAMETER Credential
        Provide a PSCredential object containing username and password.

        .PARAMETER Endpoint
        Specify which identity authentication endpoint to return (see Get-AuthEndpoint).

        .PARAMETER Flush
        Discard cached token, if present, and retrieve a new token from the API. Not usually
        required, as the token is tested for expiry before being returned anyway.

        .PARAMETER OutJson
        Output as json-formatted string. If this is not specified, output is PSObject.

        .PARAMETER AsXAuthToken
        Output only the X-Auth token, not the structured object. When used with OutJson, a quoted
        JSON string is returned.

        .PARAMETER ValidateOnly
        Does not store the token in PoshSecret if it is valid.

        .OUTPUTS
        [PSObject]

        By default, outputs a structured Powershell object.

        [string]

        When used with -OutJson, returns a json-formatted string.

        .EXAMPLE
        Get-RackerToken

        Access     XAuthToken
        ------     ----------
        @{access=} @{x-auth-token=TXkgcGFzc3dvcmQgaXMgJ2h1bnRlcjIn}

        Gets a racker token.

        .EXAMPLE
        Get-RackerToken -OutJson

        {"Access":{"access":{"serviceCatalog":[],"user":{"RAX-AUTH:defaultRegion":"","roles":[{"name":"Racker","id":"9"},{"name":"CORE_I...

        Gets a racker token, as a JSON-formatted string.
    #>

    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidGlobalVars", "")]

    [CmdletBinding(DefaultParameterSetName = 'BySso')]
    [OutputType([PSObject], [string])]
    param
    (
        [Parameter(ParameterSetName = 'ByRsa', Position = 0)]
        [string]$SsoUsername = $((Get-SsoUsername).ToLower()),

        [Parameter(ParameterSetName = 'ByRsa', Position = 1)]
        [string]$RsaToken,

        [Parameter(ParameterSetName = 'ByRsa')]
        [switch]$UseRsa = ($Global:TOKX_USE_RSA -or ($env:TOKX_USE_RSA -and $env:TOKX_USE_RSA -inotmatch '^(0|false)$')),

        [Parameter(ParameterSetName = 'ByCredential', Position = 1)]
        [System.Management.Automation.Credential()]
        [pscredential]$Credential,

        [Parameter(Position = 2)]
        [ValidateSet("Cloud", "Staging", "Internal")]
        [string]$Endpoint = $TOKX_DEFAULT_ENDPOINT,

        [Parameter()]
        [switch]$Flush,

        [Parameter()]
        [switch]$AsXAuthToken,

        [Parameter()]
        [switch]$OutJson,

        [Parameter()]
        [switch]$ValidateOnly
    )


    $JsonRackerToken = $RackerToken = $null
    $EndpointUrl = Get-AuthEndpoint -Endpoint $Endpoint


    $PoshSecretSplat = @{
        Name     = "RackerToken"
        Username = $Endpoint
    }

    if (-not $Flush)
    {
        # returns null if vault entry is not present
        $JsonRackerToken = (Get-PoshSecret @PoshSecretSplat -AsPlaintext).Password

        $RackerToken = $JsonRackerToken | ConvertFrom-Json -ErrorAction SilentlyContinue
    }
    # $RackerToken should now be either null or valid


    # Token is not present in vault, or user specified -Flush. Invoke the Identity API and cache the result.
    if (-not $RackerToken)
    {
        # Handle default parameter value not selecting parameter set
        $ParameterSetName = $PSCmdlet.ParameterSetName
        if ($ParameterSetName -eq 'BySso' -and $UseRsa)
        {
            $ParameterSetName = 'ByRsa'
        }

        # Generate the payload
        if ($ParameterSetName -eq 'ByRsa')
        {
            if (-not $RsaToken)
            {
                $RsaToken = (Get-HiddenStringPrompt -Prompt "RSA token")
            }

            $Uri         = [uri]($EndpointUrl + '/tokens')
            $ContentType = "application/json"
            $Body        = ConvertTo-Json @{
                'auth' = @{
                    'RAX-AUTH:domain' = @{
                        'name'     = 'Rackspace'
                    }
                    'rsaCredentials' = @{
                        'username' = $SsoUsername
                        'tokenKey' = $RsaToken
                    }
                }
            }

        }
        elseif ($ParameterSetName -eq 'ByCredential')
        {
            $Uri         = [uri]($EndpointUrl + '/tokens')
            $ContentType = "application/json"
            $Body        = ConvertTo-Json @{
                'auth' = @{
                    'RAX-AUTH:domain' = @{
                        'name'     = 'Rackspace'
                    }
                    'passwordCredentials' = @{
                        'username' = $Credential.Username
                        'password' = $Credential.GetNetworkCredential().Password
                    }
                }
            }

        }
        else    # SSO with ADFS
        {
            $Uri         = [uri]($EndpointUrl + "/RAX-AUTH/federation/saml/auth")
            $ContentType = "application/xml"

            try
            {
                $Body = Get-SamlToken -AsXml -Flush:$Flush
            }
            catch [NoHandlerForHttp]
            {
                Write-Error $_ -ErrorAction Continue
                Write-Information "This error can be worked around by calling Get-RackerToken -UseRsa." -InformationAction Continue

                $PSBoundParameters.UseRsa = $true
                return Get-RackerToken @PSBoundParameters
            }

            # Identity service will throw 400: 'SAML issueInstant cannot be older than 1800 seconds.'
            $IssueInstant = [datetime]$Body.Response.IssueInstant
            if ($IssueInstant -lt ([datetime]::Now - (New-TimeSpan -Seconds 1780)))
            {
                $Body = Get-SamlToken -AsXml -Flush
            }
        }


        # Invoke the API
        $EndpointSplat = @{
            Uri         = $Uri
            ContentType = $ContentType
            Body        = $Body
            Method      = "Post"
        }

        $TimesRetried, $Sleep = 0, 0
        while ($TimesRetried -le $MAX_HTTP_RETRIES)
        {
            try
            {
                $AuthToken = Invoke-RestMethod @EndpointSplat -ErrorAction Stop
                break
            }
            catch
            {
                $StatusCode = $_.Exception.Response.StatusCode.value__
                if ($NON_RETRYABLE_HTTP_CODES -contains $StatusCode)
                {
                    throw
                }
                elseif ($TimesRetried -ne $MAX_HTTP_RETRIES)
                {
                    $TimesRetried++
                    $Sleep = $Sleep + $BACKOFF_INCREMENT_SECONDS
                    Write-Verbose (
                        "Retrying API call in $($Sleep)s, due to retryable exception occurred $_. " +
                        "Status Code: $StatusCode. Attempt: $TimesRetried/$MAX_HTTP_RETRIES"
                    )
                    Start-Sleep $Sleep
                }
                else
                {
                    throw # Boil exception back up to user if we're on our last retry
                }
            }
        }

        if (-not $AuthToken)
        {
            throw "Error occurred acquiring auth token from $Endpoint API"
        }

        # Format the returned token
        $RackerToken = [pscustomobject]@{
            'Access'     = $AuthToken
            'XAuthToken' = [pscustomobject]@{
                'x-auth-token' = $AuthToken.access.token.id
            }
        }
        $JsonRackerToken = ConvertTo-Json $RackerToken -Depth 10 -Compress


        # Store the formatted token
        $Expiry = Get-Date $RackerToken.Access.access.token.expires
        if (-not $ValidateOnly)
        {
            Add-PoshSecret @PoshSecretSplat -Password $JsonRackerToken -Property @{URL = $EndpointUrl} -Expiry $Expiry
        }

    }
    # $RackerToken and $JsonRackerToken should now be both valid and not expired

    if ($AsXAuthToken)
    {
        $RackerToken = $RackerToken.XAuthToken.'x-auth-token'
        $JsonRackerToken = $RackerToken | ConvertTo-Json
    }

    if ($OutJson)
    {
        return $JsonRackerToken
    }
    else
    {
        return $RackerToken
    }
}
