﻿function Grant-IntCustomerAccess
{
    <#
        .SYNOPSIS
        Calls the self-service Intensive API to grant access to a customer account.

        .DESCRIPTION
        Adds the user's intensive .cust account to the <customer_account>-RAX group in the INTENSIVE domain,
        all child domains, and the GLOBALRS domain.

        .PARAMETER Account
        The customer account to which to grant access.

        .OUTPUTS
        [void]

        This command does not return any output.

        .EXAMPLE
        Grant-IntCustomerAccess -Account 1103359

        Adds the user's intensive .cust account to the 1103359-RAX group.
    #>
    [CmdletBinding()]
    [OutputType([void])]
    param
    (
        [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
        [ValidateNotNullOrEmpty()]
        [ValidatePattern('^[0-9]+$')]
        [string]$Account
    )

    process
    {
        # Check if we already have access for 45 minutes or more
        $GrantedAlready = Test-IntCustomerAccess -Account $Account
        if ($GrantedAlready)
        {
            Write-Verbose "Access has already been granted to account $Account."
            return
        }

        $IntUsername = Get-IntUsername -Cust
        $StrippedUsername = $IntUsername -replace "INTENSIVE\\"
        $Splat = @{
            ContentType  = "application/json"
            Uri          = "$SS_API/ad/v1/domains/intensive/users/$StrippedUsername/customeraccess/"
            Body         = $Account
            Method       = "Post"
            ErrorAction  = "Stop"
        }
        try
        {
            # This API endpoint is not the best to parse results out of, so we test further down.
            $ApiResult = Invoke-RestMethodWithAuth @Splat
        }
        catch
        {
            Write-Error "Unexpected error from self-service Intensive API: $($_.Exception.Message)"
            return
        }

        $AccessGranted = Test-IntCustomerAccess -Account $Account
        if ($AccessGranted)
        {
            Write-Verbose "Confirmed access has been granted to account $Account."
        }
        else
        {
            Write-Error "Could not confirm access has been granted to account $Account - response from API: $($ApiResult | Out-String)"
        }
    }
}
