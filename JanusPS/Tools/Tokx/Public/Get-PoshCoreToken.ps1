﻿function Get-PoshCoreToken
{
    <#
        .SYNOPSIS
        Returns authentication token from the CTKCore API

        .DESCRIPTION
        Securely caches authentication tokens; see PoshSecret module for specific documentation.

        If the required token is cached and is not expired and the flush switch is not specified, returns the token from cache. Otherwise, queries API for token and saves to cache. This will prompt for RSA token if not supplied.

        Tokens are 32 hex digits, returned as a secure string by default.

        Throws System.Security.Authentication.InvalidCredentialException if API returns a 403 (invalid credentials).

        .PARAMETER SsoUsername
        The users SSO

        .PARAMETER RsaToken
        The RSA token

        .PARAMETER Flush
        Discard cached token, if present, and retrieve a new token from the API. Not usually required, as the token is tested for expiry before being returned anyway.

        .PARAMETER AsPlainText
        Return token as a plaintext string. Default is to return a secure string.

        .PARAMETER Endpoint
        Specifies whether to fetch a token for the Core production or staging environments.

        .OUTPUTS
        [string]

        .EXAMPLE
        Get the token for mart6141 as plain text
        Get-PoshCoreToken -SsoUsername mart6141 -AsPlainText

        .NOTES
        Written by Freddie Sackur, IAWW-891
    #>

    [CmdletBinding()]
    [OutputType([string], [securestring])]
    param
    (
        [Parameter(Position = 0)]
        [string]$SsoUsername = $((Get-SsoUsername).Tolower()),

        [Parameter(Position = 1)]
        [string]$RsaToken,

        [Parameter()]
        [switch]$Flush,

        [Parameter()]
        [switch]$AsPlainText,

        [Parameter()]
        [ValidateSet("Production", "Staging")]
        [string]$Endpoint = "Production"
    )

    #null our coretoken so we do not accidently get it from parent scope
    $CoreToken = $null

    $SecretSplat = @{
        Name     = if ($Endpoint -eq "Staging") {"CoreTokenStaging"} else {"CoreToken"}
        Username = $SsoUsername
    }

    #Retrieve from cache, if available.
    if (-not $Flush)
    {
        #This returns null if vault entry is not present or if expiry time was stored and is expired
        $CoreToken = (Get-PoshSecret @SecretSplat -AsPlaintext).Password

    } #$CoreToken should now be either null or valid

    #Token is not present as a secret, or user specified -Flush. Invoke the Core API and cache the result.
    if (-not $CoreToken)
    {
        if ($Endpoint -eq "Staging")
        {
            # Staging env uses a single password which works for any SSO that prod accepts (sync with prod may be days, not minutes)
            # To get access to staging, request access to this PasswordSafe project
            $ProjectId    = 3514
            $CredId       = 33318
            $PWSafeSplat  = @{
                Uri         = "https://passwordsafe.corp.rackspace.com/projects/$ProjectId/credentials/$CredId"
                Headers     = @{Accept = 'application/json'}
                ErrorAction = 'Stop'
            }

            if ($RsaToken)
            {
                # Has no effect if token already cached. If it isn't, then this uses the RSA, which
                # will cache the token for Invoke-RestMethodWithAuth
                $null = Get-RackerToken -RsaToken $RsaToken
            }

            try
            {
                $PWSafeReturn = Invoke-RestMethodWithAuth @PWSafeSplat      # TODO: replace with PWSafe call when GTSWAE-7709 is done
            }
            catch
            {
                # This pattern preserves the underlying error while adding to the message
                # $PSCmdlet.WriteError also works
                # Write-Error obscures the original error
                $_.ErrorDetails = "Failed to fetch Core staging credential from $($PWSafeSplat.Uri): $_"
                $PSCmdlet.ThrowTerminatingError($_)
            }

            $Password = $PWSafeReturn.credential.password
            $Uri      = "https://ws.staging.core.rackspace.com/ctkapi/login/$SsoUsername"

        }
        else
        {
            if (-not $RsaToken) {$RsaToken = Get-HiddenStringPrompt -Prompt "RSA token"}

            $Uri      = "https://ws.core.rackspace.com/ctkapi/login/$SsoUsername"
            $Password = $RsaToken
        }

        $RestSplat = @{
            Uri         = $Uri
            Body        = @{password = $Password} | ConvertTo-Json
            Method      = "Post"
            ErrorAction = "Stop"
        }

        try
        {
            $CoreToken = (Invoke-RestMethod @RestSplat).authtoken

        }
        catch
        {
            if ($_ -match '\(403\) Forbidden')
            {
                throw (New-Object System.Security.Authentication.InvalidCredentialException (
                    "Error fetching Core token: Invalid credentials: $_"
                ))
            }
            else
            {
                throw "Error fetching Core token: $_"
            }
        }

        $Expiry = (Get-Date).AddSeconds($TOKX_CORETOKEN_LIFETIME_SECONDS)

        Add-PoshSecret @SecretSplat -Password $CoreToken -Expiry $Expiry

    } #$CoreToken should now be valid and not expired

    if ($AsPlainText)
    {
        return $CoreToken
    }
    else
    {
        return ConvertTo-SecureString $CoreToken -AsPlainText -Force
    }
}
