﻿function Get-IntCustomerAccess
{
    <#
        .SYNOPSIS
        Returns a list of all customer accounts you currently have access to, as well as the expiry time.

        .DESCRIPTION
        Returns a list of all customer accounts you currently have access to, as well as the expiry time.

        .EXAMPLE
        Get-IntCustomerAccess

        Account Expires
        ------- -------
        1103359 29/06/2020 18:00:58
        933316  29/06/2020 18:02:55

        Returns customer accounts you currently have access to. In this case, I currently have access to account 1103359 and 933316.

        .OUTPUTS
        [psobject]
    #>
    [CmdletBinding()]
    [OutputType([psobject])]
    param
    ()

    begin
    {
        $OutputProperties = @(
            @{Name = "Account"; Expression = {$_.Name}}
            @{Name = "Expires"; Expression = {Get-Date $_.Value}}
        )
    }

    process
    {
        $IntUsername = Get-IntUsername -Cust
        $StrippedUsername = $IntUsername -replace "INTENSIVE\\"
        $Splat = @{
            ContentType  = "application/json"
            Uri          = "$SS_API/ad/v1/domains/intensive/users/$($StrippedUsername)?attributes=rsactiveaccess"
            Method       = "Get"
            ErrorAction  = "Stop"
        }
        try
        {
            $Result = Invoke-RestMethodWithAuth @Splat
        }
        catch
        {
            Write-Error "Unexpected error from self-service Intensive API: $($_.Exception.Message)"
            return
        }

        $GrantedAccounts = $Result.attributes.rsactiveaccess | ConvertFrom-Json
        $GrantedAccounts.PSObject.Properties | Select-Object $OutputProperties
    }
}
