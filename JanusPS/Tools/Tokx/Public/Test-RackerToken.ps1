﻿function Test-RackerToken
{
    <#
        .SYNOPSIS
        Test if a racker token is valid

        .DESCRIPTION
        Given a token, returns true or false to indicate whether the token is valid

        This is only of use if you wish to control whether the end user gets prompted for an RSA key. If interactive prompts
        must be avoided, then test for the value of (Test-RackerToken -TestCached) to determine whether you need to supply the
        RsaToken parameter when calling Get-RackerToken.

        .PARAMETER RackerToken
        The authentication token to test, passed as a structured PSObject

        .PARAMETER JsonRackerToken
        The authentication token to test, passed as a JSON string

        .PARAMETER TestApiAuth
        Specifies to test the token against the API. By default, the token is only examined for valid structure (we test if it looks correct, but we don't submit it to the server for testing)

        .PARAMETER TestCachedToken
        Do not use
        This is only for backward compatibility. Tests the "Global variable" token (which may
        actually be a credential manager entry rather than a variable)

        .PARAMETER Endpoint
        Specify which identity authentication endpoint to return (see Get-AuthEndpoint).

        .OUTPUTS
        Bool

        .EXAMPLE
        Return true if token is valid; otherwise, false.

        Test-RackerToken

        .NOTES
        Written by Hounsou Dansou, updated Freddie Sackur IAWW-903
    #>
    [CmdletBinding(DefaultParameterSetName='Legacy')]
    [OutputType([void], ParameterSetName='Legacy')]     # We'll remove this once we've remedied all the calling code that makes it throw
    [OutputType([bool], ParameterSetName='Default')]
    [OutputType([bool], ParameterSetName='Json')]
    [OutputType([bool], ParameterSetName='TestCached')]
    param
    (
        [Parameter(ParameterSetName='Default', Position=0)]
        [psobject]$RackerToken,

        [Parameter(ParameterSetName='Json')]
        [string]$JsonRackerToken,

        [Parameter(ParameterSetName='TestCached')]
        [Alias('ReturnBoolean')]  # for backward compatibility only
        [switch]$TestCachedToken,

        [Parameter(ParameterSetName='TestCached')]
        [ValidateSet("Cloud", "Staging", "Internal")]
        [string]$Endpoint = $TOKX_DEFAULT_ENDPOINT,

        [Parameter()]
        [switch]$TestApiAuth
    )


    #Populate $RackerToken with psobject, if it wasn't specified in the parameters
    switch ($PSCmdlet.ParameterSetName)
    {
        "TestCached"
        {
            $PoshSecretSplat = @{
                Name = "RackerToken"
                Username = $Endpoint
                AsPlaintext = $true
            }
            $JsonRackerToken = (Get-PoshSecret @PoshSecretSplat).Password

            try
            {
                $RackerToken = ConvertFrom-Json $JsonRackerToken -ErrorAction Stop
            }
            catch
            {
                return $false
            }
        }

        "Legacy"
        {
            throw (
                "function Test-RackerToken has been updated; you need to update your code to use Get-RackerToken if you want to fetch a token from the API. " +
                "If you only want to test whether the cached token is valid, use the -TestCachedToken switch."
            )
        }

        "Json"
        {
            try
            {
                $RackerToken = ConvertFrom-Json $JsonRackerToken -ErrorAction Stop
            }
            catch
            {
                return $false
            }
        }
        default {}
    }

    $RegexBase64 = '^([A-Za-z0-9+/=\-_]){1,}$'

    [bool]$IsValidStructure = (
        $RackerToken.Access.access.token.id -match $RegexBase64 -and
        $RackerToken.Access.access.user.id -is "string" -and
        $RackerToken.Access.access.serviceCatalog -is "Array" -and
        $RackerToken.XAuthToken.'x-auth-token' -match $RegexBase64 -and
        $(try {(Get-Date $RackerToken.Access.access.token.expires) -is "DateTime"} catch {$false})
    )

    if ($TestApiAuth)
    {
        $Uri = "{0}/tokens/{1}" -f (Get-AuthEndpoint -Endpoint $Endpoint), $RackerToken.access.access.token.id
        try
        {
            $null = Invoke-RestMethod -Uri $Uri -Headers $RackerToken.XAuthToken -ErrorAction Stop
        }
        catch
        {
            return $false
        }
    }

    return ($IsValidStructure -and
        $(
            $Expiry = Get-Date $RackerToken.Access.access.token.expires;
            $Expiry -gt (Get-Date).AddSeconds($TOKX_MINIMUM_SECONDS_BEFORE_EXPIRY)  #Token date is in the future
        )
    )
}
