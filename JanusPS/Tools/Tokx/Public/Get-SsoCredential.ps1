function Get-SsoCredential
{
    <#
        .SYNOPSIS
        Gets the valid SSO password of the user

        .DESCRIPTION
        Securely caches SSO passwords using the PoshSecret module.

        If the SSO password is cached and is valid and the flush switch is not specified,
        returns the SSO from cache. Otherwise, queries Identity API to validate SSO password and saves to cache. Will
        prompt for SSO password if not found in cache nor a PSCredential are supplied.

        .PARAMETER SsoUsername
        The user's SSO, without the domain prefix.

        .PARAMETER Credential
        Provide a PSCredential object containing username and password.

        .PARAMETER Flush
        Discard cached SSO, if present, and store a new SSO once validated. Not usually
        required, as the SSO is validated before being returned anyway.

        .PARAMETER AsPlainText
        When specified, this will return the password in plain text.

        .OUTPUTS
        [PSCredential]
        By default, outputs a PSCredential object

        [PSObject]
        When used with -AsPlainText, returns a PSObject

        .EXAMPLE
        Get-SsoCredential

        UserName                     Password
        --------                     --------
        ryan0603 System.Security.SecureString

        Outputs SSO as PSCredential.

        .EXAMPLE
        Get-SsoCredential -SsoUsername whamapi-cicd-svc

        UserName                     Password
        --------                     --------
        whamapi-cicd-svc System.Security.SecureString

        Output alternate SSO as PSCredential

        .EXAMPLE
        Get-SsoCredential -AsPlainText

        Name Username               SecurePassword Password
        ---- --------               -------------- --------
        SSO  ryan0603 System.Security.SecureString REDACTED

        Output SSO as PSObject
    #>
    [CmdletBinding(DefaultParameterSetName = 'Sso')]
    [OutputType([PSObject], [PSCredential])]
    param
    (
        [Parameter(Position = 0, ParameterSetName = 'Sso')]
        [string]$SsoUsername = $((Get-SsoUsername).Tolower()),

        [Parameter(Position = 0, Mandatory = $true, ParameterSetName = 'Credential')]
        [System.Management.Automation.Credential()]
        [PSCredential]$Credential,

        [Parameter()]
        [switch]$Flush,

        [Parameter()]
        [switch]$AsPlainText

    )
    begin
    {
        Write-Verbose "[$(Get-Date)] Begin :: $($MyInvocation.MyCommand)"
        Write-Verbose "[$(Get-Date)] List of Parameters :: $($PSBoundParameters.GetEnumerator() | Out-String)"
    }

    process

    {
        if ($Credential)
        {
            $SsoUsername = $Credential.Username
        }

        $PoshSecretSplat = @{
            Name     = "Sso"
            Username = $SsoUsername
        }

        # Retrieve from cache and check expiry
        if (-not $Flush)
        {
            # returns null if vault entry is not present
            $SsoPassword = Get-PoshSecret @PoshSecretSplat -AsPSCredential

            if ($SsoPassword)
            {
                # Okay, we have an SSO in the cache, have we recently validated it?
                $PoshSecretSplat.Name = "SsoValid"
                $ValidSso = Get-PoshSecret @PoshSecretSplat
                if ($ValidSso)
                {
                    Write-Verbose "$SsoUsername password in cache is valid!"
                    $CredObject = Get-PoshSecret @PoshSecretSplat -AsPSCredential
                }

                # Hit Identity for a new Rackertoken - note, we do not store this as not all APIs work with non-RSA auth tokens.
                else
                {
                    Write-Verbose "Validating $(Get-SsoUsername) password against Identity API..."
                    try
                    {
                        Get-RackerToken -Credential $SsoPassword -Flush -ValidateOnly | Out-Null
                        $Splat = @{
                            Name     = "SsoValid"
                            Username = (Get-SsoUsername).ToLower()
                            Password = $SsoPassword.GetNetworkCredential().Password
                            Expiry   = (Get-Date).AddHours(21)
                        }
                        Add-PoshSecret @Splat # For checking the SSO has been recently checked
                        $CredObject = $SsoPassword
                        Write-Verbose "$(Get-SsoUsername) password is valid, caching for 21h..."
                    }
                    catch
                    {
                        $SsoPassword = $null
                        Write-Error "Unable to validate SSO password! $($_.Exception.Message)"
                    }
                }
            }
        }

        # SSO is not present in vault, or user specified -Flush.
        if (-not $SsoPassword)
        {
            if (-not $Credential)
            {
                $Password = Get-HiddenStringPrompt -Prompt "Password" | ConvertTo-SecureString -AsPlainText -Force
                $CredObject = New-Object Management.Automation.PSCredential ($SsoUsername, $Password)
            }

            else
            {
                $CredObject = $Credential
            }

            try
            {
                Get-RackerToken -Credential $CredObject -Flush -ValidateOnly | Out-Null
                $Splat = @{
                    Username = (Get-SsoUsername).ToLower()
                    Password = $CredObject.GetNetworkCredential().Password
                    Expiry   = (Get-Date).AddHours(21)
                }
                Write-Verbose "$(Get-SsoUsername) password is valid, caching for 21h..."
                Add-PoshSecret @Splat -Name SsoValid # For checking the SSO has been recently checked
                $Splat.Remove('Expiry')
                Write-Verbose "$(Get-SsoUsername) password is valid, storing Sso..."
                Add-PoshSecret @Splat -Name Sso      # For future requests

            }
            catch
            {
                Write-Error "Unable to validate SSO password! $($_.Exception.Message)"
                return
            }
        }

        #Output
        if ($AsPlainText)
        {
            Get-PoshSecret @PoshSecretSplat -AsPlaintext
        }

        else
        {
            $CredObject
        }
    }

    end
    {
        Write-Verbose "[$(Get-Date)] End   :: $($MyInvocation.MyCommand)"
    }
}
