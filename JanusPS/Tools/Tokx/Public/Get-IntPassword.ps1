function Get-IntPassword
{
    <#
        .SYNOPSIS
        Gets the password for the user's intensive or intensive .cust account.

        .DESCRIPTION
        Gets the password for the user's intensive or intensive .cust account.

        If the password has expired, or is close to expiry, it will be rotated.

        .PARAMETER Cust
        Specifies to get the password for the .cust account.

        .PARAMETER Flush
        Specifies to rotate the password for the .cust account, even if it is not
        expired or close to expiry.

        .PARAMETER AsPlaintext
        Specifies to get the password for the .cust account.

        .OUTPUTS
        [PSCredential]

        By default, the password is returned as a PSCredential object.

        [string]

        If the -AsPlaintext parameter is specified, the password is returned as a string.

        .EXAMPLE
        Get-IntPassword -Cust -AsPlaintext

        n34ji3OBqWsC3JbW

        Gets the password for the user's intensive .cust account.

        .NOTES
        The time defined as 'close to expiry' is a feature of the PoshSecret module.
    #>
    [CmdletBinding(DefaultParameterSetName = "AsPSCredential")]
    [OutputType([PSCredential], ParameterSetName = "AsPSCredential")]
    [OutputType([string], ParameterSetName = "AsPlaintext")]
    param
    (
        [Parameter()]
        [switch]$Cust,

        [Parameter()]
        [switch]$Flush,

        [Parameter(Mandatory, ParameterSetName = "AsPlaintext")]
        [switch]$AsPlaintext
    )

    $IntUsername = Get-IntUsername -Cust:$Cust
    if ($Flush)
    {
        Remove-PoshSecret -Name "Intensive" -Username $IntUsername -ErrorAction SilentlyContinue
    }

    $IntPassword = Get-PoshSecret -Name "Intensive" -Username $IntUsername -AsPSCredential
    if (-not $IntPassword)
    {
        $StrippedUsername = $IntUsername -replace "INTENSIVE\\"
        $Splat = @{
            ContentType  = "application/json"
            Uri          = "$SS_API/ad/v1/domains/intensive/users/$StrippedUsername/password"
            Method       = "Get"
            ErrorAction  = "Stop"
        }
        try
        {
            $Intensive = Invoke-RestMethodWithAuth @Splat
        }
        catch
        {
            Write-Error "Unexpected error from self-service Intensive API: $($_.Exception.Message)"
            return
        }

        $Password = $Intensive.Password
        $Expiry = Get-Date $Intensive.expires
        Add-PoshSecret -Name "Intensive" -Username $IntUsername -Password $Password -Expiry $Expiry
        $IntPassword = Get-PoshSecret -Name "Intensive" -Username $IntUsername -AsPSCredential
    }

    if ($AsPlaintext)
    {
        return $IntPassword.Password | ConvertTo-PlainText
    }
    else
    {
        return $IntPassword
    }
}
