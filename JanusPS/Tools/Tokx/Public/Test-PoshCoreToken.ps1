﻿function Test-PoshCoreToken
{
<#
    .SYNOPSIS
    Test if a racker token is valid

    .DESCRIPTION
    Given a token, returns true or false to indicate whether the token is valid

    This is only of use if you wish to control whether the end user gets prompted for an RSA key. If interactive prompts
    must be avoided, then test for the value of (Test-PoshCoreToken -TestCached) to determine whether you need to supply the
    RsaToken parameter when calling Get-PoshCoreToken.

    .PARAMETER SsoUsername
    The users SSO

    .PARAMETER CoreToken
    The authentication token to test, passed as a string

    .PARAMETER TestCachedToken
    Do not use. This is only for backward compatibility. Tests the token from the credential manager entry

    .OUTPUTS
    Bool

    .EXAMPLE
    Return true if token is valid; otherwise, false.

    Test-PoshCoreToken

    .NOTES
    Written by Freddie Sackur, IAWW-891
#>
    [CmdletBinding(DefaultParameterSetName='Default')]
    [OutputType([bool])]
    param
    (
        [Parameter(ParameterSetName='Default', Position=0)]
        [string]$CoreToken,

        [Parameter(ParameterSetName='TestCached', Mandatory=$true)]
        [switch]$TestCachedToken,

        [Parameter(ParameterSetName='TestCached')]
        [string]$SsoUsername = $((Get-SsoUsername).Tolower())
    )

    if ($TestCachedToken)
    {
        $CoreToken = $null

        $GetPoshSecretSplat = @{
            Name = "CoreToken";
            Username = $SsoUsername;
            AsPlaintext = $true
        }
        try
        {
             #This returns null if vault entry is not present, which fails validation on CoreToken
            $CoreToken = (Get-PoshSecret @GetPoshSecretSplat).Password
        }
        catch
        {
            return $false
        }
    }

    #Bail early if not correct format
    if ($CoreToken -inotmatch '[0-9a-f]{32}') {return $false}

    #Query core to confirm if it is valid
    $OldEAP = $ErrorActionPreference; $ErrorActionPreference = 'SilentlyContinue'

    $URL = "https://ws.core.rackspace.com/ctkapi/session/$COREToken"
    $CoreTokenValid = (Invoke-RestMethod -Uri $URL -Method GET).valid

    $ErrorActionPreference = $OldEAP

    return $CoreTokenValid
}
