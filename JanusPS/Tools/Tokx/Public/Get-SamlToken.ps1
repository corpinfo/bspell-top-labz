﻿function Get-SamlToken
{
    <#
        .SYNOPSIS
        Uses a browser-based authentication flow to obtain a SAML token.

        .DESCRIPTION
        Checks the local secret store to see if a SAML token is saved and within lifetime. If so, it
        is returned immediately. Otherwise, uses the default browser for authentication.

        This command will not work on headless OSes where a browser is not available. It needs to be
        possible to provide an HTTP URI to Start-Process.

        .PARAMETER AsBase64
        By default, this command returns a Base64-encoded SAML token. However, you can use this
        parameter to make that output explicit.

        .PARAMETER AsXmlString
        Specifies to decode the token from Base64 and output the token formatted as XML.

        .PARAMETER AsXml
        Specifies to decode the token and parse it into an XML document, which can be navigated for
        identity claims, tokens, lifetime, etc.

        .PARAMETER Flush
        Specifies to remove any stored token from the local secret store and perform browser-based
        authentication.

        This does not remove any cookies from the browser.

        .OUTPUTS
        [string]

        By default, this command returns a Base64-encoded SAML token.

        [string]

        When -AsXmlString is specified, this command returns an XML-formatted string.

        [Xml]
        When the -AsXml parameter is specified, this command returns the token as an XML document,
        which can be navigated for identity claims, tokens, lifetime, etc.

        .EXAMPLE
        Get-SamlToken

        IlRoZXkncmUgbWFkZSBvdXQgb2YgbWVhdC4iCgoiTWVhdD8iCgoiTWVhdC4gVGhleSdyZSBtYWRlIG91dCBvZiBtZWF0
        LiIKCiJNZWF0PyIKCiJUaGVyZSdzIG5vIGRvdWJ0IGFib3V0IGl0LiBXZSBwaWNrZWQgc2V2ZXJhbCBmcm9tIGRpZmZl
        cmVudCBwYXJ0cyBvZiB0aGUgcGxhbmV0LCB0b29rIHRoZW0gYWJvYXJkIG91ciByZWNvbiB2ZXNzZWxzLCBwcm9iZWQg
        dGhlbSBhbGwgdGhlIHdheSB0aHJvdWdoLiBUaGV5J3JlIGNvbXBsZXRlbHkgbWVhdC4iCgoiVGhhdCdzIGltcG9zc2li
        bGUuIFdoYXQgYWJvdXQgdGhlIHJhZGlvIHNpZ25hbHM/IFRoZSBtZXNzYWdlcyB0byB0aGUgc3RhcnMuIgoKIlRoZXkg
        dXNlIHRoZSByYWRpbyB3YXZlcyB0byB0YWxrLCBidXQgdGhlIHNpZ25hbHMgZG9uJ3QgY29tZSBmcm9tIHRoZW0uIFRo
        ZSBzaWduYWxzIGNvbWUgZnJvbSBtYWNoaW5lcy4iCgoiU28gd2hvIG1hZGUgdGhlIG1hY2hpbmVzPyBUaGF0J3Mgd2hv
        IHdlIHdhbnQgdG8gY29udGFjdC4iCgoiVGhleSBtYWRlIHRoZSBtYWNoaW5lcy4gVGhhdCdzIHdoYXQgSSdtIHRyeWlu
        ZyB0byB0ZWxsIHlvdS4gTWVhdCBtYWRlIHRoZSBtYWNoaW5lcy4iCgoiVGhhdCdzIHJpZGljdWxvdXMuIEhvdyBjYW4g
        bWVhdCBtYWtlIGEgbWFjaGluZT8gWW91J3JlIGFza2luZyBtZSB0byBiZWxpZXZlIGluIHNlbnRpZW50IG1lYXQuIgoK
        IkknbSBub3QgYXNraW5nIHlvdSwgSSdtIHRlbGxpbmcgeW91LiBUaGVzZSBjcmVhdHVyZXMgYXJlIHRoZSBvbmx5IHNl
        bnRpZW50IHJhY2UgaW4gdGhlIHNlY3RvciBhbmQgdGhleSdyZSBtYWRlIG91dCBvZiBtZWF0LiI=

        Gets a SAML token from ADFS. If the browser holds the required cookies, this is
        non-interactive; otherwise, the user will need to authenticate in the browser.

        .EXAMPLE
        Get-SamlToken -AsXmlString

        <samlp:Response ID="_972e8aa2-a7ab-4c99-a26b-45b64d4e66b3" Version="2.0" IssueInstant="2021-
        01-25T10:44:27.058Z" Destination="https://stepladder.rax.io/static/tokx_adfs_bounce" Consent
        ="urn:oasis:names:tc:SAML:2.0:consent:unspecified" xmlns:samlp="urn:oasis:names:tc:SAML:2.0:
        protocol"><Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">urn:federation:rackspace</Is
        suer><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:SignedInfo><ds:Canonica
        lizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" /><ds:SignatureMethod Alg

        Gets a SAML token as an XML-formatted string.

        .EXAMPLE
        Get-SamlToken -AsXml | Select-Object -ExpandProperty Response


        ID           : _e07a51d2-b927-4584-8d17-0d2f2862ac67
        Version      : 2.0
        IssueInstant : 2021-01-16T23:01:13.082Z
        Destination  : https://stepladder.rax.io/static/tokx_adfs_bounce
        Consent      : urn:oasis:names:tc:SAML:2.0:consent:unspecified
        samlp        : urn:oasis:names:tc:SAML:2.0:protocol
        Issuer       : Issuer
        Signature    : Signature
        Status       : Status
        Assertion    : Assertion


        Gets a SAML token from ADFS, as an XML document. In this example, we have expanded the
        top-level 'Response' node.
    #>

    [OutputType([string], ParameterSetName = 'AsString')]
    [OutputType([PSObject], ParameterSetName = 'AsXml')]
    [OutputType([string], ParameterSetName = 'AsXmlString')]
    [CmdletBinding(DefaultParameterSetName = 'AsBase64')]
    param
    (
        [Parameter(ParameterSetName = 'AsXml', Mandatory)]
        [switch]$AsXml,

        [Parameter(ParameterSetName = 'AsXmlString', Mandatory)]
        [switch]$AsXmlString,

        [Parameter(ParameterSetName = 'AsBase64')]
        [switch]$AsBase64,

        [Parameter()]
        [switch]$Flush
    )


    $ShouldWriteSecret = $SamlTokenBase64 = $SamlTokenXml = $SamlTokenXmlString = $null


    $SecretSplat = @{
        Name     = "SamlToken"
        Username = (Get-SsoUsername)
    }
    [string]$SamlTokenBase64 = (Get-PoshSecret @SecretSplat -AsPlaintext).Password


    if ($Flush -or -not $SamlTokenBase64)
    {
        $SamlTokenBase64 = Request-SamlToken -RelyingParty tokx -ErrorAction Stop

        $ShouldWriteSecret = $true
    }


    if ($ShouldWriteSecret -or $PSCmdlet.ParameterSetName -match 'AsXml')
    {
        # This is ConvertFrom-Base64, which exists elsewhere. However, Tokx is used in a lot of
        # places. I'm very keen to keep load times down for this key module.
        $Bytes              = [Convert]::FromBase64String($SamlTokenBase64)
        $SamlTokenXmlString = [Text.Encoding]::UTF8.GetString($Bytes)
        $SamlTokenXml       = [xml]$SamlTokenXmlString
    }


    if ($ShouldWriteSecret)
    {
        [datetime]$Expiry = $SamlTokenXml.Response.Assertion.Conditions.NotOnOrAfter

        Add-PoshSecret @SecretSplat -Password $SamlTokenBase64 -Expiry $Expiry
    }


    if ($PSCmdlet.ParameterSetName -eq 'AsXml')
    {
        return $SamlTokenXml
    }
    elseif ($PSCmdlet.ParameterSetName -eq 'AsXmlString')
    {
        return $SamlTokenXmlString
    }
    else
    {
        return $SamlTokenBase64
    }
}
