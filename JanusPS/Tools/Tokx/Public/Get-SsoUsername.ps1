﻿function Get-SsoUsername
{
<#
    .SYNOPSIS
    Gets the SSO username

    .DESCRIPTION
    Code exists that assumes your SSO username is the same as your current logon username. This function provides the actual SsoUsername.
    If the SsoUsername environment variable exists, this function returns the value of it. Otherwise, this function returns the current logged-on username.
    Use in conjunction with Set-SsoUsername.

    .OUTPUTS
    SsoUsername

    .EXAMPLE
    Get-SsoUsername
    Returns the user's SSO username

    .NOTES
    Author: Freddie Sackur
    Date: 2016-09-09T14:03:17
    Jira: IAWW-627

#>
    if ($env:SsoUsername)
    {
        return $env:SsoUsername
    }
    else
    {
        return $(($env:USERNAME).Tolower())
    }
}
