Get-ChildItem $PSScriptRoot\Private -Filter '*.ps1' | Foreach-Object { . $_.FullName }
Get-ChildItem $PSScriptRoot\Public -Filter '*.ps1' | Foreach-Object { . $_.FullName }

#Enable current session use TLS 1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.ServicePointManager]::SecurityProtocol -bor [Net.SecurityProtocolType]::Tls12

$VarParams = @{
    'Name'        = 'TOKX_DEFAULT_ENDPOINT'
    'Value'       = 'Internal'
    'Description' = 'Default authentication endpoint to use, if not specified'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'ReadOnly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'TOKX_MINIMUM_SECONDS_BEFORE_EXPIRY'
    'Value'       = 600
    'Description' = 'A token or password is discarded if it has less than this long before it expires'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'ReadOnly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'TOKX_CORETOKEN_LIFETIME_SECONDS'
    'Value'       = 7200
    'Description' = 'Core tokens last for 2 hours'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'ReadOnly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'FIREENGINE_URL_ROOT'
    'Value'       = 'https://fe.netsec.rackspace.net'
    'Description' = '#The url root is used in certain functions as the default value'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'          = 'TOKX_SECRETSERVER_PROD_ENDPOINT'
    'Value'         = 'https://secretserver.datapipe.net/SecretServer/webservices/sswebservice.asmx'
    'Description'   = 'Secret Server Production Endpoint'
    'Scope'         = 'Script'
    'Force'         = $True
    'Option'        = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'          = 'TOKX_SECRETSERVER_TOKEN_LIFETIME_SECONDS'
    'Value'         = 43200
    'Description'   = 'SecretServer tokens last for 12 hours'
    'Scope'         = 'Script'
    'Force'         = $True
    'Option'        = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'SS_API'
    'Value'       = 'https://api.selfservice.intensive.int'
    'Description' = '#The url root is used in certain functions as the default value'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'SAML_LISTENER_URI'
    'Value'       = 'http://127.0.0.1:29003'
    'Description' = 'Address that Tokx listens on for SAML postbacks from ADFS'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'ADFS_SUCCESS_PAGE_URI'
    'Value'       = 'https://stepladder.rax.io/static/tokx_adfs/tokx-authenticated.html'
    'Description' = 'Page we redirect to when SAML response is successful'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'ADFS_FAILURE_PAGE_URI'
    'Value'       = 'https://stepladder.rax.io/static/tokx_adfs/tokx-auth-failed.html'
    'Description' = 'Page we redirect to when SAML response is unsuccessful'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'MAX_HTTP_RETRIES'
    'Value'       = 3
    'Description' = 'Default max retry count'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'BACKOFF_INCREMENT_SECONDS'
    'Value'       = 3
    'Description' = 'Amount of time to sleep between each failed request.'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'NON_RETRYABLE_HTTP_CODES'
    'Value'       = @(401, 404)
    'Description' = 'Status codes we do not retry on'
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'readonly'
}
Set-Variable @VarParams
