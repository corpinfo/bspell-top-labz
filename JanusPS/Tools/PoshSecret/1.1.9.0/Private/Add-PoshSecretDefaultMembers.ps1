
function Add-PoshSecretDefaultMembers
{
    <#
        .SYNOPSIS
        Applies formatting info to the PoshSecret object

        .DESCRIPTION
        Pass-by-reference - the original object is updated

        .PARAMETER PoshSecret
        Pass-by-reference - the original object is updated

        .OUTPUTS
        [object]

        .EXAMPLE
        None

    #>

    #too complex to rename this now so ignoring
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseSingularNouns", "")]
    #too much risk rewriting this function for no gain
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingPositionalParameters", "")]
    param
    (
            [Parameter(Mandatory=$True)]
            [object]$PoshSecret
    )

    $PoshSecret.PSTypeNames.Insert(0, 'Rax.WindowsAutomation.PoshSecret')
    Add-Member -InputObject $PoshSecret MemberSet PSStandardMembers $(
        $Display = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet', [string[]]('Name', 'Username', 'SecurePassword', 'Password'));
        $Sort = New-Object System.Management.Automation.PSPropertySet('DefaultKeyPropertySet', [string[]]('Name', 'Username'));
        [System.Management.Automation.PSMemberInfo[]]@($Display, $Sort)
    ) -Force
}
