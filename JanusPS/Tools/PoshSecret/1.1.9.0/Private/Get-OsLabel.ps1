function Get-OsLabel
{
    <#
        .SYNOPSIS
        Get the Os label on Windows, macOS ot Linux

        .DESCRIPTION
        Return a string that define the Os running on the computer.

        .OUTPUTS
        [String]

        .EXAMPLE
        Get-OsLabel

        Returns one of the following string values: macOS, Windows, Linux
        #>

    [CmdletBinding()]
    [OutputType([Object])]

    Param()

    $OS = $PSVersionTable.OS

    switch ($OS)
    {
        {$_ -match 'Darwin'} {$Label = 'macOS'}
        {$_ -match 'Linux'} {$Label = 'Linux'}
        {$_ -match 'Windows'} {$Label = 'Windows'}
        $null {$Label = 'Windows'}
    }
    $Label
}
