function Get-SettingPath
{
    <#
        .SYNOPSIS
        Get the default config path of the OS

        .DESCRIPTION
        Each Operating system has its predefined config path location. This function retrieve the path and return it.

        .OUTPUTS
        [string]

        .EXAMPLE
        Get-SettingPath

        Returns the path of the config directory on the running OS
        #>

    [CmdletBinding()]
    [OutputType([Object])]

    Param()

    $OS = $PSVersionTable.OS

    switch -regex ($OS)
    {
        'Darwin' { $settingPath = "$HOME/.config" }
        'Linux' { $settingPath = "$XDG_CONFIG_HOME" }
        # $Win is not a variable but a regex matching of the line starting with 'Win'
        "$Win" { $settingPath = "$env:APPDATA" }
        $null { $settingPath = "$env:APPDATA" }
    }

    $settingPath
}
