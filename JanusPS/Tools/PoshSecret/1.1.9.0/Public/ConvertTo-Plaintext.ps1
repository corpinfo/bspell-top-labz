
function ConvertTo-Plaintext
{
    <#
        .SYNOPSIS
        Converts a SecureString to a plaintext string

        .DESCRIPTION
        Converts a SecureString to a plaintext string

        .PARAMETER SecureString
        The SecureString to convert to plaintext

        .OUTPUTS
        [String]

        .EXAMPLE
        $Password = ConvertTo-SecureString "hunter2" -AsPlainText -Force
        ConvertTo-Plaintext -SecureString $Password

        Returns the plaintext string from a SecureString
        #>

    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true, Position = 0)]
        [Alias("SecurePassword")]
        [System.Security.SecureString]$SecureString
    )

    try
    {
        $UnSecurePointer = [System.Runtime.InteropServices.Marshal]::SecureStringToGlobalAllocUnicode($SecureString)
        $UnSecureString = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($UnsecurePointer)
    }
    finally
    {
        #This is important, it zeroes out the memory
        [System.Runtime.InteropServices.Marshal]::ZeroFreeGlobalAllocUnicode($UnsecurePointer)
    }

    return $UnSecureString
}
