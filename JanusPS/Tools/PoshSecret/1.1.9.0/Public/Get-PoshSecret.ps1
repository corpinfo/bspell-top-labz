function Get-PoshSecret
{
    <#
        .SYNOPSIS
        Retrieves secrets from persistent storage. Secrets can only be retrieved by the user
        account that stored them.

        .DESCRIPTION
        Secrets are encrypted with the Windows DPAPI (using PS cmdlets) and saved with metadata in +
        XML format in $POSH_SECRET_PATH.

        A combination of Resource and Username is guaranteed unique. Specifying Resource and, +
        optionally, Username will return a single object (or null if no matching object is found).
        Specifying no parameters will return all stored objects.

        If no matching object is found, will return null.

        If a matching secret was found but it was stored with an expiry time and that expiry time
        is now expired, the secret is deleted and the function returns null. A secret is
        considered expired if the expiry time is less than $MINIMUM_SECONDS_BEFORE_EXPIRY in
        the future.

        .PARAMETER Name
        This is what the secret applies to, for example, an API key. It can be used as a display
         name.

        .PARAMETER Username
        An optional username field

        .PARAMETER AsSecureString
        Return the secret as a SecureString

        .PARAMETER AsPlaintext
        Return the secret as a string

        .PARAMETER AsPSCredential
        Return the secret as a PSCredential

        .PARAMETER MinimumSecondsBeforeExpiry
        Minimum time before the password expires

        .PARAMETER EncryptionKey
        Used with Linux and MacOS as the encryption secret

        .OUTPUTS
        [object]

        .EXAMPLE
        Get-PoshSecret -Name 'MySecret'
    #>
    [CmdletBinding(DefaultParameterSetName = 'NoParameters')]
    [OutputType([PSObject[]], ParameterSetName = 'NoParameters')]
    [OutputType([PSObject], ParameterSetName = 'ReturnPSObject')]
    [OutputType([PSCredential], ParameterSetName = 'ReturnPSCredential')]
    param(
        #The name of the secret or credential to retrieve
        [Parameter(Mandatory = $true, ParameterSetName = 'ReturnPSCredential')]
        [Parameter(Mandatory = $false, ParameterSetName = 'ReturnPSObject', ValueFromPipelineByPropertyName = $true)]
        [Parameter(Mandatory = $true, ParameterSetName = 'Encryption')]
        [string]$Name,

        #Username, to select between secrets or credentials with the same name
        [Parameter(Mandatory = $false, ParameterSetName = 'ReturnPSCredential')]
        [Parameter(Mandatory = $false, ParameterSetName = 'ReturnPSObject', ValueFromPipelineByPropertyName = $true)]
        [Parameter(Mandatory = $false, ParameterSetName = 'Encryption')]
        [string]$Username,

        #Whether to return a PSCredential object (default is to return a PoshSecret object)
        [Parameter(Mandatory = $true, ParameterSetName = 'ReturnPSCredential')]
        [Parameter(Mandatory = $false, ParameterSetName = 'Encryption')]
        [switch]$AsPSCredential,

        #Whether to decrypt the secret or password (default is not to return secrets in plaintext)
        [Parameter(Mandatory = $false, ParameterSetName = 'ReturnPSObject')]
        [Parameter(Mandatory = $false   , ParameterSetName = 'Encryption')]
        [switch]$AsPlaintext,

        #Minimum remaining validity in seconds for any secret or credential to be returned. Expired secrets are purged. Only affects secrets stored with the -Expiry parameter
        [Parameter()]
        [uint16]$MinimumSecondsBeforeExpiry = $MINIMUM_SECONDS_BEFORE_EXPIRY,

        # Encryption key used to secure the string
        [Parameter(Mandatory = $true, ParameterSetName = 'Encryption')]
        [string]$EncryptionKey
    )

    begin
    {
        Write-Verbose "[$(Get-Date)] List of Parameters :: $($PSBoundParameters.GetEnumerator() | Out-String)"
        Write-Verbose "[$(Get-Date)] Begin :: $($MyInvocation.MyCommand)"
    }

    process
    {
        try
        {
            if ($PSCmdlet.ParameterSetName -eq 'NoParameters')
            {
                $SecretArray = Get-ChildItem $POSH_SECRET_PATH | ForEach-Object {
                    $Name, $Username = [System.Uri]::UnescapeDataString($_.Name).Split([char]31)
                    $PoshSecret = New-Object PSObject -Property @{Name = $Name; Username = $Username}
                    Add-PoshSecretDefaultMembers -PoshSecret $PoshSecret
                    return $PoshSecret
                }
                return $SecretArray
            }

            #Load the secret from disk
            $Filename = [System.Uri]::EscapeDataString(($Name, $Username) -join [char]31)

            $SecretPath = Join-Path -Path $POSH_SECRET_PATH -ChildPath $Filename

            #PoshSecret = Import-CliXml (Join-Path $POSH_SECRET_PATH $Filename)
            if (Test-Path -Path $SecretPath)
            {
                if ($PSCmdlet.ParameterSetName -eq 'Encryption')
                {
                    $PoshSecret = Get-Content -Path $SecretPath -Raw -Encoding utf8 |
                        ConvertFrom-Json
                }
                else
                {
                    $PoshSecret = Import-CliXml $SecretPath
                }
            }
            else
            {
                $PoshSecret = $null
            }

            #Handle expiry
            if ($PoshSecret.ExpiryTime -and ($PoshSecret.ExpiryTime -lt (Get-Date).AddSeconds($MinimumSecondsBeforeExpiry).ToString("s")))
            {
                Remove-PoshSecret -Name $Name -Username $Username
                return $null
            }

            if (($PSCmdlet.ParameterSetName -eq 'Encryption') -and ($null -ne $PoshSecret))
            {
                #Deserialize SecureString property
                $PoshSecret.SecurePassword = ConvertTo-SecureString $PoshSecret.SerializedSecurePassword -Key $EncryptionKey
            }
            else
            {
                #Deserialize SecureString property
                If ($null -ne $PoshSecret)
                {
                    $PoshSecret.SecurePassword = ConvertTo-SecureString $PoshSecret.SerializedSecurePassword
                }
            }

            if ($AsPlaintext -and  ($null -ne $PoshSecret))
            {
                $PoshSecret | Add-Member -Name "Password" -MemberType NoteProperty -Value (
                    ConvertTo-Plaintext $PoshSecret.SecurePassword
                )
            }

            if ($PSCmdlet.ParameterSetName -eq 'ReturnPSCredential')
            {
                if ($PoshSecret.Username -and $PoshSecret.SecurePassword)
                {
                    return New-Object PSCredential ($PoshSecret.Username, $PoshSecret.SecurePassword)
                }
            }

            if ($null -ne $PoshSecret)
            {
                Add-PoshSecretDefaultMembers -PoshSecret $PoshSecret
            }

            return $PoshSecret
        }
        catch
        {
            Write-Verbose "[$(Get-Date)] Info  :: $($MyInvocation.MyCommand)"
            Write-Verbose "[$(Get-Date)] Error :: $_.Exception.Message"

            throw
        }
    }
    end
    {
        Write-Verbose "[$(Get-Date)] End   :: $($MyInvocation.MyCommand)"
    }
}
