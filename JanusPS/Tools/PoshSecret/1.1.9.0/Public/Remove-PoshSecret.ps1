function Remove-PoshSecret
{
    <#
        .SYNOPSIS
        Removes secrets from persistent storage.

        .DESCRIPTION
        Secrets are encrypted with the Windows DPAPI (using PS cmdlets) and saved with metadata
        in XML format in $POSH_SECRET_PATH.

        This function will remove secrets from storage.

        .PARAMETER Name
        This is what the secret applies to, for example, an API key. It can be used as a
        display name.

        .PARAMETER Username
        The secret to delete is identified by the combination of
        name and username, which is guaranteed unique.

        .OUTPUTS
        [void]

        .EXAMPLE
        Remove-PoshSecret -Name 'MySecret' -Username rack1234

        Removes secret called 'MySecret' stored for username 'rack1234'
    #>

    [CmdletBinding()]
    [OutputType([void])]

    Param(
        #The name of the secret or credential to remove
        [Parameter(Mandatory = $true)]
        [string]$Name,

        #The username of the secret or credential to remove
        [Parameter(Mandatory = $true)]
        [string]$Username
    )

    $Filename = [System.Uri]::EscapeDataString(($Name, $Username) -join [char]31)

    try
    {
        Remove-Item (Join-Path $POSH_SECRET_PATH $Filename) -Force
    }
    catch [System.IO.FileNotFoundException] {return $null}
}
