function Add-PoshSecret
{
    <#
        .Synopsis
        Stores secrets securely and persistently. Secrets can only be retrieved by the user account
        that stored them.

        .Description
        Secrets are encrypted with the Windows DPAPI (using PS cmdlets)
        and saved with metadata in XML format in $POSH_SECRET_PATH.

        A combination of Name and Username is guaranteed unique - storing a new secret with an
         existing combination of name and username will overwrite the previous secret.

        .PARAMETER Name
        A name for the credential

        .PARAMETER Property
        Extra information to be stored along with the secret, as a hashtable.

        .PARAMETER Username
        An optional username field

        .PARAMETER Secret
        The password or secret to store, as a SecureString

        .PARAMETER PlaintextSecret
        The password or secret to store, as a string

        .PARAMETER Expiry
        An optional expiry time for the secret. If this is specified at storage time then,+
        at retrieval time, the secret will be deleted and not returned

        .PARAMETER Password
        Plain text password

        .PARAMETER SecurePassword
        Encrypted password

        .PARAMETER EncryptionKey
        Used with Linux and MacOS as the encryption secret

        .OUTPUTS
        []

        .EXAMPLE

        Add-PoshSecret -Name "PentagonServer" -Username "Bob" -PlaintextPassword "hunter2"
        Adds a straightforward credential

        $Token = Get-SomeStructuredToken
        Add-PoshSecret -Name "CloudAuthAPI" -PlaintextPassword $Token.AuthToken -Property @{Url="https://auth.provider.com/"} -Expiry $Token.Expiry
        Stores an API token with a "URL" property which can be accessed on the retrieved object

        .EXAMPLE

        Add-PoshSecret -Name "CatFactsAPI" -PlaintextPassword "3aa75b5e46b94a5aa77b0c4b172a4eb4" -Property @{Tags=@("Cat", "Facts", "Interesting")} -Expiry (Get-Date).AddHours(5)
        Adds a credential with a secret component but no username. Metadata tags are stored with the credential and can be accessed on the retrieved object. The credential will be deleted after 5 hours.
    #>
    [CmdletBinding(DefaultParameterSetName = 'Plaintext')]
    [OutputType([void])]

    #this is by design
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingPlainTextForPassword", "Password")]
    Param
    (
        #The name of the secret or credential to store
        [Parameter(Mandatory = $true, ParameterSetName = 'Plaintext', HelpMessage = "The password to store")]

        [ValidateNotNullOrEmpty()]
        [string]$Name,

        # Username to store with the secret or credential
        [Parameter(Mandatory = $true, ParameterSetName = 'Plaintext', HelpMessage = "The password to store")]
        [string]$Username,

        #Additional metadata to store with the secret or credential
        [Parameter()]
        [System.Collections.IDictionary]$Property,

        #The secret or password to store, as a secure string
        [Parameter(Mandatory = $true, ParameterSetName = 'SecureString')]
        [SecureString]$SecurePassword,

        #The secret or password to store, as a plaintext string
        [Parameter(Mandatory = $true, ParameterSetName = 'Plaintext', HelpMessage = "The password to store")]
        [Alias('PlaintextPassword')]
        [string]$Password,

        #The expiry time of the secret or credential
        [Parameter(Mandatory = $false)]
        [datetime]$Expiry,

        # Encryption key used to secure the string
        [Parameter(Mandatory = $false)]
        [string]$EncryptionKey
    )

    begin
    {
        Write-Verbose "[$(Get-Date)] Begin :: $($MyInvocation.MyCommand)"
    }

    process
    {
        try
        {

            $StandardProperties = @{
                Name                     = $Name;
                Username                 = $Username;
                SerializedSecurePassword = '';
                SecurePassword           = $null;
                ExpiryTime               = '';
                StorageTime              = (Get-Date -Format s);
                Version                  = (Get-Module PoshSecret).Version
            }
            if ($Property) {$StandardProperties += $Property}

            $PoshSecret = New-Object PSObject -Property $StandardProperties


            if ($Expiry)
            {
                if ($Expiry -lt (Get-Date)) {Write-Verbose "Expiry time is in the past; discarding"; return $null}
                $PoshSecret.ExpiryTime = $Expiry.ToString("s")
            }
            if ($PSCmdlet.ParameterSetName -eq 'Plaintext')
            {
                $SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
            }
            $PoshSecret.SecurePassword = $SecurePassword
            #$PoshSecret.SerializedSecurePassword = ConvertFrom-SecureStringLegacy $SecurePassword
            # NOTE: Usage of the encryption key for powershell 6 compatibility.

            if ($(Get-OsLabel) -notMatch 'Windows')
            {
                if ($EncryptionKey)
                {
                    $PoshSecret.SerializedSecurePassword = ConvertFrom-SecureString $SecurePassword -Key $EncryptionKey
                    $Filename = [System.Uri]::EscapeDataString(($Name, $Username) -join [char]31)

                    $PoshSecret | ConvertTo-Json -Depth 20 | Out-File -FilePath $(Join-Path -Path $POSH_SECRET_PATH -ChildPath $Filename) -Force
                }
                else
                {
                    throw 'Usage of the parameter -EncryptionKey is required by powershell 6 on macOS and Linux'
                }
            }
            else
            {
                $PoshSecret.SerializedSecurePassword = ConvertFrom-SecureString $SecurePassword

                $Filename = [System.Uri]::EscapeDataString(($Name, $Username) -join [char]31)

                $PoshSecret | Export-CliXml (Join-Path $POSH_SECRET_PATH $Filename) -Depth 8 -Force
            }
        }
        catch
        {
            Write-Verbose "[$(Get-Date)] Info  :: $($MyInvocation.MyCommand)"
            Write-Verbose "[$(Get-Date)] Error :: $_.Exception.Message"

            throw
        }
    }

    end
    {
        Write-Verbose "[$(Get-Date)] End   :: $($MyInvocation.MyCommand)"
    }
}
