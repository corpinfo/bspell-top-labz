﻿# PoshSecret Release History

## 1.1.9.0

- Get-PoshSecret now supports argument completion for -Name and -UserName.

## 1.1.8.0

- Update jenkinsfile to 1.6.0.0

## 1.1.7.0

- Enabled support for module optimisation

## 1.1.6.0

- Updating Jenkinsfile to publish module to Artifactory

## 1.1.5.2

- Bugfix: Add null check when using -AsPSCredential in Get-PoshSecret

## 1.1.5.1

- Bugfix: Updated Remove-PoshSecret to require username in the input

## 1.1.5.0

- Moved `.\README.md` to `.\docs\README.md` and replaced `.\docs\index.md`
- Updated README.md (new pages index page) with Installation, Help and Change Log sections

## 1.1.4.1

- Removed input params from Verbose stream in Add-PoshSecret

## 1.1.4.0

- Adding updated PSSA exception and jenkinsfile

## 1.1.3.0

- Adding updated PSSA exception and VsCode function

## 1.1.2.2

- Fixed bug where null cannot be passed to other functions

## 1.1.2.1

- Bug with ReadOnly param in PSM1 stopped module from loading

## 1.1.2.0

- Bringing module to PSSA compliance
