#Load  public and private function definition files.
Get-ChildItem $PSScriptRoot\Private -Filter '*.ps1' | Foreach-Object {. $_.FullName}
Get-ChildItem $PSScriptRoot\Public  -Filter '*.ps1' | Foreach-Object {. $_.FullName}

$VarParams = @{
    'Name'        = 'POSH_SECRET_PATH'
    'Value'       = (Join-Path -Path "$(Get-SettingPath)" -ChildPath 'PoshSecret')
    'Description' = "Folder path where credentials are saved"
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'ReadOnly'
}
Set-Variable @VarParams

$VarParams = @{
    'Name'        = 'MINIMUM_SECONDS_BEFORE_EXPIRY'
    'Value'       = 600
    'Description' = "A token or password is discarded if it has less than this long before it expires"
    'Scope'       = 'Script'
    'Force'       = $True
    'Option'      = 'ReadOnly'
}
Set-Variable @VarParams

if (-not (Test-Path $POSH_SECRET_PATH -PathType Container))
{
    [void](New-Item $POSH_SECRET_PATH -ItemType Directory)
}

$ArgumentCompleterSplat = @{
    CommandName   = 'Get-PoshSecret'
    ParameterName = 'Name'
    ScriptBlock   = {
        param
        (
            $commandName,
            $parameterName,
            $wordToComplete,
            $commandAst,
            $fakeBoundParameters
        )

        $Secrets = Get-PoshSecret
        if ($fakeBoundParameters.ContainsKey('UserName'))
        {
            $Secrets = $Secrets | Where-Object {$_.UserName -eq $fakeBoundParameters.UserName}
        }
        $Secrets | Select-Object -ExpandProperty Name | Where-Object {$_ -like "$wordToComplete*"}
    }
}
Register-ArgumentCompleter @ArgumentCompleterSplat

$ArgumentCompleterSplat = @{
    CommandName   = 'Get-PoshSecret'
    ParameterName = 'UserName'
    ScriptBlock   = {
        param
        (
            $commandName,
            $parameterName,
            $wordToComplete,
            $commandAst,
            $fakeBoundParameters
        )

        $Secrets = Get-PoshSecret
        if ($fakeBoundParameters.ContainsKey('Name'))
        {
            $Secrets = $Secrets | Where-Object {$_.Name -eq $fakeBoundParameters.Name}
        }
        $Secrets | Select-Object -ExpandProperty Username | Where-Object {$_ -like "$wordToComplete*"}
    }
}
Register-ArgumentCompleter @ArgumentCompleterSplat
