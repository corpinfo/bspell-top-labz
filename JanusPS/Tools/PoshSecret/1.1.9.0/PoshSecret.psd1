﻿@{
    Description       = 'Secure persistent storage and retrieval of credentials'
    ModuleToProcess   = 'PoshSecret.psm1'
    ModuleVersion     = '1.1.9.0'
    GUID              = 'c658de48-0f7d-481c-bc91-5bf4054d445a'
    Author            = 'Freddie Sackur'
    CompanyName       = 'Rackspace'
    Copyright         = '2017'
    PowerShellVersion = '3.0'
    RequiredModules   = @()
    FunctionsToExport = @(
        'Add-PoshSecret',
        'Get-PoshSecret',
        'Remove-PoshSecret',
        'ConvertTo-Plaintext'
    )
    PrivateData       = @{
        PSData = @{
            Tags      = @(
                'secret',
                'password'
            )
            OSVersion = '6.0'
        }
    }
}
