# README #

This Module is used for interacting with the Janus API.

## How do I get set up? ##

1. Clone Repository

    ``` powershell
    git clone {REPOURL}
    ```

2. Install Dependencies:

    ``` powershell
    cd .{REPOSITORY_ROOT}\tools

    .\setup.PS1
    ```

    *This will setup dependent modules **Tokx** and **PoshSecret***

3. Save Module to your Modules Directory

    From the Root of the Project Folder:

    ```powershell
    $source = $pwd.path

    # Module Path is dependant on your powershell version.
    $MyDocPath  = [Environment]::GetFolderPath("MyDocuments")
    if ( $PSVersionTable.PSVersion -gt [version]'6.0.0' ) {
        $UserModulePath = Join-Path $MyDocPath 'PowerShell\Modules'
    } else {
        $UserModulePath = Join-Path $MyDocPath 'WindowsPowerShell\Modules'
    }

    Copy-Item -Path $source -Destination $UserModulePath -Force -Recurse

    ```

## Usage ##

- Set Default Account
  
    **rackspaceCloudAccountnumber** - This is also known as the *'Tenant Account number'*, *'TID'*, or just simply the *'Rackspace Account'*

    **AWSAccountNumber** - As the parameter name states, this is your AWS Account ID.

    These Parameters can be located in the URI of the Janus Console.

    ![JanusConsole](etc/JanusConsole.png)

    ```powershell
    # Set Default Account Information
    Set-JanusConfig -AWSID 757506681371 -RXTID 1337809
    Set-JanusDefaultAccount -AWSID 757506681371 -RXTID 1337809

    # view default account information
    Get-JanusConfig

    # View Running Config
    Get-JanusConfig -RunningConfig
    ```

- Get Account Details

    ```powershell
    #Gets the all accounts for the default configured Rackspace Account.
    # If no account exists, returns 404
    Get-JanusAccount

    # Gets the Account with ID. You must have the default rackspace account configured.
    Get-JanusAccount -AWSAccountNumber 757506681371

    #Gets account, specifying both the AWS ID and the RXT ID. This is the most precise way to know that you retrieve the correct account.
    Get-JanusAccount -AWSAccountNumber 757506681371 -rackspaceCloudAccountNumber 1337809

    #You can use Parameter Aliases
    Get-JanusAccount -AWSID 757506681371 -RXTID  1337809

    #or function alias
    Get-jAccount -AWSID 757506681371 -RXTID  1337809
    ```

- Get Credentials for an account

    ```powershell
    #Get raw credentials for the configured default account
    Get-JanusAccountCredential

    #Using Alias
    Get-jCred

    #Format for Linux and copy to clipboard
    get-jcred -Linux | Set-Clipboard

    #format for ~/.aws/credentials file and copy to clipboard
    get-jcred -CredentialFile | Set-Clipboard

    

    # Get Raw Credentials returned to your terminal for a specified account
    Get-JanusAccount -AWSID 757506681371 -RXTID  1337809 | Get-JanusAccountCredential

    #Get the account set as default and pip it to get the credentials
    Get-jCred
    # This is 'shorthand'. the full typed out version would be
    # Get-JanusAccount -DefaultRXTID -DefaultAWSID | Get-JanusAccountCredential

    #Set Credentials in your powershell profile for use in your current session.
    Get-jAccount -da -dr | Get-jCred | Set-JanusAWSProfile

    #Set Credentials in the configured powershell profile for reuse in other sessions
    Get-jAccount -da -dr | Get-jCred | Set-JanusAWSProfile -Persistent
    ```
  
- Open the AWS Console for an account:

    ```powershell
    # Get a specific account and open the AWS Console
    Get-jAccount -AWSID 757506681371 -RXTID 1337809 | Start-JanusAWSConsole

    # using aliases, open the console for the default account
    Get-jAccount -da -dr | jConsole
    ```

## Contribution guidelines ##

TBD

### Who do I talk to? ###

- Repo owner or admin
- Other community or team contact
