Function Clear-JanusConfig {
    [CmdletBinding(SupportsShouldProcess,ConfirmImpact='high')]
    [Alias('Clear-jconf')]
    param (
        # Specify Configuration to clear.
        [Parameter()]
        [ValidateSet('RunningConfig','DefaultConfig')]
        [string[]]
        $Configuration = 'RunningConfig'
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name

        Write-Verbose ( '{0}: Beginning Function' -f $function )
    }
    Process {
        if ( $PSCmdlet.ShouldProcess("Janus $Configuration Configuration'", "Clear") ) {
            switch ($Configuration) {
                RunningConfig { 
                    Remove-Variable JanusConfig -Scope Global -Verbose -ErrorAction SilentlyContinue
                    $env:JanusDefaultBrowser = 'microsoft-edge'
                    $env:JanusDefaultAWSAccount = $null
                    $env:JanusDefaultRackspaceCloudAccount = $null
                    Get-JanusConfig -RunningConfig
                 }
                 DefaultConfig {
                     Set-JanusConfig -AWSAccountNumber '' -rackspaceCloudAccountNumber ''
                 }
            }
        }
    }
}
