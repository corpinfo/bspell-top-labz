Function Start-JanusAWSConsole {
    [CmdletBinding()]
    [Alias('jConsole')]
    param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName)]
        [Alias('AWSID')]
        [string]
        $AWSAccountNumber = $env:JanusDefaultAWSAccount,
  
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [Alias('RXTID')]
        [string]
        $rackspaceCloudAccountNumber = $env:JanusDefaultRackspaceCloudAccount
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose ( '{0}: Beginning Function' -f $function )
    }
    Process {
        $connectID = $AWSAccountNumber
        $connectTenant = $rackspaceCloudAccountNumber
        $ConsoleUri = 'https://manage.rackspace.com/racker/rackspaceAccounts/{0}/awsAccounts/{1}/consoleSignin' -f $connectTenant, $connectID

        switch ( $env:JanusDefaultBrowser ) {
            microsoft-edge {  
                Start-Process microsoft-edge:$ConsoleUri
            }
            Default {
                Start-Process $env:JanusDefaultBrowser $ConsoleUri
            }
        }
    }
}