function Set-JanusConfig{
    [CmdletBinding()]
    [Alias('Set-jConf')]
    param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName)]
        [Alias('AWSID')]
        [string]
        $AWSAccountNumber,
  
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [Alias('RXTID')]
        [string]
        $rackspaceCloudAccountNumber,

        [ValidateSet('microsoft-edge','chrome','firefox')]
        [string]
        $Browser,

        [ValidateSet(
            'AllUsersAllHosts',
            'AllUsersCurrentHost',
            'CurrentUserAllHosts',
            'CurrentUserCurrentHost')]
        [string]
        $PowershellProfile = 'CurrentUserAllHosts',

        [string]
        $APIEndpoint = 'https://accounts.api.manage.rackspace.com',

        # Config the account set 
        [bool]
        $defaultAccountEnabled,

        [switch]
        $Initialize
      )
      Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name

        Write-Verbose ( '{0}: Beginning Function' -f $function )
        Write-Verbose ( '{0}: Parameters:`n {1}' -f $function, ($PSBoundParameters|ConvertTo-Json))
    }
    Process {
        $DefaultConfig = @{
            APIEndpoint = $APIEndpoint
            AWSAccountNumber = $null
            rackspaceCloudAccountNumber = $null
            Browser = $null
            PowershellProfile = $PowershellProfile
            defaultAccountEnabled = $false
        }
        if ( $Initialize ) {
            Write-Verbose ( '{0}: Initializing JanusConfig.json..' -f $function)
            if ( -not ( Test-Path $env:JanusDefaultConfig ) ) {
                $configOutput = $DefaultConfig | ConvertTo-Json
                Set-Content $env:JanusDefaultConfig -Value $configOutput -Force
                Write-Verbose ( '{0}: Configuration Initilized.' -f $function )
            } else {
                Write-Verbose ( '{0}: JanusConfig.json already Intialized.' -f $function )
            }
            return
        }
        if ( $PSBoundParameters.ContainsKey('AWSAccountNumber') ) {
            $DefaultConfig['AWSAccountNumber'] = $AWSAccountNumber
        } else {
            $DefaultConfig['AWSAccountNumber'] = $env:JanusDefaultAWSAccount
        }
        if ( $PSBoundParameters.ContainsKey('rackspaceCloudAccountNumber') ) {
            $DefaultConfig['rackspaceCloudAccountNumber'] = $rackspaceCloudAccountNumber
        } else {
            $DefaultConfig['rackspaceCloudAccountNumber'] = $env:JanusDefaultRackspaceCloudAccount
        }
        if ( $PSBoundParameters.ContainsKey('Browser') ) {
            $DefaultConfig['Browser'] = $Browser
        } else {
            $DefaultConfig['Browser'] = $env:JanusDefaultBrowser
        }
        if ( $PSBoundParameters.ContainsKey( 'defaultAccountEnabled' ) ) {
            $DefaultConfig['defaultAccountEnabled'] = $defaultAccountEnabled
        } else {
            $DefaultConfig['defaultAccountEnabled'] = $env:JanusEnableDefaultAccount
        }

        $configOutput = $DefaultConfig | ConvertTo-Json
        Write-Verbose 'Setting Default Config:'
        Write-Verbose $configOutput

        Set-Content $env:JanusDefaultConfig -Value $configOutput -Force
        Write-Verbose 'Default Config updated.'
        Write-Verbose ( '{0}: Reimporting Module: {1}' -f $function, $PSCmdlet.MyInvocation.MyCommand.Source )
        Import-Module $PSCmdlet.MyInvocation.MyCommand.Source -Force
        Get-JanusConfig
    }
}
