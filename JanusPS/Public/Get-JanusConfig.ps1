function Get-JanusConfig {
    [CmdletBinding()]
    [Alias('Get-jConf')]
    param (
        [Parameter()]
        [switch]
        $RunningConfig
    )
    Process {
        if ( $RunningConfig ) {
            Get-ChildItem env:\Janus*
        } else {
            Get-Content $env:JanusDefaultConfig | ConvertFrom-Json
        }
    }
}
