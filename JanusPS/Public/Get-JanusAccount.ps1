function Get-JanusAccount {
    [CmdletBinding()]
    [Alias('Get-jAccount')]
    param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName)]
        [Alias('AWSID')]
        [string]
        $AWSAccountNumber,

        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [Alias('RXTID')]
        [ValidateNotNullOrEmpty()]
        [string]
        $rackspaceCloudAccountNumber,

        # Name or Partial Name of the account. Can be a regular expression.
        [Parameter()]
        [string]
        $Name
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose ( '{0}: Beginning Function' -f $function )
        Write-Verbose ( '{0}: Parameters:`n {1}' -f $function, ($PSBoundParameters|ConvertTo-Json))

        $private:splat = (Get-JanusRestParameters).clone()
        
    }
    Process {
        if ( [string]::IsNullOrEmpty( $rackspaceCloudAccountNumber ) ) {
            throw 'No Rackspace Account Specified.'
        }
        $private:splat.Headers.'X-Tenant-Id' = $rackspaceCloudAccountNumber

        if ( $AWSAccountNumber ) {
            $private:splat.Uri = -join ( $private:splat.Uri, "/v0/awsAccounts/$AWSAccountNumber" )
            $expand = 'awsAccount'
        } else {
            $private:splat.Uri = -join ( $private:splat.Uri, "/v0/awsAccounts" )
            $expand = 'awsAccounts'
        }
        
        try {
            Write-Verbose ( '{0}: {1}' -f $function, ($private:splat|ConvertTo-Json))
            $accounts = Invoke-RestMethod @private:splat

            if ( $accounts ) {
                $accounts = $accounts.$expand
                if ( $Name ) {
                    $accounts = $accounts | Where-Object { $_.name -match $Name }
                }
                Write-Output $accounts
            }
        } catch {
            Write-Warning $_.Exception.Message
        }
    }
}
