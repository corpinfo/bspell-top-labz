function Set-JanusDefaultAccount {
    [CmdletBinding(SupportsShouldProcess, ConfirmImpact = 'High')]
    [Alias('Set-jAccount')]
    param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName, Mandatory)]
        [Alias('AWSID')]
        [ValidateNotNullOrEmpty()]
        [string]
        $AWSAccountNumber,

        [Parameter(Position = 1, ValueFromPipelineByPropertyName, Mandatory)]
        [Alias('RXTID')]
        [ValidateNotNullOrEmpty()]
        [string]
        $rackspaceCloudAccountNumber,
        
        [ValidatePattern("([TtRrUuEe|FfAaLlSsEe|0-1])")]
        [string]
        $SuppressWarnings
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose "[$function] Beginning Function"
        Write-Verbose ( '[{0}] Parameters: {1}' -f $function, ($PSBoundParameters|ConvertTo-Json))
        
        function Add-JanusDefaultParameter {
            param(
                [string]
                $Command,

                [string]
                $Parameter,

                [string]
                $Value
            )
            Process {
                Write-Verbose "[$($MyInvocation.MyCommand.Name)] Setting [$command : $parameter] = $value"
                $PSDefaultParameterValues["${command}:${parameter}"] = $Value
                $global:PSDefaultParameterValues["${command}:${parameter}"] = $Value
            }
        }

        $warn = switch -regex ($SuppressWarnings) {
            "(TtRrUuEe|1)" { $false }
            Default { $true }
        }
    }
    Process {
        if ( $warn ) {
            if ( 
                $Global:PSDefaultParameterValues['Set-JanusDefaultAccount:SuppressWarnings'] -and 
                $PSDefaultParameterValues['Set-JanusDefaultAccount:SuppressWarnings']
                ) {
            } else {
                Add-JanusDefaultParameter -Command $function -Parameter 'SuppressWarnings' -Value 1
            }   
        } else {
            Write-Warning @'
            This Operation will set the default account for all functions.
            Do so means that, if you run a command such as 'Get-JanusAccount' with no parameters,
            the configured default account will load. This can cause unexpected problems with searching for
            accounts other than the default account. Any such problems should be reported on the Project.
            To suppress this message, run:
            Set-JanusDefaultAccount -SuppressWarnings $true
'@
        }
        if ( $PSCmdlet.ShouldProcess("RXTID:$rackspaceCloudAccountNumber AWSID:$AWSAccountNumber", 'Set Default' ) -or $SuppressWarnings) {
            $moduleCommands = Get-Command -Module JanusPS | 
                Where-Object { 
                    $_.Parameters.Keys -contains 'AWSAccountNumber' -and 
                    $_.Parameters.Keys -contains 'rackspaceCloudAccountNumber'
                }
            foreach ($command in $moduleCommands) {
                if ( $command.Parameters.Keys -contains 'AWSAccountNumber' ) {
                    Add-JanusDefaultParameter -Command $command -Parameter 'AWSAccountNumber' -Value $AWSAccountNumber
                }
                if ( $command.Parameters.Keys -contains 'rackspaceCloudAccountNumber' ) {
                    Add-JanusDefaultParameter -Command $command -Parameter 'rackspaceCloudAccountNumber' -Value $rackspaceCloudAccountNumber
                }
            }
        }
    }
}
