function Get-JanusAccountCredential {
    [CmdletBinding(DefaultParameterSetName = 'none')]
    [Alias('Get-jCred')]
    param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName)]
        [Alias('AWSID')]
        [string]
        $AWSAccountNumber = $env:JanusDefaultAWSAccount,
  
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [Alias('RXTID')]
        [string]
        $rackspaceCloudAccountNumber = $env:JanusDefaultRackspaceCloudAccount,

        # Format Output to load into a linux bash shell
        [Parameter(ParameterSetName='Linux')]
        [switch]
        $Linux,

        # Format output to load into an aws 'credentials' file
        [Parameter(ParameterSetName='CredentialFile')]
        [switch]
        $CredentialFile,

        [switch]
        $Flush
    )
    Begin {
        $function = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Verbose ( '[{0}] Beginning Function' -f $function )
        Write-Verbose ( '[{0}] Parameters: {1}' -f $function, ($PSBoundParameters|ConvertTo-Json))

        $private:splat = (Get-JanusRestParameters).clone()
        $private:splat.Headers.'X-Tenant-Id' = $rackspaceCloudAccountNumber
        $private:splat.Uri = -join ( $private:splat.Uri, "/v0/awsAccounts/$AWSAccountNumber/credentials" )
        $private:splat.Method = 'Post'

    }
    Process {
        #Check for existing Session
        try {
            $originalEA = $ErrorActionPreference
            $ErrorActionPreference = 'stop'
            $secret = Get-PoshSecret -Name AWSCredential -Username AWSCredential -MinimumSecondsBeforeExpiry 1200
            if ( $secret -and (-not $Flush.IsPresent ) ) {
                $credentials = $secret.session
                Write-Verbose ('[{0}] Using Cached Credentials.')
            } else {
                Write-Error 'No Cached Credentials Found'
            }
        } catch {
            Write-Verbose ('[{0}] {1}' -f $function, $_.Exception.Message )
            try {
                Write-Verbose ( '[{0}] REst Parameters: {1}' -f $function, ($private:splat|ConvertTo-Json))
                $credentials = Invoke-RestMethod @private:splat
            } catch {
                if ( $_.Exception.response.StatusCode -eq 'Unauthorized' ) {
                    try {
                        Get-RackerToken -AsXAuthToken -Flush | Out-Null
                        $private:splat = Get-JanusRestParameters
                        $credentials = Invoke-RestMethod @private:splat
                    }
                    catch {
                        Write-Warning $_.Exception.Message
                        return $null
                    }
                }
            }
            Try {
                Write-Verbose ( '[{0}] Saving session as PoshSecret' -f $function )
                $poshSecretSplat = @{
                    Name = 'AWSCredential'
                    PlaintextPassword = 'AWSCredential'
                    UserName = 'AWSCredential'
                    Property = @{ session = $credentials }
                    Expiry = $credentials.credential.expiration.ToLocalTime()
                    ErrorAction = 'SilentlyContinue'
                }
                Add-PoshSecret @poshSecretSplat
            } catch {
                Write-Warning $_.Exception.Message
                return $null
            }

        }
        $ErrorActionPreference = $originalEA

        if ( $credentials ) {
            if ( $Linux ) {
                [string[]]$output = @()
                $output += 'export AWS_ACCESS_KEY_ID="{0}"' -F $credentials.credential.accessKeyId
                $output += 'export AWS_SECRET_ACCESS_KEY="{0}"' -F $credentials.credential.secretAccessKey
                $output += 'export AWS_SESSION_TOKEN="{0}"' -F $credentials.credential.sessionToken
                $output += 'export AWS_SESSION_EXPIRATION="{0}"' -F $credentials.credential.expiration.ToLocalTime().tostring('f')
            } elseif ( $CredentialFile ) {
                [string[]]$output = @()
                $output += '[default]'
                $output += 'aws_access_key_id={0}' -F $credentials.credential.accessKeyId
                $output += 'aws_secret_access_key={0}' -F $credentials.credential.secretAccessKey
                $output += 'aws_session_token={0}' -F $credentials.credential.sessionToken
                $output += '; Credentials Expiration: {0}' -F $credentials.credential.expiration.ToLocalTime().tostring('f')
            } else {
                $output = $credentials.Credential
            }

            $output.psobject.Typenames.insert(0, 'Janus.Credential')
            Write-Output $output
        }
    }
}
