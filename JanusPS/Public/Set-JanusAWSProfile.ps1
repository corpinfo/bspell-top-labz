Function Set-JanusAWSProfile {
    [CmdletBinding(DefaultParameterSetName = 'none')]
    [Alias('Set-jProfile')]
    param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName)]
        [Alias('AWSID')]
        [string]
        $AWSAccountNumber,
  
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [Alias('RXTID')]
        [string]
        $rackspaceCloudAccountNumber,

        [switch]
        $Persistent,

        [switch]
        $Quiet,

        # Parameter help description
        [Parameter(ValueFromPipeline, ParameterSetName = 'Credential')]
        [System.Management.Automation.PSTypeName('Janus.Credential')]
        $InputObject
    )
    Process {
        if ( $PSCmdlet.ParameterSetName -eq 'Credential' ) {
            $env:AWS_ACCESS_KEY_ID = $InputObject.accessKeyId
            $env:AWS_SECRET_ACCESS_KEY = $InputObject.secretAccessKey
            $env:AWS_SESSION_TOKEN = $InputObject.sessionToken
            $env:AWS_SESSION_EXPIRATION = $InputObject.expiration.toLocalTime().tostring()
        } else {
            try {
                $credential = Get-JanusAccountCredential -AWSID $AWSAccountNumber -RXTID $rackspaceCloudAccountNumber
                $env:AWS_ACCESS_KEY_ID = $credential.accessKeyId
                $env:AWS_SECRET_ACCESS_KEY = $credential.secretAccessKey
                $env:AWS_SESSION_TOKEN = $credential.sessionToken
                $env:AWS_SESSION_EXPIRATION = $credential.expiration.toLocalTime().tostring()  
            } catch {
                Write-Warning $_.Exception.Message
                return
            }
        } #end Else 
        Set-AWSCredentials -AccessKey $env:AWS_ACCESS_KEY_ID  -SecretKey $env:AWS_SECRET_ACCESS_KEY -SessionToken $env:AWS_SESSION_TOKEN -Scope Global

        if ( $Persistent ) {
            $output = @"
            `$env:AWS_ACCESS_KEY_ID = '$env:AWS_ACCESS_KEY_ID'
            `$env:AWS_SECRET_ACCESS_KEY = '$env:AWS_SECRET_ACCESS_KEY'
            `$env:AWS_SESSION_TOKEN = '$env:AWS_SESSION_TOKEN'
            Set-AWSCredential -AccessKey `$env:AWS_ACCESS_KEY_ID -SecretKey `$env:AWS_SECRET_ACCESS_KEY -SessionToken `$env:AWS_SESSION_TOKEN
            #Expiration: $env:AWS_SESSION_EXPIRATION
"@
            Set-Content -Path $profile.$env:JanusDefaultPSProfile.ToString() -Value $output -Force
        }
        
        if ( ! $quiet ) {
            Write-Host -f green ( 'Credentials Updated. Expiration: {0}' -f $env:AWS_SESSION_EXPIRATION)
        }
    }
}
