#Load Functions
$PublicFunctions = @( Get-ChildItem -Path "$PSScriptRoot/Public/*.ps1" -ErrorAction SilentlyContinue )
$PrivateFunctions = @( Get-ChildItem -Path "$PSScriptRoot/Private/*.ps1" -ErrorAction SilentlyContinue )

# Dot Source 'em
foreach ($file in @($PublicFunctions + $PrivateFunctions)) {
    try {
        . $file.FullName
    }
    catch {
        $exception = ([System.ArgumentException]"Function not found")
        $errorId = "Load.Function"
        $errorCategory = 'ObjectNotFound'
        $errorTarget = $file
        $errorItem = New-Object -TypeName System.Management.Automation.ErrorRecord $exception, $errorId, $errorCategory, $errorTarget
        $errorItem.ErrorDetails = "Failed to import function $($file.BaseName)"
        throw $errorItem
    }
}
Export-ModuleMember -Function $PublicFunctions.BaseName -Alias *

# run to generate initial JanusConfig.json
$env:JanusDefaultConfig = "$PSScriptRoot\JanusConfig.json"
Set-JanusConfig -Initialize

#Define Variables
$script:DefaultConfig = Get-Content $env:JanusDefaultConfig | ConvertFrom-Json

if ( $script:DefaultConfig.Browser ) {
    $env:JanusDefaultBrowser = $script:DefaultConfig.Browser    
} else {
    $env:JanusDefaultBrowser = 'microsoft-edge'
}
$env:JanusAPIEndpoint = $script:DefaultConfig.APIEndpoint
$env:JanusDefaultAWSAccount = $script:DefaultConfig.AWSAccountNumber
$env:JanusDefaultRackspaceCloudAccount = $script:DefaultConfig.rackspaceCloudAccountNumber
$env:JanusDefaultPSProfile = $script:DefaultConfig.PowershellProfile
$env:JanusEnableDefaultAccount = $script:DefaultConfig.defaultAccountEnabled
if ( $env:JanusEnableDefaultAccount -and $env:JanusDefaultAWSAccount -and $env:JanusDefaultRackspaceCloudAccount) {
    Set-JanusDefaultAccount -AWSAccountNumber $env:JanusDefaultAWSAccount -rackspaceCloudAccountNumber $env:JanusDefaultRackspaceCloudAccount -Confirm:$false
}