[CmdletBinding(SupportsShouldProcess,ConfirmImpact='low',DefaultParameterSetName='none')]
param (
    [Parameter()]
    $Region = 'us-west-1',

    # Parameter help description
    [Parameter(ValueFromPipelineByPropertyName)]
    [string]
    $InstanceID,

    $KeyPath = "~\.ssh\id_rsa.pem",

    [Parameter(ParameterSetName = 'RDP')]
    [switch]
    $ConnectRDP,

    # Parameter help description
    [Parameter(ParameterSetName='PSSession')]
    [switch]
    $ConnectPSSession
)
begin {
    Function Test-Port($ip,$port){
        $client = New-Object System.Net.Sockets.TcpClient
        try {
            #$client.Connect($ip, $port)
            #[bool]$client.Connected
            $client.BeginConnect($ip, $port, $null, $null).AsyncWaitHandle.WaitOne(1000, $false)
        }
        catch {
            $false
        }
        finally {
            $client.Dispose()
        }
    }
    Function Add-SGFWRule ( $ip, $portRange, $sgid ) {
        if ($portRange -match "\d{1,}-\d{1,}"){
            $fromPort = $portRange.Split('-')[0]
            $toPort =  $portRange.Split('-')[1]
        } else {
            $fromPort = $portRange
            $toPort = $portRange
        }
        $sgpermisson = new-object Amazon.EC2.Model.IpPermission
        $sgpermisson.IpProtocol = "tcp"
        $sgpermisson.FromPort = $fromPort
        $sgpermisson.ToPort = $toPort
        $sgpermisson.IpRanges.Add($ip)
        try {
            Write-Host -f Green "Updating Security group with IP: $myip/32"
            Grant-EC2SecurityGroupIngress -GroupId $sgid -IpPermission $sgpermisson -Region $Region
        }
        catch {
            Write-Warning $_.Exception.Message
        }
    }

    Function FilterSG ($sgs){
        do {
            Write-Host 'More than one security group Found:'
            $n = 0
            do {
                Write-Host -f Yellow "$n" -NoNewline
                Write-Host -f Green (': {0}-{1}' -f $sgs[$n].GroupId,$sgs[$n].GroupName)
                $n++
            } while ( $n -lt $sgs.Count )
            $n--
            $choice = Read-Host -Prompt 'Select a Security Group'
            $regex = '[0-{0}]' -f $n
            $proceed = $choice -match $regex
        } until ( $proceed )
        return $sgs[$choice]
    }
}
Process {
    if ( $InstanceID ) {
        $Instances = (Get-EC2Instance -InstanceId $InstanceID -Region $Region).Instances
        if ( $Instances.Platform.Value -ne 'Windows' ) {
            throw "$InstanceID is not Windows...scrub."
        }
    } else {
        $Instances = ( Get-EC2Instance -Region $Region ).Instances | Where-Object { $_.Platform.Value -eq 'Windows' -and $_.State.Name -match 'running' }
    }
    
    foreach ($instance in $Instances) {
        try {

            #JUNK!!! $unencryptedPass = Get-EC2PasswordData -InstanceId $instance.InstanceId -Region $Region -PemFile $KeyPath
            $passreturn = aws --region=$Region ec2 'get-password-data' --'instance-id'=$($instance.InstanceId) --priv-launch-key=$KeyPath | ConvertFrom-Json
        } catch {
            $passreturn = $false
            Write-Warning ( 'Unable to retrieve key: {0}' -f $_.Exception.Message )
        }
        
        $return = New-Object psobject -Property ([ordered]@{
            Id = $instance.InstanceId
            Name = $instance.Tags.where({$_.key -eq 'Name'}).Value
            Password = $passreturn.PasswordData
            PublicIP = $instance.PublicIpAddress
        })
        if ($ConnectRDP -and $return.Password) {
            #test firewall:
            if ( -not ( Test-Port $return.PublicIP 3389 ) -or  -not ( Test-Port $return.PublicIP 5985 ) ) {
                Write-Warning 'Cannot reach instance.'
                if ( $PSCmdlet.ShouldContinue( "$($return.PublicIP):3389", 'Open Firewall' ) ) {
                    $myip = (Invoke-RestMethod -Uri 'https://icanhazip.com').trim()
                    if ( $instance.SecurityGroups.Count -ne 1 ) {
                        $sg = FilterSG $instance.SecurityGroups
                    } else {
                        $sg = $instance.SecurityGroups[0]
                    }
                    Add-SGFWRule -ip "$myip/32" -portRange 3389 -sgid $sg.GroupId
                }      
            }
            if ($PSCmdlet.ShouldProcess($return.PublicIP, "Connect")) {
                cmdkey /generic:$($return.PublicIP) /user:'Administrator' /pass:$($return.Password) | Out-Null
                mstsc /v:$($return.PublicIP ) /admin
            }
        } elseif ( $ConnectPSSession -and $return.Password ) {
            if ( -not ( Test-Port $return.PublicIP 5985 ) ) {
                Write-Warning 'Cannot reach server on PSSession Ports 5985-5986'
                if ( $PSCmdlet.ShouldContinue( "$($return.PublicIP):3389", 'Open Firewall') ) {
                    $myip = (Invoke-RestMethod -Uri 'https://icanhazip.com').trim()
                    if ( $instance.SecurityGroups.Count -ne 1 ) {
                        $sg = FilterSG $instance.SecurityGroups
                    } else {
                        $sg = $instance.SecurityGroups[0]
                    }
                    Add-SGFWRule -ip "$myip/32" -portRange '5985' -sgid $sg.GroupId
                }
            }
            try {
                Add-PoshSecret -Name $instance.InstanceId -Username Administrator -Password $return.Password
                $cred = Get-PoshSecret -Name $instance.InstanceId -Username Administrator -AsPSCredential
                Get-PSSession | 
                    Where-Object { $_.ComputerName -eq $return.PublicIP } |
                        ForEach-Object {$_ | Remove-PSSession  -ErrorAction SilentlyContinue}
                $session = New-PSSession -ComputerName $return.PublicIP -Credential $cred -ErrorAction Stop
                Write-Warning ( 'Session Created. To connect, run "Enter-PSSession -Id {0}"' -f $session.Id )
                Write-Output $session
                }
            catch {
                Write-Warning $_.Exception.Message
            }
        } else {
            Write-Output $return
        }
    }
}