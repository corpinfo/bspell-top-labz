[CmdletBinding(SupportsShouldProcess , ConfirmImpact = 'High')]
param(
    [validateset('common','dev','prod')]
    $DeployEnv
)
try {
    $env:ACCOUNT_ID= Get-STSCallerIdentity -Select Account
} catch {
    Write-Warning $_.Exception.Message
    return
}

#region Deploy Common
    if ( $DeployEnv -match 'common' -or ($null -eq $DeployEnv) ) {
        $env:DEPLOY_ENVIRONMENT = 'common'
        if ( $WhatIfPreference ) {
            Write-Warning 'Running Taxi Only. NO CHANGES WILL BE MADE TO ENVIRONMENT!!!'
            runway.exe taxi --verbose
        } 
        if( $PSCmdlet.ShouldProcess( $DeployEnv, "Deploy to environment") ) {
            runway.exe takeoff --verbose
        }
    }
#endregion

#region Deploy Dev
if ( $DeployEnv -match 'dev' ) {
    $env:DEPLOY_ENVIRONMENT = 'dev'
    if ( $WhatIfPreference ) {
        Write-Warning 'Running Taxi Only. NO CHANGES WILL BE MADE TO ENVIRONMENT!!!'
        runway.exe taxi --verbose
    }
    if( $PSCmdlet.ShouldProcess( $Environment, "Deploy to environment") ) {
        runway.exe takeoff --verbose
    }
}
#endregion

#region Deploy Prod

#endregion