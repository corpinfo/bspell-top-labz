$accounts = Get-jAccount -AWSID $null | Where-Object { $_.name -match "Student Lab Account" }

$return = @()

foreach ( $account in $accounts ) {
    Get-jCred -awsid $account.awsAccountNumber -Flush | Set-jProfile -Quiet
    #Get-STSCallerIdentity

    try {
        $stackid = (Get-CFNStack -StackName 'top-training-szechuan-sauce').StackId
        $stackExistence = $true
    } catch {
        $stackExistence = $false
    }
    $return += [PSCustomObject]@{
        Account = $account.awsAccountNumber
        SSMStackExists = $stackExistence
        SSMStackId = $stackid
    }
    $stackid = $null
}

#al
$acntid = '017580275405 '
Get-jCred -awsid $acntid -Flush | Set-jProfile -Quiet
Get-jAccount -awsid $acntid
jConsole -AWSID $acntid

#trav
$acntid = '085465185459 '
Get-jCred -awsid $acntid -Flush | Set-jProfile -Quiet
Get-jAccount -awsid $acntid
jConsole -AWSID $acntid

 