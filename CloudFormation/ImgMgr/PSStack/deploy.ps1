[cmdletbinding()]
Param (   
    [Parameter(Position = 0)]
    [ValidateSet('VPC','ImgMGRApp')]
    $DeployStack = 'VPC',

    [Parameter()]
    $Region = 'us-east-1',

    [Parameter()]
    [ValidateSet('production','development')]
    [string]
    $EnvironmentName = 'development',

    [Parameter()]
    [string]
    $ConfigFile = 'vpc.yaml'
)

$stacksplat = @{
    StackName = "Top-Lab-$DeployStack"
    Region = $Region
}

switch ( $DeployStack ) {
    VPC {
        $template = Get-Content -Path 'vpc.yaml' -Raw
    }
    ImgMGRApp {
        $template = Get-Content -Path 'imgmgr-cfn.yaml' -Raw
    }
    Default {}
}

try {
    Get-CFNStack @stacksplat | Out-Null

    Update-CFNStack @stacksplat `
        -Capability CAPABILITY_IAM `
        -TemplateBody $template | Out-Null
    $stack = Get-CFNStack @stacksplat
} catch {
    if ( $_.Exception.Message -match 'No Updates are to be performed' ) {
        Write-Warning "No Changes found to Stack: $StackName"
    }
    if ( $_.Exception.Message -match "Stack.+does not exist" ) {
        try {
            New-CFNStack @stacksplat -Capability CAPABILITY_IAM `
                -TemplateBody $template `
                -OnFailure ROLLBACK | Out-Null
            $stack = Get-CFNStack @stacksplat
        } catch {
            Write-Warning 'Failure Creating Stack:'
            Write-Warning $_.Exception.Message
            exit 1
        }
    } else {
        Write-Warning $_.Exception.Message
        exit 1
    }
}
do  {
    $stack = Get-CFNStack @stacksplat
    Write-Host -ForegroundColor Yellow ( '{0} Stack:{1} Status: {2}' -f [datetime]::Now.ToString('T'),$Stack.StackName,$stack.StackStatus.Value )
    if ( $stack.StackStatus.Value -match "FAILED|ROLLBACK" ) {
        $isFailed = $true
    }
    if ( $stack.StackStatus.Value -match 'COMPLETE' ) {break}
    Start-Sleep -Seconds 30
} until ( $stack.StackStatus.Value -match 'COMPLETE' -or $isFailed )

#add firewall rule for my local IP once stack completes
if ( $DeployStack -eq 'ImgMGRApp' ) {
    $updateSG = $true
    if ( (Get-Process -Name 'Appgate SDP') ) {
        Write-Warning 'Appgate VPN is running. To proceed with adding your local IP to the SecurityGroup, please disconnect from VPN first.'
        if ( $PSCmdlet.ShouldContinue('Have you disconnected from the VPN?', $null ) ) {
            Write-Host -ForegroundColor Green 'Proceeding with SecurityGroup Update..'
        } else {
            Write-Warning 'Skipping Security Group Update..'
            $updateSG = $false
        }
    }
    if ( $updateSG ) {
        $myip = (Invoke-RestMethod -Uri 'https://icanhazip.com').trim()
        $sg = $stack.Outputs.Where({$_.OutputKey -eq 'AutoscaleSG'}).OutputValue
        $sgpermisson = new-object Amazon.EC2.Model.IpPermission
        $sgpermisson.IpProtocol = "tcp"
        $sgpermisson.FromPort = 22
        $sgpermisson.ToPort = 22
        $sgpermisson.IpRanges.Add("$myip/32")
        try {
            Write-Host -f Green "Updating Security group with IP: $myip/32"
            Grant-EC2SecurityGroupIngress -GroupId $sg -IpPermission $sgpermisson
        }
        catch {
            Write-Warning $_.Exception.Message
        }
    }
}

Write-Output $stack