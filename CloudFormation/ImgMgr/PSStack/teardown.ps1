[cmdletbinding(SupportsShouldProcess, ConfirmImpact='High')]
Param (
    # Specify one or more stacks to teardown.
    [Parameter(Position = 0)]
    [ValidateSet('VPC','ImgMGRApp')]
    [string[]]
    $DeployStack = 'ImgMGRApp',

    [Parameter()]
    $Region = 'us-east-1'
)
Process {
    $removeStack = $true
    switch ($DeployStack) {
        ImgMGRApp {
            $stacksplat = @{
                StackName = "Top-Lab-ImgMGRApp"
                Region = $Region
            }
            try {
                $stack = Get-CFNStack @stacksplat
                # need to delete s3 bucket prior to removing app.
                $s3bucket = Get-CFNStackResourceList @stacksplat | Where-Object { $_.ResourceType -eq 'AWS::S3::Bucket' }
                if ( $s3bucket ) {
                    Remove-S3Bucket -BucketName $s3bucket.PhysicalResourceId -DeleteBucketContent
                }
                if ( $removeStack ) {
                    try {
                        Remove-CFNStack @stacksplat
                        $stack = Get-CFNStack @stacksplat
                    }
                    catch {
                        Write-Warning $_.Exception.Message
                    }
                }
                while ($stack) {
                    try {
                        $stack = Get-CFNStack @stacksplat
                        if ( $stack.StackStatus.Value -match "FAILED|ROLLBACK" ) {
                            Write-Warning 'Stack Delete Failed'
                            Write-Output $stack
                            break
                        } else {
                            Write-Warning ( '{0} Stack: {1} Status: {2}' -f [datetime]::Now.ToString('T'),$Stack.StackName,$stack.StackStatus.Value )
                        }
                    } catch { 
                        $stack = $null
                        $stacksplat = $null
                        $s3bucket = $null
                        break
                    }
                    Start-Sleep -Seconds 30
                }
                Write-Host -f Green ('Stack Deleted.')                    
            }
            catch {
               Write-Warning $_.Exception.Message 
            }
            
        }
        VPC {
            $stacksplat = @{
                StackName = "Top-Lab-VPC"
                Region = $Region
            }
            try {
                $stack = Get-CFNStack @stacksplat

                # need to check if the ImgMgr stack exists
                try {
                    Get-CFNStack -StackName 'Top-Lab-ImgMGRApp1'
                    Write-Warning 'Dependent Stack still exists. Please remove prior to removing this stack.'
                    $removeStack = $false
                }
                catch {
                    Write-Host -f Green 'No Dependent Stacks found.'
                }
                try {
                    if ( $removeStack ) {
                        try {
                            Remove-CFNStack @stacksplat
                            $stack = Get-CFNStack @stacksplat
                        }
                        catch {
                            Write-Warning $_.Exception.Message
                        }
                    }
                }
                catch {
                    Write-Warning $_.Exception.Message
                }
                while ($stack) {
                    try {
                        $stack = Get-CFNStack @stacksplat
                        if ( $stack.StackStatus.Value -match "FAILED|ROLLBACK" ) {
                            Write-Warning 'Stack Delete Failed'
                            Write-Output $stack
                            break
                        } else {
                            Write-Warning ( '{0} Stack: {1} Status: {2}' -f [datetime]::Now.ToString('T'),$Stack.StackName,$stack.StackStatus.Value )
                        }
                    } catch { 
                        $stack = $null
                        $stacksplat = $null
                        break
                    }
                    Start-Sleep -Seconds 30
                }
                Write-Host -f Green ('Stack Deleted.')                    
            }
            catch {
                Write-Warning $_.Exception.Message
            }

        }
        Default {}
    }
}
