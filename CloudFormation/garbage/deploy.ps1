Param (
    $Region = 'us-west-2',
    [ValidateSet('Prod','Dev')]
    $StackName = 'Dev-img-mgr',
    $ConfigFile = 'deploy.json'
)
    
aws --region $Region cloudformation deploy `
    --stack-name $StackName `
    --template-file $ConfigFile `
    --capabilities CAPABILITY_IAM `
    --parameter-overrides  `
        InstanceType=t2.micro  `
        EnvironmentName=development  `
        CustomerName=Best `
        ApplicationName=imgmgr `
        SshKey=MyVPCLab-KeyPair `
        ImageId=ami-083ac7c7ecf9bb9b0 `
        VpcId=vpc-0daa9249d4733a265 `
        Subnet=subnet-0a38683c406a54427 `
        LoadBalancerSubnets=subnet-0a38683c406a54427
