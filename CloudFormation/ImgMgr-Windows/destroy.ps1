<#
.SYNOPSIS
    Wrapper script for deploying runway across environment.
.DESCRIPTION
    Long description
.EXAMPLE
    PS C:\> .\deploy.ps1 -Environment Common
    Deploys the common environment stack.
.EXAMPLE
    PS C:\> .\deploy.ps1 -Environment Development
    Deploys the development environment stack.

.INPUTS
    Inputs (if any)
.OUTPUTS
    Output (if any)
.NOTES
    General notes
#>
[CmdletBinding(SupportsShouldProcess , ConfirmImpact = 'High')]
param (
    [Parameter()]
    [string]
    [ValidateSet('development','production')]
    $Environment = 'development',

    $region = 'us-east-1'
)
Begin {
    $env:DEPLOY_ENVIRONMENT = $Environment
}

Process {
    #let R Rip
    if( $PSCmdlet.ShouldProcess( $Environment, "Kill it") ) {
        runway.exe destroy
    }
}