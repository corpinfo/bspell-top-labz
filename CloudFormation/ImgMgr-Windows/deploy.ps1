<#
.SYNOPSIS
    Wrapper script for deploying runway across environment.
.DESCRIPTION
    Long description
.EXAMPLE
    PS C:\> .\deploy.ps1 -Environment Common
    Deploys the common environment stack.
.EXAMPLE
    PS C:\> .\deploy.ps1 -Environment Development
    Deploys the development environment stack.

.INPUTS
    Inputs (if any)
.OUTPUTS
    Output (if any)
.NOTES
    General notes
#>
[CmdletBinding(SupportsShouldProcess , ConfirmImpact = 'High')]
param (
    [Parameter()]
    [string]
    [ValidateSet('development','production')]
    $Environment = 'development'
)
Begin {
    $env:DEPLOY_ENVIRONMENT = $Environment
}

Process {
    if ( $DebugPreference -match '^[Cc]ontinue' ) {
        $logLevel = '--debug'
    } elseif ( $VerbosePreference -match '^[Cc]ontinue' ) {
        $logLevel = '--verbose'
    }

    if ( $WhatIfPreference ) {
        Write-Warning 'Running Taxi Only. NO CHANGES WILL BE MADE TO ENVIRONMENT!!!'
        runway.exe taxi
    }
    #let R Rip
    if( $PSCmdlet.ShouldProcess( $Environment, "Deploy to environment") ) {
        runway.exe deploy $logLevel
    }
}