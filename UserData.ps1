<powershell>
write-output "Running User Data Script"
write-host "(host) Running User Data Script"

Set-ExecutionPolicy Unrestricted -Scope LocalMachine -Force -ErrorAction Ignore


Enable-PSRemoting –force
Set-Service WinRM -StartMode Automatic
Set-NetFirewallProfile -Profile Domain, Public, Private -Enabled False
Set-Item WSMan:localhost\client\trustedhosts -value '*' -force
</powershell>